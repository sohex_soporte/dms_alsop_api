<?php

use App\Models\Autos\HistoricoProductosServicioModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaHistoricoProductosServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(HistoricoProductosServicioModel::getTableName(), function (Blueprint $table) {
            $table->increments(HistoricoProductosServicioModel::ID);
            $table->unsignedInteger(HistoricoProductosServicioModel::PRODUCTO_ID);
            $table->foreign(HistoricoProductosServicioModel::PRODUCTO_ID)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());
            $table->integer(HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID);
            $table->string(HistoricoProductosServicioModel::NO_IDENTIFICACION);
            $table->integer(HistoricoProductosServicioModel::CANTIDAD);
            $table->string(HistoricoProductosServicioModel::NUMERO_ORDEN)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(HistoricoProductosServicioModel::getTableName());
    }
}
