<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\User;
use App\Models\Usuarios\UsuariosModulosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaUsuariosModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UsuariosModulosModel::getTableName(), function (Blueprint $table) {
            $table->increments(UsuariosModulosModel::ID);
            $table->unsignedInteger(UsuariosModulosModel::MODULO_ID);
            $table->foreign(UsuariosModulosModel::MODULO_ID)
                ->references(ModulosModel::ID)
                ->on(ModulosModel::getTableName());

            $table->unsignedInteger(UsuariosModulosModel::USUARIO_ID);
            $table->foreign(UsuariosModulosModel::USUARIO_ID)
                ->references(User::ID)
                ->on(User::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(UsuariosModulosModel::getTableName());
    }
}
