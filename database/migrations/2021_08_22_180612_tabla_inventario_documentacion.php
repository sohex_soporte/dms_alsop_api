<?php

use App\Models\Autos\Inventario\DocumentacionModel;
use App\Models\Autos\Inventario\InventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioDocumentacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::create(DocumentacionModel::getTableName(), function (Blueprint $table) {
            $table->increments(DocumentacionModel::ID);
            $table->string(DocumentacionModel::POLIZA_GARANTIA)->nullable();
            $table->string(DocumentacionModel::SEGURO_RINES)->nullable();
            $table->string(DocumentacionModel::CERTIFICADO_VERIFICACION)->nullable();
            $table->string(DocumentacionModel::TARJETA_CIRCULACION)->nullable();
            $table->unsignedInteger(DocumentacionModel::ID_INVENTARIO);
            $table->foreign(DocumentacionModel::ID_INVENTARIO)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop(DocumentacionModel::getTableName());
    }
}
