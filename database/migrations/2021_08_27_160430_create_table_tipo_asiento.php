<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\TipoAsientoModel;

class CreateTableTipoAsiento extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TipoAsientoModel::getTableName(), function (Blueprint $table) {
            $table->increments(TipoAsientoModel::ID);
            $table->string(TipoAsientoModel::DESCRIPCION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TipoAsientoModel::getTableName());
    }
}
