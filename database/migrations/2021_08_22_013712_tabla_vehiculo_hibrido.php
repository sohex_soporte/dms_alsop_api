<?php

use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\SalidaUnidades\VehiculoHibridoModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaVehiculoHibrido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(VehiculoHibridoModel::getTableName(), function (Blueprint $table) {
            $table->increments(VehiculoHibridoModel::ID);
            $table->string(VehiculoHibridoModel::VEHICULO_HIBRIDO)->nullable();
            $table->string(VehiculoHibridoModel::MOTOR_GASOLINA)->nullable();
            $table->string(VehiculoHibridoModel::TIPS_MANEJO)->nullable();
            $table->string(VehiculoHibridoModel::TRANSMISION_ECVT)->nullable();
            $table->unsignedInteger(VehiculoHibridoModel::ID_VENTA_AUTO);
            $table->foreign(VehiculoHibridoModel::ID_VENTA_AUTO)->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VehiculoHibridoModel::getTableName());
    }
}
