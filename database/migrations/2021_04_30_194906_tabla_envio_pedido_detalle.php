<?php

use App\Models\Refacciones\EnvioPedidoDetalleModel;
use App\Models\Refacciones\MaProductoPedidoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEnvioPedidoDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EnvioPedidoDetalleModel::getTableName(), function (Blueprint $table) {
            $table->increments(EnvioPedidoDetalleModel::ID);

            $table->string(EnvioPedidoDetalleModel::VIA_ENVIO)->nullable();
            $table->string(EnvioPedidoDetalleModel::DIA_ENVIO)->nullable();
            $table->string(EnvioPedidoDetalleModel::RUTA)->nullable();
            $table->string(EnvioPedidoDetalleModel::LINEAS)->nullable();
            $table->string(EnvioPedidoDetalleModel::TOTAL_PIEZAS)->nullable();
            $table->string(EnvioPedidoDetalleModel::PESO_TOTAL)->nullable();
            $table->string(EnvioPedidoDetalleModel::NUMERO_REMISION)->nullable();

            $table->unsignedInteger(EnvioPedidoDetalleModel::PEDIDO_ID);
            $table->foreign(EnvioPedidoDetalleModel::PEDIDO_ID)
                ->references(MaProductoPedidoModel::ID)
                ->on(MaProductoPedidoModel::getTableName());

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EnvioPedidoDetalleModel::getTableName());
    }
}
