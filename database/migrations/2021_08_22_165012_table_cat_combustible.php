<?php

use App\Models\Autos\UnidadesNuevas\CatCombustible;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatCombustible extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatCombustible::getTableName(), function (Blueprint $table) {
            $table->increments(CatCombustible::ID);
            $table->string(CatCombustible::DESCRIPCION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatCombustible::getTableName());
    }
}
