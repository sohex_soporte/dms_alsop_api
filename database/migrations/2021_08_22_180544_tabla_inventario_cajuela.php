<?php

use App\Models\Autos\Inventario\CajuelaModel;
use App\Models\Autos\Inventario\InventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioCajuela extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(CajuelaModel::getTableName(), function (Blueprint $table) {
            $table->increments(CajuelaModel::ID);
            $table->string(CajuelaModel::HERRAMIENTA)->nullable();
            $table->text(CajuelaModel::GATO_LLAVE)->nullable();
            $table->string(CajuelaModel::REFLEJANTES)->nullable();
            $table->string(CajuelaModel::CABLES)->nullable();
            $table->text(CajuelaModel::EXTINTOR)->nullable();
            $table->string(CajuelaModel::LLANTA_REFACCION)->nullable();
            $table->unsignedInteger(CajuelaModel::ID_INVENTARIO);
            $table->foreign(CajuelaModel::ID_INVENTARIO)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CajuelaModel::getTableName());
    }
}
