<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;

class CrearCatalogoTipoFormaPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TipoFormaPagoModel::getTableName(), function (Blueprint $table) {
            $table->increments(TipoFormaPagoModel::ID);
            $table->string(TipoFormaPagoModel::DESCRIPCION);    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TipoFormaPagoModel::getTableName());
    }
}
