<?php

use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use App\Models\Autos\UnidadesNuevas\HistorialStatusUnidadesModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableHistorialEstatusUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(HistorialStatusUnidadesModel::getTableName(), function (Blueprint $table) {
            $table->increments(HistorialStatusUnidadesModel::ID);
            $table->unsignedInteger(HistorialStatusUnidadesModel::ID_STATUS_ACTUAL);
            $table->foreign(HistorialStatusUnidadesModel::ID_STATUS_ACTUAL)
                ->references(CatStatusRecepcionUNModel::ID)
                ->on(CatStatusRecepcionUNModel::getTableName());
            $table->unsignedInteger(HistorialStatusUnidadesModel::ID_STATUS_NUEVO);
            $table->foreign(HistorialStatusUnidadesModel::ID_STATUS_NUEVO)
                    ->references(CatStatusRecepcionUNModel::ID)
                    ->on(CatStatusRecepcionUNModel::getTableName());
            $table->unsignedInteger(HistorialStatusUnidadesModel::ID_USUARIO);
            $table->foreign(HistorialStatusUnidadesModel::ID_USUARIO)
                    ->references(User::ID)
                    ->on(User::getTableName());
            $table->unsignedInteger(HistorialStatusUnidadesModel::ID_UNIDAD);
            $table->foreign(HistorialStatusUnidadesModel::ID_UNIDAD)
                    ->references(RemisionModel::ID)
                    ->on(RemisionModel::getTableName());
            $table->text(HistorialStatusUnidadesModel::COMENTARIO);
            $table->string(HistorialStatusUnidadesModel::IMAGEN)->nullable();
            $table->unsignedInteger(HistorialStatusUnidadesModel::ID_UBICACION);
            $table->foreign(HistorialStatusUnidadesModel::ID_UBICACION)
                    ->references(CatalogoUbicacionModel::ID)
                    ->on(CatalogoUbicacionModel::getTableName());
            $table->unsignedInteger(HistorialStatusUnidadesModel::ID_UBICACION_LLAVES);
            $table->foreign(HistorialStatusUnidadesModel::ID_UBICACION_LLAVES)
                    ->references(UbicacionLLavesModel::ID)
                    ->on(UbicacionLLavesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemisionModel::getTableName());
    }
}
