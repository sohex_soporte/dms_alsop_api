<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CatalogoMotivoCancelacion;


class CreateTableCatalogoTipoCancelacion extends Migration
{
    public function up()
    {
        Schema::create(CatalogoMotivoCancelacion::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoMotivoCancelacion::ID);
            $table->string(CatalogoMotivoCancelacion::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoMotivoCancelacion::getTableName());
    }
}
