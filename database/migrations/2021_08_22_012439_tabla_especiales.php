<?php

use App\Models\Autos\SalidaUnidades\EspecialesModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEspeciales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EspecialesModel::getTableName(), function (Blueprint $table) {
            $table->increments(EspecialesModel::ID);
            $table->string(EspecialesModel::PEPS)->nullable();
            $table->string(EspecialesModel::APERTIRA_CAJUELA)->nullable();
            $table->string(EspecialesModel::ENCENDIDO_REMOTO)->nullable();
            $table->string(EspecialesModel::TECHO_PANORAMICO)->nullable();
            $table->string(EspecialesModel::AIRE_ACONDICIONADO)->nullable();
            $table->string(EspecialesModel::EATC)->nullable();
            $table->string(EspecialesModel::CONSOLA_ENFRIAMIENTO)->nullable();
            $table->string(EspecialesModel::VOLANTE_AJUSTE_ALTURA)->nullable();
            $table->string(EspecialesModel::VOLANTE_CALEFACTADO)->nullable();
            $table->string(EspecialesModel::ESTRIBOS_ELECTRICOS)->nullable();
            $table->string(EspecialesModel::APERTURA_CAJA_CARGA)->nullable();
            $table->string(EspecialesModel::LIMPIAPARABRISAS)->nullable();
            $table->string(EspecialesModel::ESPEJO_ELECTRICO)->nullable();
            $table->string(EspecialesModel::ESPEJOS_ABATIBLES)->nullable();
            $table->unsignedInteger(EspecialesModel::ID_VENTA_AUTO);
            $table->foreign(EspecialesModel::ID_VENTA_AUTO)->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EspecialesModel::getTableName());
    }
}
