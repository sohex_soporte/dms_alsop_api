<?php

use App\Models\Autos\UnidadesNuevas\CatTipoBicicletas;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatTipoBicicletas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoBicicletas::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoBicicletas::ID);
            $table->string(CatTipoBicicletas::TIPO_BICILETA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoBicicletas::getTableName());
    }
}
