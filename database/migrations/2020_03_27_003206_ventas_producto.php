<?php

use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VentasProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(VentaProductoModel::getTableName(), function (Blueprint $table) {
            $table->increments(VentaProductoModel::ID);
            $table->unsignedInteger(VentaProductoModel::PRODUCTO_ID);
            $table->foreign(VentaProductoModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->unsignedInteger(VentaProductoModel::PRECIO_ID);
            $table->foreign(VentaProductoModel::PRECIO_ID)->references(Precios::ID)->on(Precios::getTableName());

            $table->unsignedInteger(VentaProductoModel::CLIENTE_ID)->nullable();
            $table->foreign(VentaProductoModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());

            $table->unsignedInteger(VentaProductoModel::FOLIO_ID);
            $table->foreign(VentaProductoModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());

            $table->integer(VentaProductoModel::CANTIDAD);

            $table->float(VentaProductoModel::TOTAL_VENTA)->nullable();
            $table->float(VentaProductoModel::VALOR_UNITARIO)->nullable();

            $table->boolean(VentaProductoModel::DESCONTADO_ALMACEN)->nullable();

            $table->unsignedInteger(VentaProductoModel::VENTA_ID);

            $table->foreign(VentaProductoModel::VENTA_ID)->references(VentasRealizadasModel::ID)->on(VentasRealizadasModel::getTableName());
            $table->boolean(VentaProductoModel::ORDEN_ORIGINAL)->nullable();
            $table->string(VentaProductoModel::ESTATUS_PRODUCTO_ORDEN)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentaProductoModel::getTableName());
    }
}
