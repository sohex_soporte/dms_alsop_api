<?php

use App\Models\Autos\EstatusSalidaUnidadModel;
use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\CatProveedoresUN;
use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableRemisiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RemisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(RemisionModel::ID);
          
            $table->string(RemisionModel::VERSION);
            $table->string(RemisionModel::SERIE);
            $table->string(RemisionModel::FOLIO);
            $table->string(RemisionModel::FECHA);

            $table->text(RemisionModel::SELLO);
            $table->string(RemisionModel::NO_CERTIFICADO);
            $table->text(RemisionModel::CERTIFICADO);
            $table->string(RemisionModel::SUBTOTAL);
            $table->string(RemisionModel::TIPO_CAMBIO);
            $table->string(RemisionModel::MONEDA);
            $table->double(RemisionModel::TOTAL);
            $table->string(RemisionModel::TIPO_COMPROBANTE);
            $table->string(RemisionModel::METODO_PAGO);
            $table->string(RemisionModel::FORMA_PAGO);
            $table->string(RemisionModel::LUGAR_EXPEDICION);
            $table->string(RemisionModel::EMISOR_RFC);
            $table->string(RemisionModel::EMISOR_NOMBRE);
            $table->string(RemisionModel::EMISOR_REGIMEN_FISCAL);
            $table->string(RemisionModel::RECEPTOR_RFC);
            $table->string(RemisionModel::RECEPTOR_NOMBRE);
            $table->string(RemisionModel::RECEPTOR_REGIMEN_FISCAL);
            $table->unsignedInteger(RemisionModel::USERID);
            $table->foreign(RemisionModel::USERID)
                ->references(User::ID)
                ->on(User::getTableName());
           ;
            $table->unsignedInteger(RemisionModel::ESTATUS_ID)->default(EstatusSalidaUnidadModel::ESTATUS_SALIDA_DISPONIBLE);
            $table->foreign(RemisionModel::ESTATUS_ID)
                ->references(EstatusSalidaUnidadModel::ID)
                ->on(EstatusSalidaUnidadModel::getTableName());
            $table->unsignedInteger(RemisionModel::UBICACION_ID)->default(1);
            $table->foreign(RemisionModel::UBICACION_ID)
                ->references(CatalogoUbicacionModel::ID)
                ->on(CatalogoUbicacionModel::getTableName());
            $table->date(RemisionModel::ULTIMO_SERVICIO)->nullable();
            $table->unsignedInteger(RemisionModel::ID_STATUS_UNIDAD)->default(1);
            $table->foreign(RemisionModel::ID_STATUS_UNIDAD)
                ->references(CatStatusRecepcionUNModel::ID)
                ->on(CatStatusRecepcionUNModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemisionModel::getTableName());
    }
}
