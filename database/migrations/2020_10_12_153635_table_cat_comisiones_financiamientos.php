<?php

use App\Models\Financiamientos\CatComisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatComisionesFinanciamientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatComisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatComisionModel::ID);
            $table->string(CatComisionModel::COMISION);
            $table->float(CatComisionModel::VALOR);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatComisionModel::getTableName());
    }
}
