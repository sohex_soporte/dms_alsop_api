<?php

use App\Models\Autos\EstatusSalidaUnidadModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEstatusUnidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusSalidaUnidadModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusSalidaUnidadModel::ID);
            $table->string(EstatusSalidaUnidadModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusSalidaUnidadModel::getTableName());
    }
}
