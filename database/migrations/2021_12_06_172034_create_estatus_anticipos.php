<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\EstatusAnticiposModel;

class CreateEstatusAnticipos extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusAnticiposModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusAnticiposModel::ID);
            $table->string(EstatusAnticiposModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusAnticiposModel::getTableName());
    }
}
