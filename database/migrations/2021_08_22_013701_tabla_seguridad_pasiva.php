<?php


use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaSeguridadPasiva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SeguridadPasivaModel::getTableName(), function (Blueprint $table) {
            $table->increments(SeguridadPasivaModel::ID);
            $table->string(SeguridadPasivaModel::CINTURON_SEGURIDAD)->nullable();
            $table->string(SeguridadPasivaModel::BOLSAS_AIRE)->nullable();
            $table->string(SeguridadPasivaModel::CHASIS_CARROCERIA)->nullable();
            $table->string(SeguridadPasivaModel::CARROSERIA_HIDROFORMA)->nullable();
            $table->string(SeguridadPasivaModel::CRISTALES)->nullable();
            $table->string(SeguridadPasivaModel::CABECERAS)->nullable();
            $table->string(SeguridadPasivaModel::SISTEMA_ALERTA)->nullable();
            $table->string(SeguridadPasivaModel::TECLADO_PUERTA)->nullable();
            $table->string(SeguridadPasivaModel::ALARMA_VOLUMETRICA)->nullable();
            $table->string(SeguridadPasivaModel::ALARMA_PERIMETRAL)->nullable();
            $table->string(SeguridadPasivaModel::MYKEY)->nullable();
            $table->unsignedInteger(SeguridadPasivaModel::ID_VENTA_AUTO);
            $table->foreign(SeguridadPasivaModel::ID_VENTA_AUTO)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SeguridadPasivaModel::getTableName());
    }
}
