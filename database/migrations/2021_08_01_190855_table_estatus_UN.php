<?php

use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEstatusUN extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatEstatusUNModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatEstatusUNModel::ID);
            $table->string(CatEstatusUNModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatEstatusUNModel::getTableName());
    }
}
