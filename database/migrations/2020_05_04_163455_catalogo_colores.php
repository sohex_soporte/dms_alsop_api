<?php

use App\Models\Refacciones\CatalogoColoresModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoColores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoColoresModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoColoresModel::ID);
            $table->string(CatalogoColoresModel::NOMBRE);
            $table->string(CatalogoColoresModel::CLAVE);
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoColoresModel::getTableName());
    }
}
