<?php

use App\Models\Soporte\TelefonosSoporteModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableTelefonosSoporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TelefonosSoporteModel::getTableName(), function (Blueprint $table) {
            $table->increments(TelefonosSoporteModel::ID);
            $table->text(TelefonosSoporteModel::TELEFONO);
            $table->text(TelefonosSoporteModel::USUARIO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TelefonosSoporteModel::getTableName());
    }
}
