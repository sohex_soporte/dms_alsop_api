<?php

use App\Models\Facturas\Factura;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ProveedorRefacciones;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrdenCompraTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OrdenCompraModel::getTableName(), function (Blueprint $table) {
            $table->increments(OrdenCompraModel::ID);

            $table->unsignedInteger(OrdenCompraModel::PROVEEDOR_ID);
            $table->foreign(OrdenCompraModel::PROVEEDOR_ID)->references(ProveedorRefacciones::ID)->on(ProveedorRefacciones::getTableName());

            $table->unsignedInteger(OrdenCompraModel::FACTURA_ID)->nullable();
            $table->foreign(OrdenCompraModel::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());

            $table->unsignedInteger(OrdenCompraModel::FOLIO_ID)->nullable();
            $table->foreign(OrdenCompraModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());

            $table->date(OrdenCompraModel::FECHA_PAGO);
            $table->date(OrdenCompraModel::FECHA);
            $table->string(OrdenCompraModel::FACTURA)->nullable();
            $table->longText(OrdenCompraModel::OBSERVACIONES)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(OrdenCompraModel::getTableName());
    }
}
