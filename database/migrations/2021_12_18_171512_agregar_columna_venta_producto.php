<?php

use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarColumnaVentaProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(VentaProductoModel::getTableName(), function (Blueprint $table) {
            $table->string(VentaProductoModel::ID_INDEX_REQUISICION)->nullable();
            $table->integer(VentaProductoModel::PRECIO_MANUAL)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(VentaProductoModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(VentaProductoModel::ID_INDEX_REQUISICION);
            $table->dropColumn(VentaProductoModel::PRECIO_MANUAL);
        });
    }
}
