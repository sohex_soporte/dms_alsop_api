<?php

use App\Models\Facturas\Factura;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\Talleres;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregaRelacionPrecioProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ProductosModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(ProductosModel::PRECIO_ID)->nullable();
            $table->foreign(ProductosModel::PRECIO_ID)->references(Precios::ID)->on(Precios::getTableName());
            $table->unsignedInteger(ProductosModel::TALLER_ID)->nullable();
            $table->foreign(ProductosModel::TALLER_ID)->references(Talleres::ID)->on(Talleres::getTableName());
            $table->unsignedInteger(ProductosModel::FACTURA_ID)->nullable();
            $table->foreign(ProductosModel::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());

            $table->unsignedInteger(ProductosModel::ALMACEN_ID)->nullable();
            $table->foreign(ProductosModel::ALMACEN_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ProductosModel::getTableName());
    }
}
