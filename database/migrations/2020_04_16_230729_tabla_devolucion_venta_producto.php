<?php

use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\DevolucionVentaProductoModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDevolucionVentaProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(DevolucionVentaProductoModel::getTableName(), function (Blueprint $table) {
            $table->increments(DevolucionVentaProductoModel::ID);
            $table->unsignedInteger(DevolucionVentaProductoModel::FOLIO_ID);
            $table->foreign(DevolucionVentaProductoModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
     
            
            $table->unsignedInteger(DevolucionVentaProductoModel::PRODUCTO_ID);
            $table->foreign(DevolucionVentaProductoModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            
            $table->unsignedInteger(DevolucionVentaProductoModel::PRECIO_ID);
            $table->foreign(DevolucionVentaProductoModel::PRECIO_ID)->references(Precios::ID)->on(Precios::getTableName());
            
            $table->integer(DevolucionVentaProductoModel::CANTIDAD);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DevolucionVentaProductoModel::getTableName());
    }
}
