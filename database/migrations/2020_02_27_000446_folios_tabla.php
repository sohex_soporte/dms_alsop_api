<?php

use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\FoliosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FoliosTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FoliosModel::getTableName(), function (Blueprint $table) {
            $table->increments(FoliosModel::ID);
            $table->string(FoliosModel::FOLIO)->nullable();
            $table->unsignedInteger(FoliosModel::TIPO_PROCESO_ID);
            $table->foreign(FoliosModel::TIPO_PROCESO_ID)->references(CatalogoProcesosModel::ID)->on(CatalogoProcesosModel::getTableName());
            $table->string(FoliosModel::STATUS)->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FoliosModel::getTableName());
    }
}
