<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;


class CreateTablaCatTipoAbono extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoAbonoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoAbonoModel::ID);
            $table->string(CatTipoAbonoModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoAbonoModel::getTableName());

    }
}
