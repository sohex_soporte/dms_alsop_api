<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\EstatusVentaModel;


class CreateTableEstatusVentas extends Migration
{
    public function up()
    {
        Schema::create(EstatusVentaModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusVentaModel::ID);
            $table->string(EstatusVentaModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusVentaModel::getTableName());
    }
}
