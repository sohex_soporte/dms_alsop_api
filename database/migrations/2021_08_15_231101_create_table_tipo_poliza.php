<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\TipoPolizasModel;

class CreateTableTipoPoliza extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TipoPolizasModel::getTableName(), function (Blueprint $table) {
            $table->increments(TipoPolizasModel::ID);
            $table->string(TipoPolizasModel::CLAVE);
            $table->string(TipoPolizasModel::DESCRIPCION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TipoPolizasModel::getTableName());
    }
}
