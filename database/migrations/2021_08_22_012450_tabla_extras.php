<?php

use App\Models\Autos\SalidaUnidades\ExtrasModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaExtras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ExtrasModel::getTableName(), function (Blueprint $table) {
            $table->increments(ExtrasModel::ID);
            $table->string(ExtrasModel::CONECTIVIDAD)->nullable();
            $table->string(ExtrasModel::SYNC)->nullable();
            $table->string(ExtrasModel::MYFORD_TOUCH)->nullable();
            $table->string(ExtrasModel::MYFORD_TOUCH_NAVEGACION)->nullable();
            $table->string(ExtrasModel::SYNC_FORDPASS)->nullable();
            $table->string(ExtrasModel::APPLE_CARPLAY)->nullable();
            $table->string(ExtrasModel::ANDROID_AUTO)->nullable();
            $table->string(ExtrasModel::CONSOLA_SMARTPHONE)->nullable();
            $table->string(ExtrasModel::AUDIO_SHAKER_PRO)->nullable();
            $table->string(ExtrasModel::RADIO_HD)->nullable();
            $table->string(ExtrasModel::EQUIPO_SONY)->nullable();
            $table->string(ExtrasModel::PUERTOS_USB)->nullable();
            $table->string(ExtrasModel::BLUETOOH)->nullable();
            $table->string(ExtrasModel::WIFI)->nullable();
            $table->string(ExtrasModel::TARJETA_SD)->nullable();
            $table->string(ExtrasModel::INVERSOR_CORRIENTE)->nullable();
            $table->string(ExtrasModel::ESPEJO_ELECTROCROMATICO)->nullable();
            $table->unsignedInteger(ExtrasModel::ID_VENTA_AUTO);
            $table->foreign(ExtrasModel::ID_VENTA_AUTO)->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ExtrasModel::getTableName());
    }
}
