<?php

use App\Models\Autos\CatModelosModel;
use App\Models\Oasis\CatEstatusOasisModel;
use App\Models\Oasis\OasisModel;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableOasis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OasisModel::getTableName(), function (Blueprint $table) {
            $table->increments(OasisModel::ID);
            $table->string(OasisModel::VIN);
            $table->string(OasisModel::SERIE_CORTA);
            $table->string(OasisModel::NO_ECONOMICO);
            $table->unsignedInteger(OasisModel::ID_UNIDAD);
            $table->foreign(OasisModel::ID_UNIDAD)->references(CatModelosModel::ID)->on(CatModelosModel::getTableName());
            $table->unsignedInteger(OasisModel::ID_COLOR);
            $table->foreign(OasisModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->unsignedInteger(OasisModel::ID_ANIO);
            $table->foreign(OasisModel::ID_ANIO)->references(CatalogoAnioModel::ID)->on(CatalogoAnioModel::getTableName());
            $table->string(OasisModel::QR)->nullable();
            $table->unsignedInteger(OasisModel::ID_USUARIO)->nullable();
            $table->unsignedInteger(OasisModel::ID_UBICACION);
            $table->foreign(OasisModel::ID_UBICACION)->references(CatalogoUbicacionModel::ID)->on(CatalogoUbicacionModel::getTableName());
            $table->unsignedInteger(OasisModel::ID_UBICACION_LLAVES);
            $table->foreign(OasisModel::ID_UBICACION_LLAVES)->references(UbicacionLLavesModel::ID)->on(UbicacionLLavesModel::getTableName());
            $table->date(OasisModel::FECHA);

            $table->float(OasisModel::DESCUENTO_MAXIMO)->nullable();
            $table->float(OasisModel::TENENCIA)->nullable();
            $table->float(OasisModel::SEGURO_COB_AMPLIA)->nullable();
            $table->float(OasisModel::PLAN_GANE)->nullable();
            $table->string(OasisModel::ORDEN_REPORTE)->nullable();
            $table->string(OasisModel::UNIDAD_IMPORTADA)->nullable();
            $table->string(OasisModel::TIPO_AUTO)->nullable();
            $table->float(OasisModel::COSTO_VALOR_UNIDAD)->nullable();
            $table->float(OasisModel::VENTA_VALOR_UNIDAD)->nullable();
            $table->float(OasisModel::COSTO_EQUIPO_BASE)->nullable();
            $table->float(OasisModel::VENTA_EQUIPO_BASE)->nullable();
            $table->float(OasisModel::COSTO_TOTAL_BASE)->nullable();
            $table->float(OasisModel::VENTA_TOTAL_BASE)->nullable();
            $table->float(OasisModel::COSTO_OTROS_GASTOS)->nullable();
            $table->float(OasisModel::VENTA_OTROS_GASTOS)->nullable();
            $table->float(OasisModel::COSTO_DEDUCCIONES_FORD)->nullable();
            $table->float(OasisModel::VENTA_DEDUCCIONES_FORD)->nullable();
            $table->float(OasisModel::COSTO_SEGUROS_TRASLADOS)->nullable();
            $table->float(OasisModel::VENTA_SEGUROS_TRASLADOS)->nullable();
            $table->float(OasisModel::COSTO_GASTOS_TRASLADOS)->nullable();
            $table->float(OasisModel::VENTA_GASTOS_TRASLADOS)->nullable();
            $table->float(OasisModel::COSTO_GASTOS_ACOND)->nullable();
            $table->float(OasisModel::VENTA_GASTOS_ACOND)->nullable();
            $table->float(OasisModel::COSTO_IMP_IMP)->nullable();
            $table->float(OasisModel::VENTA_IMP_IMP)->nullable();
            $table->float(OasisModel::COSTO_FLETES_EXT)->nullable();
            $table->float(OasisModel::VENTA_FLETES_EXT)->nullable();
            $table->float(OasisModel::COSTO_ISAN)->nullable();
            $table->float(OasisModel::VENTA_ISAN)->nullable();
            $table->float(OasisModel::COSTO_SUBTOTAL)->nullable();
            $table->float(OasisModel::VENTA_SUBTOTAL)->nullable();
            $table->float(OasisModel::COSTO_IVA)->nullable();
            $table->float(OasisModel::VENTA_IVA)->nullable();
            $table->string(OasisModel::CLAVE_ISAN)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(OasisModel::getTableName());
    }
}
