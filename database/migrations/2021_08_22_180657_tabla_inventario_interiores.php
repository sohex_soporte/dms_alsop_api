<?php

use App\Models\Autos\Inventario\InterioresModel;
use App\Models\Autos\Inventario\InventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioInteriores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(InterioresModel::getTableName(), function (Blueprint $table) {
            $table->increments(InterioresModel::ID);
            $table->string(InterioresModel::LLAVERO)->nullable();
            $table->string(InterioresModel::SEGURO_RINES)->nullable();
            $table->string(InterioresModel::INDICADORES_ACTIVADOS)->nullable();

            $table->string(InterioresModel::INDICADORES)->nullable();
            $table->string(InterioresModel::ROCIADOR)->nullable();
            $table->string(InterioresModel::CLAXON)->nullable();
            $table->string(InterioresModel::LUCES_DEL)->nullable();
            $table->string(InterioresModel::LUCES_TRAS)->nullable();
            $table->string(InterioresModel::LUCES_STOP)->nullable();
            $table->string(InterioresModel::RADIO)->nullable();
            $table->string(InterioresModel::PANTALLAS)->nullable();
            $table->string(InterioresModel::AC)->nullable();
            $table->string(InterioresModel::ENCENDEDOR)->nullable();
            $table->string(InterioresModel::VIDRIOS)->nullable();
            $table->string(InterioresModel::ESPEJOS)->nullable();
            $table->string(InterioresModel::SEGUROS_ELECTRICOS)->nullable();
            $table->string(InterioresModel::DISCO_COMPACTO)->nullable();
            $table->string(InterioresModel::ASIENTO_VESTIDURA)->nullable();
            $table->string(InterioresModel::TAPETES)->nullable();

            $table->unsignedInteger(InterioresModel::ID_INVENTARIO);
            $table->foreign(InterioresModel::ID_INVENTARIO)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop(InterioresModel::getTableName());
    }
}
