<?php

use App\Models\Notificaciones\NotificacionesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableNotificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(NotificacionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(NotificacionesModel::ID);
            $table->dateTime(NotificacionesModel::FECHA_NOTIFICACION);
            $table->string(NotificacionesModel::ASUNTO);
            $table->text(NotificacionesModel::TEXTO);
            $table->boolean(NotificacionesModel::LEIDA)->default(0);
            $table->boolean(NotificacionesModel::ELIMINADA)->default(0);
            $table->unsignedInteger(NotificacionesModel::USER_ID);
            $table->foreign(NotificacionesModel::USER_ID)
                ->references(User::ID)
                ->on(User::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(NotificacionesModel::getTableName());
    }
}
