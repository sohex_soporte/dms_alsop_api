<?php

use App\Models\CPVentas\CatStatusCPModel;
use App\Models\CPVentas\CPHistorialModel;
use App\Models\CPVentas\CPVentasModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableHistorialContactoProactivoVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CPHistorialModel::getTableName(), function (Blueprint $table) {
            $table->increments(CPHistorialModel::ID);
            $table->string(CPHistorialModel::OBSERVACIONES)->nullable();
            $table->date(CPHistorialModel::FECHA);
            $table->time(CPHistorialModel::HORA);
            $table->unsignedInteger(CPHistorialModel::ESTATUS_ID);
            $table->foreign(CPHistorialModel::ESTATUS_ID)
                ->references(CatStatusCPModel::ID)
                ->on(CatStatusCPModel::getTableName());
            $table->unsignedInteger(CPHistorialModel::USER_ID);
            $table->foreign(CPHistorialModel::USER_ID)
                ->references(User::ID)
                ->on(User::getTableName());
            $table->unsignedInteger(CPHistorialModel::CP_ID);
            $table->foreign(CPHistorialModel::CP_ID)
                ->references(CPVentasModel::ID)
                ->on(CPVentasModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CPHistorialModel::getTableName());
    }
}
