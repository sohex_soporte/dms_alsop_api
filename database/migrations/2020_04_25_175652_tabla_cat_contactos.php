<?php

use App\Models\Refacciones\CatContactos;
use App\Models\Refacciones\ClientesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaCatContactos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatContactos::getTableName(), function (Blueprint $table) {
            $table->increments(CatContactos::ID);
            $table->string(CatContactos::NOMBRE);
            $table->string(CatContactos::APELLIDO_PATERNO);
            $table->string(CatContactos::APELLIDO_MATERNO);
            $table->string(CatContactos::TELEFONO);
            $table->string(CatContactos::TELEFONO_SECUNDARIO);
            $table->string(CatContactos::CORREO);
            $table->string(CatContactos::CORREO_SECUNDARIO);
            $table->string(CatContactos::NOTAS);
            $table->unsignedInteger(CatContactos::CLIENTE_ID);
            $table->foreign(CatContactos::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatContactos::getTableName());
    }
}
