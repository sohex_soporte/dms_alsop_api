<?php

use App\Models\Autos\CatModelosModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Models\Autos\UnidadesNuevas\CatTipoBicicletas;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableDetalleRemision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DetalleRemisionModel::getTableName(), function (Blueprint $table) {
            $table->increments(DetalleRemisionModel::ID);
            $table->unsignedInteger(DetalleRemisionModel::REMISIONID);
            $table->foreign(DetalleRemisionModel::REMISIONID)
                ->references(RemisionModel::ID)
                ->on(RemisionModel::getTableName());
            $table->string(DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO);
            $table->string(DetalleRemisionModel::NO_IDENTIFICACION);
            $table->float(DetalleRemisionModel::CANTIDAD);
            $table->string(DetalleRemisionModel::CLAVE_UNIDAD);
            $table->string(DetalleRemisionModel::UNIDAD);
            $table->string(DetalleRemisionModel::DESCRIPCION);
            $table->string(DetalleRemisionModel::VALOR_UNITARIO);
            $table->string(DetalleRemisionModel::IMPORTE);
            $table->string(DetalleRemisionModel::BRAND);
            $table->unsignedInteger(DetalleRemisionModel::COLOR_ID);
            $table->foreign(DetalleRemisionModel::COLOR_ID)
                ->references(CatalogoColoresModel::ID)
                ->on(CatalogoColoresModel::getTableName());

            $table->unsignedInteger(DetalleRemisionModel::TYPE_ID);
            $table->foreign(DetalleRemisionModel::TYPE_ID)
                ->references(CatTipoBicicletas::ID)
                ->on(CatTipoBicicletas::getTableName());

            $table->unsignedInteger(DetalleRemisionModel::MODELO_ID);
            $table->foreign(DetalleRemisionModel::MODELO_ID)
                ->references(CatModelosModel::ID)
                ->on(CatModelosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DetalleRemisionModel::getTableName());
    }
}
