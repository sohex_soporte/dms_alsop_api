<?php

use App\Models\Autos\TipoDocumentosVentaModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumentosVentaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_documentos = [
            [TipoDocumentosVentaModel::NOMBRE => "PEDIDO"],
            [TipoDocumentosVentaModel::NOMBRE => "REMISION"],
            [TipoDocumentosVentaModel::NOMBRE => "IFE"],
            [TipoDocumentosVentaModel::NOMBRE => "CURP"],
            [TipoDocumentosVentaModel::NOMBRE => "COMPROBANTE DE DOMICILIO"],
            [TipoDocumentosVentaModel::NOMBRE => "FORMATO ID DEL CLT"],
            [TipoDocumentosVentaModel::NOMBRE => "RECIBO DE CAJA DEL ANTICIPO"],
            [TipoDocumentosVentaModel::NOMBRE => "FORMATO DE BONO FORD (FACTURACION TRADICIONAL)"],
            [TipoDocumentosVentaModel::NOMBRE => "CONSTANCIA DE SITUACIÓN FISCAL"],
            [TipoDocumentosVentaModel::NOMBRE => "HOJA SICOP (FICHA DE SEGUIMIENTO)"],
            [TipoDocumentosVentaModel::NOMBRE => "CORREO DE CONTACTO DEL CLIENTE"],
            [TipoDocumentosVentaModel::NOMBRE => "VoBo AREA DE PROCESOS (MAIL)"],
            [TipoDocumentosVentaModel::NOMBRE => "ACTA CONSTITUTIVA PERSONA MORAL"],
            [TipoDocumentosVentaModel::NOMBRE => "CONSTANCIA DE SITUACIÓN FISCAL"],
            [TipoDocumentosVentaModel::NOMBRE => "COMPROBANTE DE DOMICILIO"],
            [TipoDocumentosVentaModel::NOMBRE => "PODER REPESENTANTE LEGAL"],
            [TipoDocumentosVentaModel::NOMBRE => "IFE REPRESENTANTE LEGAL"],
            [TipoDocumentosVentaModel::NOMBRE => "CURP REPRESENTANTE LEGAL"],
            [TipoDocumentosVentaModel::NOMBRE => "CONSULTA DE LISTAS NEGRAS"],
            [TipoDocumentosVentaModel::NOMBRE => "CARTA RENUNCIA EXT. GARANTIA"],
            [TipoDocumentosVentaModel::NOMBRE => "ORDEN DE COMPRA"],
            [TipoDocumentosVentaModel::NOMBRE => "COTIZACION FORD"],
            [TipoDocumentosVentaModel::NOMBRE => "BOLETIN STREET PROGRAM"],
            [TipoDocumentosVentaModel::NOMBRE => "APROBACION FORD CREDIT"],
            [TipoDocumentosVentaModel::NOMBRE => "RECIBO DE CAJA DEL ANTICIPO o ENG"],
            [TipoDocumentosVentaModel::NOMBRE => "CARATULA DE FIRMA OTROS BANCOS"],
            [TipoDocumentosVentaModel::NOMBRE => "FORMATO DE BONO FORD CREDIT"],
            [TipoDocumentosVentaModel::NOMBRE => "FORMATO DE BONO FORD (FORD CREDIT, BANCOS Y AUTOPCION)"],
            [TipoDocumentosVentaModel::NOMBRE => "CARTA CREDITO AUTORIZADA"],
            [TipoDocumentosVentaModel::NOMBRE => "APROBACION DE COND. AUTO OPCION"],
            [TipoDocumentosVentaModel::NOMBRE => "RECIBO DE CAJA DE PAGO CLIENTE"],
            [TipoDocumentosVentaModel::NOMBRE => "FORMATO DE BONO FORD (CONAUTO)"]
        ];

        foreach ($tipo_documentos as $key => $value) {
            DB::table(TipoDocumentosVentaModel::getTableName())->insert([
                TipoDocumentosVentaModel::NOMBRE => $value[TipoDocumentosVentaModel::NOMBRE]
            ]);
        }
    }
}
