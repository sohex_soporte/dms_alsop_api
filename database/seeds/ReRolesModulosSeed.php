<?php

use App\Models\Usuarios\ReRolesModulosModel;
use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReRolesModulosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->modulosModel = new ModulosModel();
        $modulos = $this->modulosModel->get();

        foreach ($modulos as $modulo) {

            DB::table(ReRolesModulosModel::getTableName())->insert([
                ReRolesModulosModel::ROL_ID => RolModel::ROL_ADMIN,
                ReRolesModulosModel::MODULO_ID => $modulo->id
            ]);
        }

        $data = [
            [
                ReRolesModulosModel::ROL_ID => RolModel::ROL_REFACCIONES,
                ReRolesModulosModel::MODULO_ID => ModulosModel::REFACCIONES
            ],
            [
                ReRolesModulosModel::ROL_ID => RolModel::ROL_CAJA,
                ReRolesModulosModel::MODULO_ID => ModulosModel::CAJA
            ],
            [
                ReRolesModulosModel::ROL_ID => RolModel::ROL_CONTABILIDAD,
                ReRolesModulosModel::MODULO_ID => ModulosModel::CONTABILIDAD
            ]
        ];

        foreach ($data as $items) {
            DB::table(ReRolesModulosModel::getTableName())->insert($items);
        }
    }
}
