<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTipoJornadaImssModel as Model;

class CaTipoJornadaImssSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1,Model::Descripcion => 'Sin semana o jornada reducida'],
            [Model::ID => 2,Model::Descripcion => '1'],
            [Model::ID => 3,Model::Descripcion => '2'],
            [Model::ID => 4,Model::Descripcion => '3'],
            [Model::ID => 5,Model::Descripcion => '4'],
            [Model::ID => 6,Model::Descripcion => '5'],
            [Model::ID => 7,Model::Descripcion => 'Jornada reducida']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
