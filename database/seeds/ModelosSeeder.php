<?php

use App\Models\Autos\CatModelosModel;
use Illuminate\Database\Seeder;

class ModelosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = 'csv/modelos.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {

                $nombre = trim($data[1]);
                $clave = trim($data[2]);
                $year = trim($data[3]);
                $descripcion = trim($data[4]);
                $precio = trim($data[5]);
                $id_marca = trim($data[6]);
                $tiempo_lavado = trim($data[7]);

                CatModelosModel::create([
                    CatModelosModel::NOMBRE =>  $nombre,
                    CatModelosModel::CLAVE =>  $clave,
                    CatModelosModel::YEAR =>  $year,
                    CatModelosModel::DESCRIPCION =>  $descripcion,
                    CatModelosModel::PRECIO => $precio,
                    CatModelosModel::ID_MARCA => $id_marca,
                    CatModelosModel::TIEMPO_LAVADO => $tiempo_lavado,
                ]);
            }
        }
    }
}
