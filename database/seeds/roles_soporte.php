<?php

use App\Models\Soporte\CatRolesSoporteModel;
use Illuminate\Database\Seeder;

class roles_soporte extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = 'csv/roles_soporte.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {

                $id = trim($data[0]);
                $rol = trim($data[1]);
                CatRolesSoporteModel::create([
                    CatRolesSoporteModel::ID => $id,
                    CatRolesSoporteModel::ROL => $rol
                ]);
            }
        }
    }
}
