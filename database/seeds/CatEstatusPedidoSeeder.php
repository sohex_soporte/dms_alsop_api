<?php

use App\Models\Refacciones\TipoPedidoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatTipoPedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                TipoPedidoModel::NOMBRE => 'PROCESO',
                TipoPedidoModel::CLAVE => 'PROCESO'
            ],
            [
                TipoPedidoModel::NOMBRE => 'STOCK',
                TipoPedidoModel::CLAVE => 'STOCK'
            ],
            [
                TipoPedidoModel::NOMBRE => 'COMPLEMENTARIO',
                TipoPedidoModel::CLAVE => 'COMPLE'
            ],
            [
                TipoPedidoModel::NOMBRE => 'DIRECT',
                TipoPedidoModel::CLAVE => 'DIRECTO'
            ],
            [
                TipoPedidoModel::NOMBRE => 'EMERGENCIA',
                TipoPedidoModel::CLAVE => 'EMERGE'
            ],
            [
                TipoPedidoModel::NOMBRE => 'UNIDADES INMOVIL',
                TipoPedidoModel::CLAVE => 'UNINMO'
            ],
            [
                TipoPedidoModel::NOMBRE => 'CONTADO',
                TipoPedidoModel::CLAVE => 'CONTAD'
            ],
            [
                TipoPedidoModel::NOMBRE => 'BACK ORDER',
                TipoPedidoModel::CLAVE => 'BACKOR'
            ],

            [
                TipoPedidoModel::NOMBRE => 'PEDIDO SUGERIDO',
                TipoPedidoModel::CLAVE => 'SUGERIDO'
            ],
            [
                TipoPedidoModel::NOMBRE => 'BORRADO SUGERIDO',
                TipoPedidoModel::CLAVE => 'BORRADO'
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(TipoPedidoModel::getTableName())
                ->insert($items);
        }
    }
}
