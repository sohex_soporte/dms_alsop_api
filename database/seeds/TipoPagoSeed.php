<?php

use App\Models\Refacciones\CatTipoPagoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPagoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $almacenes = [
            [
                CatTipoPagoModel::ID => 1,
                CatTipoPagoModel::NOMBRE => 'Efectivo',
                CatTipoPagoModel::CLAVE => '01',

            ],
            [
                CatTipoPagoModel::ID => 2,
                CatTipoPagoModel::NOMBRE => 'Cheque',
                CatTipoPagoModel::CLAVE => '02',
            ],
            [
                CatTipoPagoModel::ID => 3,
                CatTipoPagoModel::NOMBRE => 'Transferencia Electrónica de fondos',
                CatTipoPagoModel::CLAVE => '03',
            ],
            [
                CatTipoPagoModel::ID => 4,
                CatTipoPagoModel::NOMBRE => 'Tarjeta de crédito',
                CatTipoPagoModel::CLAVE => '04',
            ],
            [
                CatTipoPagoModel::ID => 5,
                CatTipoPagoModel::NOMBRE => 'Monedero electrónico',
                CatTipoPagoModel::CLAVE => '05',
            ],
            [
                CatTipoPagoModel::ID => 6,
                CatTipoPagoModel::NOMBRE => 'Dinero electrónico',
                CatTipoPagoModel::CLAVE => '06',
            ],
            [
                CatTipoPagoModel::ID => 7,
                CatTipoPagoModel::NOMBRE => 'Vales de despesa',
                CatTipoPagoModel::CLAVE => '08',
            ],
            [
                CatTipoPagoModel::ID => 8,
                CatTipoPagoModel::NOMBRE => 'Tarjeta de débito',
                CatTipoPagoModel::CLAVE => '28',
            ],
            [
                CatTipoPagoModel::ID => 9,
                CatTipoPagoModel::NOMBRE => 'Tarjeta de servicio',
                CatTipoPagoModel::CLAVE => '29',
            ],
            [
                CatTipoPagoModel::ID => 10,
                CatTipoPagoModel::NOMBRE => 'Tarjeta de servicio',
                CatTipoPagoModel::CLAVE => '99',
            ]
        ];

        foreach ($almacenes as $key => $value) {
            DB::table(CatTipoPagoModel::getTableName())->insert([
                CatTipoPagoModel::ID => $value[CatTipoPagoModel::ID],
                CatTipoPagoModel::NOMBRE => $value[CatTipoPagoModel::NOMBRE],
                CatTipoPagoModel::CLAVE => $value[CatTipoPagoModel::CLAVE],
            ]);
        }
    }
}
