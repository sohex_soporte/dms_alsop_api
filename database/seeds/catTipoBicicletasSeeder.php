<?php

use App\Models\Autos\UnidadesNuevas\CatTipoBicicletas;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class catTipoBicicletasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatTipoBicicletas::ID => 1,
                CatTipoBicicletas::TIPO_BICILETA => 'Montaña'
            ],
            [
                CatTipoBicicletas::ID => 2,
                CatTipoBicicletas::TIPO_BICILETA => 'Ruta'
            ],
            [
                CatTipoBicicletas::ID => 3,
                CatTipoBicicletas::TIPO_BICILETA => 'Eléctrica'
            ],
            [
                CatTipoBicicletas::ID => 4,
                CatTipoBicicletas::TIPO_BICILETA => 'Fitness'
            ],
            [
                CatTipoBicicletas::ID => 5,
                CatTipoBicicletas::TIPO_BICILETA => 'Niños'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatTipoBicicletas::getTableName())->insert($value);
        }
    }
}
