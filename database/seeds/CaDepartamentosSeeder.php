<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaDepartamentoModel as Model;

class CaDepartamentosSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1, Model::Clave => 1,Model::Descripcion => 'DEPARTAMENTO no. 1'],
            [Model::ID => 2, Model::Clave => 2,Model::Descripcion => 'DEPARTAMENTO no. 2'],
            [Model::ID => 3, Model::Clave => 3,Model::Descripcion => 'DEPARTAMENTO no. 3'],
            [Model::ID => 4, Model::Clave => 4,Model::Descripcion => 'DEPARTAMENTO no. 4'],
            [Model::ID => 5, Model::Clave => 5,Model::Descripcion => 'DEPARTAMENTO no. 5']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
