<?php

use App\Models\CuentasPorCobrar\TipoPolizasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPolizaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_polizas = [
            [
                TipoPolizasModel::CLAVE => 'C0',
                TipoPolizasModel::DESCRIPCION => 'Compra de refacciones'
            ],
            [
                TipoPolizasModel::CLAVE => 'B',
                TipoPolizasModel::DESCRIPCION => 'Bancos'
            ],
            [
                TipoPolizasModel::CLAVE => 'DQ',
                TipoPolizasModel::DESCRIPCION => 'Cancelacioness de refacciones de proceso a almacén'
            ],
            [
                TipoPolizasModel::CLAVE => 'CA',
                TipoPolizasModel::DESCRIPCION => 'Caja auxiliar'
            ],
            [
                TipoPolizasModel::CLAVE => 'CG',
                TipoPolizasModel::DESCRIPCION => 'Caja general Queretaro'
            ],
            [
                TipoPolizasModel::CLAVE => 'CH',
                TipoPolizasModel::DESCRIPCION => 'Cancelaciones de hojalatería'
            ],
            [
                TipoPolizasModel::CLAVE => 'CJ',
                TipoPolizasModel::DESCRIPCION => 'Caja general San Juan'
            ],
            [
                TipoPolizasModel::CLAVE => 'CM',
                TipoPolizasModel::DESCRIPCION => 'Caja de refacciones Queretaro'
            ],
            [
                TipoPolizasModel::CLAVE => 'CN',
                TipoPolizasModel::DESCRIPCION => 'Compra de unidades nuevas'
            ],
            [
                TipoPolizasModel::CLAVE => 'CQ',
                TipoPolizasModel::DESCRIPCION => 'Cancelaciones de ventas refacciones mostrador'
            ],
            [
                TipoPolizasModel::CLAVE => 'CS',
                TipoPolizasModel::DESCRIPCION => 'Compras refacciones de San Juan los traspaso de Queretaro a San Juan'
            ],
            [
                TipoPolizasModel::CLAVE => 'DO',
                TipoPolizasModel::DESCRIPCION => 'Diario'
            ],
            [
                TipoPolizasModel::CLAVE => 'FH',
                TipoPolizasModel::DESCRIPCION => 'Devoluciones del almacén de San Juan a Queretaro'
            ],
            [
                TipoPolizasModel::CLAVE => 'GT',
                TipoPolizasModel::DESCRIPCION => 'Cancelaciones de ordenes de facturación de Queretaro'
            ],
            [
                TipoPolizasModel::CLAVE => 'OG',
                TipoPolizasModel::DESCRIPCION => 'Facturación de garantías San Juan'
            ],
            [
                TipoPolizasModel::CLAVE => 'OT',
                TipoPolizasModel::DESCRIPCION => 'Ventas de servicio San Juan'
            ],
            [
                TipoPolizasModel::CLAVE => 'QG',
                TipoPolizasModel::DESCRIPCION => 'Facturación de garantías de Queretaro'
            ],
            [
                TipoPolizasModel::CLAVE => 'QH',
                TipoPolizasModel::DESCRIPCION => 'Facturación de hojalatería'
            ],
            [
                TipoPolizasModel::CLAVE => 'QT',
                TipoPolizasModel::DESCRIPCION => 'Facturación de servicio de Queretaro'
            ],
            [
                TipoPolizasModel::CLAVE => 'V0',
                TipoPolizasModel::DESCRIPCION => 'Traspasos de inventario a proceso'
            ],
            [
                TipoPolizasModel::CLAVE => 'VN',
                TipoPolizasModel::DESCRIPCION => 'Autos nuevos'
            ],
            [
                TipoPolizasModel::CLAVE => 'VR',
                TipoPolizasModel::DESCRIPCION => 'Traspaso del almacén de san Juan a proceso'
            ]      
        ];

        foreach ($tipo_polizas as $value) {
            DB::table(TipoPolizasModel::getTableName())->insert([
                TipoPolizasModel::CLAVE => $value[TipoPolizasModel::CLAVE],
                TipoPolizasModel::DESCRIPCION => $value[TipoPolizasModel::DESCRIPCION]
            ]);
        }
    }
}
