<?php

use App\Models\Autos\EstatusSalidaUnidadModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class table_estatus_salida_unidad extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estatus_salidas = [
            [
                EstatusSalidaUnidadModel::ID => 1,
                EstatusSalidaUnidadModel::NOMBRE => 'Disponible'
            ],
            [
                EstatusSalidaUnidadModel::ID => 2,
                EstatusSalidaUnidadModel::NOMBRE => 'Apartada'
            ],
            [
                EstatusSalidaUnidadModel::ID => 3,
                EstatusSalidaUnidadModel::NOMBRE => 'Vendida'
            ]
        ];

        foreach ($estatus_salidas as $value) {
            DB::table(EstatusSalidaUnidadModel::getTableName())->insert([
                EstatusSalidaUnidadModel::ID => $value[EstatusSalidaUnidadModel::ID],
                EstatusSalidaUnidadModel::NOMBRE => $value[EstatusSalidaUnidadModel::NOMBRE]
            ]);
        }
    }
}
