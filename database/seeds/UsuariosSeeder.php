<?php

use App\Models\Usuarios\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = 'csv/usuarios_new.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {

                $nombre = trim($data[1]);
                $apellido_paterno = trim($data[2]);
                $apellido_materno = trim($data[3]);
                $telefono = trim($data[4]);
                $usuario = trim($data[5]);
                $email = trim($data[6]);
                $pass = trim($data[7]);
                $estatus_id = trim($data[8]);
                $rfc = trim($data[9]);
                $rol_id = trim($data[19]);

                User::create([
                    User::NOMBRE =>  $nombre,
                    User::APELLIDO_MATERNO =>  $apellido_paterno,
                    User::APELLIDO_PATERNO =>  $apellido_materno,
                    User::TELEFONO =>  $telefono,
                    User::USUARIO => $usuario,
                    User::EMAIL => $email,
                    User::PASSWORD => $pass,
                    User::ESTATUS_ID => $estatus_id,
                    User::RFC => $rfc,
                    User::ROL_ID => $rol_id
                ]);
            }
        }
    }
}
