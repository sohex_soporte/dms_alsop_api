<?php

use App\Models\Bodyshop\CatProyectoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaBodyshopProyectosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatProyectoModel::PROYECTO => 'FILIAL'],
            [CatProyectoModel::PROYECTO => 'INTERNO'],
            [CatProyectoModel::PROYECTO => 'OTROS'],
            [CatProyectoModel::PROYECTO => 'PUBLICO'],
            [CatProyectoModel::PROYECTO => 'RECLAMACIÓN'],
            [CatProyectoModel::PROYECTO => 'SINIESTRO'],
        ];

        foreach ($data as $key => $items) {
            DB::table(CatProyectoModel::getTableName())->insert($items);
        }
    }
}
