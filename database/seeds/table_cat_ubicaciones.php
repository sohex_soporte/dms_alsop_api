<?php

use App\Models\Refacciones\CatalogoUbicacionModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class table_cat_ubicaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos_formas_pagos = [
            [
                CatalogoUbicacionModel::ID => 1,
                CatalogoUbicacionModel::NOMBRE => 'CDM'
            ],
            [
                CatalogoUbicacionModel::ID => 2,
                CatalogoUbicacionModel::NOMBRE => 'PISO QRO'
            ],
            [
                CatalogoUbicacionModel::ID => 3,
                CatalogoUbicacionModel::NOMBRE => 'PISO SJR'
            ],
            [
                CatalogoUbicacionModel::ID => 4,
                CatalogoUbicacionModel::NOMBRE => 'REPARACIÓN'
            ],
            [
                CatalogoUbicacionModel::ID => 5,
                CatalogoUbicacionModel::NOMBRE => 'DEMO'
            ]
        ];

        foreach ($tipos_formas_pagos as $value) {
            DB::table(CatalogoUbicacionModel::getTableName())->insert([
                CatalogoUbicacionModel::ID => $value[CatalogoUbicacionModel::ID],
                CatalogoUbicacionModel::NOMBRE => $value[CatalogoUbicacionModel::NOMBRE]
            ]);
        }
    }
}
