<?php

use App\Models\Caja\EstatusAnticiposModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusAnticiposSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estatus = [
            [
                EstatusAnticiposModel::NOMBRE => 'ADQUIRIDO',

            ],
            [
                EstatusAnticiposModel::NOMBRE => 'APLICADO',
            ],
            [
                EstatusAnticiposModel::NOMBRE => 'CANCELADO',
            ],
           
        ];

        foreach ($estatus as $value) {
            DB::table(EstatusAnticiposModel::getTableName())->insert([
                EstatusAnticiposModel::NOMBRE => $value[EstatusAnticiposModel::NOMBRE],
            ]);
        }
    }
}
