<?php

use App\Models\Contabilidad\CatalogoProcesosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoProcesosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data =  [
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_VENTA_MOSTRADOR',
                CatalogoProcesosModel::CLAVE_POLIZA => 'CM'
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_TRASPASO',
                CatalogoProcesosModel::CLAVE_POLIZA => ''
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_COMPRAS',
                CatalogoProcesosModel::CLAVE_POLIZA => ''
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_DEVOLUCION_PROVEEDOR',
                CatalogoProcesosModel::CLAVE_POLIZA => ''
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_DEVOLUCION_VENTA',
                CatalogoProcesosModel::CLAVE_POLIZA => ''
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_VENTA_AUTOS_NUEVOS',
                CatalogoProcesosModel::CLAVE_POLIZA => ''
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_VARIOS',
                CatalogoProcesosModel::CLAVE_POLIZA => ''
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_SERVICIOS',
                CatalogoProcesosModel::CLAVE_POLIZA => 'QT'
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_HOJALATERIA',
                CatalogoProcesosModel::CLAVE_POLIZA => 'QH'
            ],
            [
                CatalogoProcesosModel::DESCRIPCION => 'PROCESO_VENTA_GARANTIAS',
                CatalogoProcesosModel::CLAVE_POLIZA => 'QG'
            ],

        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoProcesosModel::getTableName())
                ->insert($items);
        }
    }
}
