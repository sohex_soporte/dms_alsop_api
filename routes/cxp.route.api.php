

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO AUTOS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'cuentas-por-pagar'], function () {
    Route::get('/', 'CuentasPorPagar\CuentasPorPagarController@listado');
    Route::get('/getkardexpagos', 'CuentasPorPagar\CuentasPorPagarController@getKardexPagos');
    Route::get('/verifica-pagos-morosos', 'CuentasPorPagar\CuentasPorPagarController@getVerificaCuentasMorosas');
    Route::get('/aviso-pago', 'CuentasPorPagar\CuentasPorPagarController@envioAvisoPago');
    Route::get('/{id}', 'CuentasPorPagar\CuentasPorPagarController@show');
    Route::get('buscar-por-folio-id/{folio_id}', 'CuentasPorPagar\CuentasPorPagarController@showByFolioId');
    Route::post('/', 'CuentasPorPagar\CuentasPorPagarController@store');
    Route::put('/{id}', 'CuentasPorPagar\CuentasPorPagarController@update');
    Route::put('/cancelar-cuenta/{id}', 'CuentasPorPagar\CuentasPorPagarController@cancelarCuentaById');
    Route::delete('/{id}', 'CuentasPorPagar\CuentasPorPagarController@destroy');
});

Route::group(['prefix' => 'abonos-por-pagar'], function () {
    Route::get('abonos-by-orden-entrada', 'CuentasPorPagar\AbonoPorPagarController@showByIdOrdenEntrada');
    Route::get('listado-abonos-by-orden-entrada', 'CuentasPorPagar\AbonoPorPagarController@listadoAbonosByOrdenEntrada');
    Route::get('/', 'CuentasPorPagar\AbonoPorPagarController@index');
    Route::get('/{id}', 'CuentasPorPagar\AbonoPorPagarController@show');
    Route::post('/', 'CuentasPorPagar\AbonoPorPagarController@store');
    Route::put('/{id}', 'CuentasPorPagar\AbonoPorPagarController@update');
    Route::delete('/{id}', 'CuentasPorPagar\AbonoPorPagarController@destroy');
});

Route::group(['prefix' => 'tipo-abono'], function () {
    Route::get('/', 'CuentasPorPagar\CatTipoAbonoController@index');
    Route::get('/{id}', 'CuentasPorPagar\CatTipoAbonoController@show');
    Route::post('/', 'CuentasPorPagar\CatTipoAbonoController@store');
    Route::put('/{id}', 'CuentasPorPagar\CatTipoAbonoController@update');
    Route::delete('/{id}', 'CuentasPorPagar\CatTipoAbonoController@destroy');
});

Route::group(['prefix' => 'estatus-abono'], function () {
    Route::get('/', 'CuentasPorPagar\CatEstatusAbonoController@index');
    Route::get('/{id}', 'CuentasPorPagar\CatEstatusAbonoController@show');
    Route::post('/', 'CuentasPorPagar\CatEstatusAbonoController@store');
    Route::put('/{id}', 'CuentasPorPagar\CatEstatusAbonoController@update');
    Route::delete('/{id}', 'CuentasPorPagar\CatEstatusAbonoController@destroy');
});

Route::group(['prefix' => 'pedidos'], function () {
    Route::post('/proveedor', 'Refacciones\ProveedorRefaccionesController@getPedidosByProveedor');
});
