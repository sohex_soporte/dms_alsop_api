

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO AUTOS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'catalogo-caja'], function () {
    Route::get('/', 'Caja\CatalogoCajaController@index');
    Route::get('/{id}', 'Caja\CatalogoCajaController@show');
    Route::post('/', 'Caja\CatalogoCajaController@store');
    Route::put('/{id}', 'Caja\CatalogoCajaController@update');
    Route::delete('/{id}', 'Caja\CatalogoCajaController@destroy');
});

Route::group(['prefix' => 'corte-caja'], function () {
    Route::get('/', 'Caja\CorteCajaController@index');
    Route::get('/getPolizas', 'Caja\CorteCajaController@getPolizas');
    Route::get('/getByUsuario', 'Caja\CorteCajaController@getByUsuario');
    Route::get('/mostrarPorUsuarioAndFecha', 'Caja\CorteCajaController@mostrarPorUsuarioAndFecha');
    Route::get('/{id}', 'Caja\CorteCajaController@show');
    Route::post('/', 'Caja\CorteCajaController@store');
    Route::put('/{id}', 'Caja\CorteCajaController@update');
    Route::delete('/{id}', 'Caja\CorteCajaController@destroy');
});

Route::group(['prefix' => 'detalle-corte-caja'], function () {
    Route::get('/', 'Caja\DetalleCorteController@index');
    Route::get('/getByCajaId', 'Caja\DetalleCorteController@getByCajaId');
    Route::get('/{id}', 'Caja\DetalleCorteController@show');
    Route::post('/', 'Caja\DetalleCorteController@store');
    Route::put('/{id}', 'Caja\DetalleCorteController@update');
    Route::delete('/{id}', 'Caja\DetalleCorteController@destroy');
});

Route::group(['prefix' => 'contabilidad'], function () {
    Route::get('/', 'Contabilidad\DetalleCorteController@index');
    Route::get('/getByCajaId', 'Contabilidad\DetalleCorteController@getByCajaId');
    Route::get('/{id}', 'Contabilidad\DetalleCorteController@show');
    Route::post('/', 'Contabilidad\DetalleCorteController@store');
    Route::put('/{id}', 'Contabilidad\DetalleCorteController@update');
    Route::delete('/{id}', 'Contabilidad\DetalleCorteController@destroy');
});

Route::group(['prefix' => 'anticipos'], function () {
    Route::get('/get-filter', 'Caja\AnticiposController@getFilter');
    Route::get('/get-folio-id/{id}', 'Caja\AnticiposController@getFilter');
    Route::get('/get-total-folio/{id}', 'Caja\AnticiposController@getByTotalByFolioId');
    Route::get('/get-id/{id}', 'Caja\AnticiposController@getByAnticipoByFolioId');
    Route::put('/cambiar-estatus/{id}', 'Caja\AnticiposController@updateEstatus');
    Route::put('/aplicar-anticipo/{id}', 'Caja\AnticiposController@aplicarAnticipo');
});
Route::resource('anticipos', 'Caja\AnticiposController');

Route::group(['prefix' => 'descuentos'], function () {
    Route::get('/get-total-folio/{id}', 'Caja\DescuentosController@getByTotalByFolioId');
    Route::get('/get-folio-tipo', 'Caja\DescuentosController@getByDescuentoByFolioTipo');
    Route::put('/aplicar-descuento/{id}', 'Caja\DescuentosController@aplicarDescuento');
});
Route::resource('descuentos', 'Caja\DescuentosController');

Route::resource('estatus-anticipos', 'Caja\EstatusAnticiposController');
Route::resource('procesos-contabilidad', 'Contabilidad\ProcesosController');
