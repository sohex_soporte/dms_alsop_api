<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'contacto-proactivo-ventas/master'], function () {
    Route::post('/get-all', 'CPVentas\VentasCPController@getAll');
});
Route::resource('contacto-proactivo-ventas', 'CPVentas\VentasCPController');
Route::resource('estatus-contacto-proactivo', 'CPVentas\CatStatusCPController');
Route::group(['prefix' => 'historial-proactivo-ventas/master'], function () {
    Route::post('/get-historial', 'CPVentas\HistorialCPController@getHistorial');
});
Route::resource('historial-contacto-proactivo', 'CPVentas\HistorialCPController');


