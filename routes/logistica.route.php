<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::resource('logistica/catalogos/estatus-administrativo', 'Logistica\CatEstatusAdmvoController');
Route::resource('logistica/catalogos/estatus-danios-bodyshop', 'Logistica\CatEstatusDaniosBodyshopController');
Route::resource('logistica/catalogos/agencias', 'Logistica\CatAgenciasController');
Route::resource('logistica/catalogos/areas-reparacion', 'Logistica\CatAreaReparacionController');
Route::resource('logistica/catalogos/tipo-severidad', 'Logistica\CatTipoSeveridadController');
Route::resource('logistica/catalogos/estatus-sas-seguros', 'Logistica\CatEstatusSasSegurosController');
Route::resource('logistica/catalogos/cat-modos', 'Logistica\CatModosController');
Route::resource('logistica/catalogos/cat-carriles', 'Logistica\CatCarrilesController');

Route::group(['prefix' => 'logistica/danios-bodyshop'], function () {
    Route::post('/get-all', 'Logistica\DaniosBodyshopController@getByParametros');
});
Route::resource('logistica/danios-bodyshop', 'Logistica\DaniosBodyshopController');

Route::group(['prefix' => 'logistica/faltantes'], function () {
    Route::post('/get-all', 'Logistica\FaltantesController@getByParametros');
});
Route::resource('logistica/faltantes', 'Logistica\FaltantesController');

Route::group(['prefix' => 'logistica/sas-seguros'], function () {
    Route::post('/get-all', 'Logistica\SasSegurosController@getByParametros');
});
Route::resource('logistica/sas-seguros', 'Logistica\SasSegurosController');

Route::group(['prefix' => 'logistica/master/'], function () {
    Route::post('/get-all', 'Logistica\LogisticaController@getByParametros');
    Route::post('/get-horarios', 'Logistica\LogisticaController@getHorarios');
    Route::post('/horarios-ocupados', 'Logistica\LogisticaController@horariosOCupados');
    Route::post('/existe-reservacion', 'Logistica\LogisticaController@existeReservacion');
    
});
Route::resource('logistica', 'Logistica\LogisticaController');

Route::group(['prefix' => 'logistica/archivos-sas-seguros'], function () {
    Route::post('/upload', 'Logistica\ArchivosSasSegurosController@uploadFile');
    Route::get('/{seguro_id}', 'Logistica\ArchivosSasSegurosController@getFiles');
});