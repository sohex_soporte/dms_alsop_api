<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::group(['prefix' => 'oasis/master/'], function () {
//     Route::post('/get-all', 'Oasis\OasisController@getAll');
// });

Route::resource('catalogo-prioridades', 'Soporte\CatEstatusPrioridadesController');
Route::resource('catalogo-roles-soporte', 'Soporte\CatRolesController');
Route::resource('estatus-tickets', 'Soporte\CatEstatusTicketsController');


Route::group(['prefix' => 'tickets/historial'], function () {
    Route::get('/get-all-historial/{id}', 'Soporte\HistorialTicketsController@getAllHistorial');
    Route::get('/get-by-folio/{folio}', 'Soporte\HistorialTicketsController@getByFolio');
});


Route::resource('historial-tickets', 'Soporte\HistorialTicketsController');
Route::resource('tickets', 'Soporte\TicketsController');




Route::group(['prefix' => 'tickets/master'], function () {
    Route::post('/upload', 'Soporte\TicketsController@uploadTicket');
    Route::post('/get-all', 'Soporte\TicketsController@getAll');
    Route::put('/update-ticket/{id}', 'Soporte\TicketsController@updateTicket');
});


Route::resource('evidencia-tickets', 'Soporte\EvidenciaTicketController');
Route::group(['prefix' => 'evidencia-tickets/master'], function () {
    Route::post('/upload', 'Soporte\EvidenciaTicketController@uploadEvidencia');
    Route::get('/evidencia-ticket/{id}', 'Soporte\EvidenciaTicketController@getEvidenciaTicket');
});
