<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO CARDEX Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'kardex/tipo-movimiento'], function () {
    Route::get('/', 'Refacciones\Kardex\TipoMovimientosController@index');
    Route::get('/{id}', 'Refacciones\Kardex\TipoMovimientosController@show');
    Route::post('/', 'Refacciones\Kardex\TipoMovimientosController@store');
    Route::put('/{id}', 'Refacciones\Kardex\TipoMovimientosController@update');
    Route::delete('/{id}', 'Refacciones\Kardex\TipoMovimientosController@destroy');
});

Route::group(['prefix' => 'kardex'], function () {
    Route::post('/ventas/filtrar', 'Refacciones\VentasRealizadasController@ventasFiltrarWithDetails');//hacer pruebas
    Route::get('/getProducto', 'Refacciones\Kardex\KardexController@showProducto');
});

Route::group(['prefix' => 'pedido-sugerido'], function () {
    Route::get('/', 'Refacciones\PedidoSugerido\PedidoSugeridoController@sugeridoRapido');
    Route::get('/lento', 'Refacciones\PedidoSugerido\PedidoSugeridoController@sugeridoLento');
    Route::get('/inactivo', 'Refacciones\PedidoSugerido\PedidoSugeridoController@sugeridoInactivo');
    Route::get('/potencial-obsoleto', 'Refacciones\PedidoSugerido\PedidoSugeridoController@sugeridoPotencialObsoleto');
    Route::get('/estancado', 'Refacciones\PedidoSugerido\PedidoSugeridoController@sugeridoEstancado');
    Route::get('/obsoleto', 'Refacciones\PedidoSugerido\PedidoSugeridoController@sugeridoObsoleto');
    Route::get('/byproducto/{id}', 'Refacciones\PedidoSugerido\PedidoSugeridoController@categorizaporproducto');
    Route::get('/graficavalues', 'Refacciones\PedidoSugerido\PedidoSugeridoController@graficapedidosugerido');

    Route::get('/listadoproductos', 'Refacciones\PedidoSugerido\PedidoSugeridoController@listadoproductospedido');
    Route::get('/generatelayout', 'Refacciones\PedidoSugerido\PedidoSugeridoController@generatelayout');
});


