@extends('mails.template')

@section('content')
<h3>Factura proximas a vencer</h3>
<p>
        Estimado cliente: {{ $demo->numero_cliente}} {{ $demo->nombre}}.<br/>
        Este mail es para recordarle que la siguiente factura con número de folio: {{ $demo->folio }} por el
        concepto de: {{ $demo->concepto }} vence el día {{ date('d/m/y', strtotime($demo->fecha_vencimiento)) }} por lo que agradecemos su pago por un total de {{ '$'. (number_format($demo->total_abono,2)) }}. Gracias<br/>
        Clic en el enlace para descargar el estado de cuenta <a href="<?php echo 'https://sohexdms.net/cs/companycars/dms_companycars/caja/entradas/imprime_estado_cuenta?cuenta_id='.base64_encode($demo->cuenta_por_cobrar_id);?>">Estado de cuenta</a>
</p>
@endsection