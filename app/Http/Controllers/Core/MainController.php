<?php

namespace App\Http\Controllers\Core;

use App\Servicios\Core\MensajesConstantes;


abstract class MainController extends Controller
{
    use MensajesConstantes;
    protected $servicio;
}
