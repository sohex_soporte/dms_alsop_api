<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioAsientos;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Exceptions\ParametroHttpInvalidoException;
use Illuminate\Http\Request;


class AsientosController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioAsientos();
	}

	public function setAsientoContabilidad(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
			$modelo = $this->servicio->curl_asiento_api($request->all());
			$respuesta = (json_decode($modelo));
			if ($respuesta->status == 'success') {
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				return Respuesta::json($respuesta, 201, $mensaje);
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "Ocurrio un error al crear el asiento"
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaHojalateria(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaHojalateria());
			$modelo = $this->servicio->servicioPolizaHojalateria($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaServicio(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaServicio());
			$modelo = $this->servicio->servicioPolizaServicios($request->toArray());
			if ($modelo) {
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				return Respuesta::json($modelo, 201, $mensaje);
			} else {
				throw new ParametroHttpInvalidoException([
					'msg' => "El cargo y abono no coinciden favor de verificar"
				]);
			}
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
	public function registrarActualizaPoliza(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasActualizaPolizaServicio());
			$modelo = $this->servicio->servicioActualizaPoliza($request->get('cuenta_por_cobrar_id'),$request->get('tipo_descuento'));
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaVentaMostrador(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaVentaMostrador());
			$modelo = $this->servicio->servicioPolizaVentaMostrador($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaVentaMostradorCredito(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaVentaMostrador());
			$modelo = $this->servicio->servicioPolizaVentaMostradorCredito($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaGarantias(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaGarantias());
			$modelo = $this->servicio->servicioPolizaGarantias($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaServicioInterno(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaServicioInterno());
			$modelo = $this->servicio->servicioPolizaServicioInterno($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaRefaccionTaller(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaRefaccionTaller());
			$modelo = $this->servicio->servicioPolizaRefaccionTaller($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaCompraFordPlanta(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaCompraFordPlanta());
			$modelo = $this->servicio->servicioPolizaCompraFordPlanta($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function registrarPolizaCompraProveedorExterno(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasPolizaCompraProveedorExterno());
			$modelo = $this->servicio->servicioPolizaCompraProveedorExterno($request->toArray());
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
}
