<?php

namespace App\Http\Controllers\Bodyshop;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Bodyshop\ServicioBodyshop;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class BodyshopController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioBodyshop();
    }
    public function horariosOCupados(Request $request)
    {
        try {
            $modelo = $this->servicio->getHorariosOcupadosCita($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function existeReservacion(Request $request){
        try {
            $modelo = $this->servicio->validarReservacion($request->all());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
