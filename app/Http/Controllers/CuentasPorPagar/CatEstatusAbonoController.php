<?php

namespace App\Http\Controllers\CuentasPorPagar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorPagar\ServicioCatEstatusAbonos;

class CatEstatusAbonoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatEstatusAbonos();
    }

}
