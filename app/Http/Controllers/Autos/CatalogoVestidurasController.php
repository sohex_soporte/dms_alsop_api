<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\ServicioCatalogoVestiduras;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class CatalogoVestidurasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatalogoVestiduras();
    }

    public function buscar_id($nombre)
    {
        try {
            $modelo = $this->servicio->searchIdVestidura($nombre);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
} 
