<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioCatTipoBicicletas;
use Illuminate\Http\Request;

class CatTipoBicicletasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatTipoBicicletas();
    }
}
