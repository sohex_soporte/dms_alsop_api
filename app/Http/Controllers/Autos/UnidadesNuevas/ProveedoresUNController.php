<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioProveedoresUn;
use Illuminate\Http\Request;

class ProveedoresUNController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProveedoresUn();
    }
}
