<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;
use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\UnidadesNuevas\HistorialStatusUnidadesModel;
use App\Servicios\Autos\UnidadesNuevas\ServicioHistorialEstatusRecepcion;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use Illuminate\Http\Request;

class HistorialEstatusUNController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioHistorialEstatusRecepcion();
        $this->servicioArchivos = new ServicioManejoArchivos();
    }
    public function getHistorialByUnidad($id_unidad)
    {
        $modelo = $this->servicio->getHistorialId($id_unidad);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            if($request->file(HistorialStatusUnidadesModel::IMAGEN)!=null){
                $imagen = $request->file(HistorialStatusUnidadesModel::IMAGEN);
                $newFileName = $this->servicioArchivos->setFileName($imagen);
                $directorio = $this->servicioArchivos->setDirectory(HistorialStatusUnidadesModel::DIRECTORIO_IMAGEN);
                $file_uploaded = $this->servicioArchivos->upload($imagen, $directorio, $newFileName);
            }else{
                $imagen = '';
                $file_uploaded = '';
            }
            //Insertar
            $historial = $this->servicio->saveHistorialUnidad($file_uploaded,$request->all());
            return Respuesta::json(['imagen' => $file_uploaded,'historial'=>$historial], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}