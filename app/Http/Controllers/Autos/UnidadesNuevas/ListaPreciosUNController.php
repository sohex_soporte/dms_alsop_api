<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioListaPreciosUn;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class ListaPreciosUNController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioListaPreciosUn();
    }
    public function getByParameters(Request $request)
    {
        return Respuesta::json($this->servicio->getPreciosParameters($request->all()), 200);
    }
}
