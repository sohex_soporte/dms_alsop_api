<?php

namespace App\Http\Controllers\Polizas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Polizas\ServicioCatTipoPolizas;
use App\Servicios\Polizas\ServicioMovimientos;
use Illuminate\Http\Request;

class CatTipoPolizaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatTipoPolizas();
        $this->servicioMovimientos = new ServicioMovimientos();
    }

    public function test(Request $request)
    {
        try {
            return Respuesta::json($this->servicioMovimientos->getTipoCuenta([
                'cxc_id' => $request->get('cxc_id')
            ]), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
