<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Financiamientos\ServicioCatPerfilFinanciamiento;
use Illuminate\Http\Request;

class CatPerfilesFinanciamientosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatPerfilFinanciamiento();
    }
}
