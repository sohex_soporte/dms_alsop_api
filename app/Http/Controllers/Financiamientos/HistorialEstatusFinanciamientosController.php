<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Financiamientos\ServicioHistorialEstatusFinanciamientos;
use Illuminate\Http\Request;

class HistorialEstatusFinanciamientosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioHistorialEstatusFinanciamientos();
    }
    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function getComentariosFinanciamientos(Request $request){
        return Respuesta::json($this->servicio->buscarComentariosPorParametros($request->all()), 200);
    }
}
