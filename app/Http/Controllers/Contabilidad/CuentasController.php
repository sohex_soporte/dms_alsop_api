<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioCuentas;

class CuentasController extends CrudController
{
    public function __construct() {
        $this->servicio = new ServicioCuentas();
    }
}
