<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\PolizaContabilidadDetalle;
use Illuminate\Http\Request;

class PolizaContabilidadDetalleController extends CrudController
{
    public function __construct() {
        $this->servicio = new PolizaContabilidadDetalle();
    }
}
