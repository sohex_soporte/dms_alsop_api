<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentaProductoModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Refacciones\ServicioVentaProducto;
use Illuminate\Http\Request;

class VentaProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentaProducto();
        $this->servicioCurl = new ServicioCurl();
        $this->productosModel = new ProductosModel();
    }

    public function detalleVenta()
    {
        try {
            $data = $this->servicio->getDetalleVenta();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleVentaByFolio($folio_id)
    {
        try {
            $data = $this->servicio->getDetalleVentaByFolio($folio_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleVentaId($id)
    {
        try {
            $data = $this->servicio->geDetalleVentaById($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalVentaByFolio($folio_id)
    {
        try {
            $data = $this->servicio->totalVentaByFolio($folio_id);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $data = $this->servicio->store($request);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function validaCambioPreciosApartado(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['venta_id' => 'required']);
            $productos_venta = $this->servicio->geDetalleVentaById($request->venta_id)->toArray();
            $rel_precio_productos = [];
            foreach ($productos_venta as $key => $venta) {
                $producto = $this->productosModel->where(ProductosModel::ID, $venta->producto_id)->first();

                if ($producto->valor_unitario > $venta->valor_unitario) {
                    $rel_precio_productos[$key] = [
                        'no_identificacion' => $producto->no_identificacion,
                        'valor_actual' => $venta->valor_unitario,
                        'valor_actualizado' => $producto->valor_unitario
                    ];
                }
            }

            return Respuesta::json($rel_precio_productos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    //Actualiza precios despues de apartarlos
    public function actualizarProductosVenta(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['venta_id' => 'required']);
            $productos_venta = $this->servicio->geDetalleVentaById($request->venta_id)->toArray();
            $rel_precio_productos = [];
            foreach ($productos_venta as $key => $venta) {
                // $producto = $this->productosModel->where(ProductosModel::ID, $venta->producto_id)->first();
                // $venta_total = $producto->venta_total * $venta->cantidad;
                // $this->servicio->massUpdateWhereId(VentaProductoModel::ID, $request->venta_id, [
                //     VentaProductoModel::TOTAL_VENTA => 1
                // ]);
                // $producto = $this->productosModel->where(ProductosModel::ID, $venta->producto_id)->first();
                // $this->servicioProductos->mass
                // if ($producto->valor_unitario > $venta->valor_unitario) {
                //     $rel_precio_productos[$key] = [
                //         'no_identificacion' => $producto->no_identificacion,
                //         'valor_actual' => $venta->valor_unitario,
                //         'valor_actualizado' => $producto->valor_unitario
                //     ];
                // }
            }

            return Respuesta::json($rel_precio_productos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function deleteMpmitem($id, Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['id_cita' => 'required', 'id_indice'=> 'required']);
            $this->servicio->massUpdateWhereId(VentaProductoModel::ID, $id, [
                VentaProductoModel::ESTATUS_PRODUCTO_ORDEN => VentaProductoModel::ESTATUS_PRODUCTO_ORDEN_B
            ]);
            
            $this->servicioCurl->curlPost(env('SERVICIOS_ELIMINAR_REQUISICION'), [
                "id_cita" =>  $request->id_cita,
                "id_indice" => $request->id_indice
            ], true);
            return Respuesta::noContent(204);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
