<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioListaProductosOrdenCompra;
use Illuminate\Http\Request;

class ListaProductosOrdenCompraController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioListaProductosOrdenCompra();
    }

    public function listadoProductosCarrito($orden_compra)
    {
        try {
            $data = $this->servicio->listadoProductos($orden_compra);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $data = $this->servicio->store($request);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalByOrdenCompra($id)
    {
        try {
            $data = $this->servicio->totalByOrdenCompra($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

}
