<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioClientes;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Models\Refacciones\ClientesModel;

class ClientesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioClientes();
    }

    public function index()
    {
        try {
            $modelo = $this->servicio->getAll();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function selectCatalogo()
    {
        try {
            $modelo = $this->servicio->getCatalogo();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $modelo = $this->servicio->getOneCliente($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getLastRecord()
    {
        try {
            $modelo = $this->servicio->lastRecord();
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    #TODO: REVISAR CONSULTAS - MEJORAR
    public function searchNombreCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaNombre());
            $modelo = $this->servicio->searchNombreCliente($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getClienteByclave(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClaveCliente());
            $modelo = $this->servicio->clientePorClave($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaCliente());
            $modelo = $this->servicio->searchCliente($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function tieneCredito(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasClienteId());
            $modelo = $this->servicio->tieneCredito($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchNumeroCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCliente());
            $modelo = $this->servicio->searchNumeroCliente($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function actualizarAplicaCredito(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdateAplicaCredito());
            $data = [
                ClientesModel::APLICA_CREDITO => $request->aplica_credito,
            ];
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);

            $modelo = $this->servicio->updateAplicaCredito($data, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function actualizarPlazoCredito(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdatePlazoCredito());
            $data = [
                ClientesModel::PLAZO_CREDITO_ID => $request->plazo_credito_id,
                ClientesModel::LIMITE_CREDITO => $request->limite_credito,
                ClientesModel::APLICA_CREDITO => true,
            ];
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);

            $modelo = $this->servicio->updatePlazoCredito($data, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function crearRegresarCliente(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [
                ClientesModel::NOMBRE => 'required',
                ClientesModel::APELLIDO_MATERNO => 'nullable',
                ClientesModel::APELLIDO_PATERNO => 'nullable',
                ClientesModel::RFC => 'required'
            ]);

            $modelo = $this->servicio->createOrUpdate(
                [
                    ClientesModel::NOMBRE => $request->nombre,
                    ClientesModel::APELLIDO_PATERNO => $request->apellido_paterno,
                    ClientesModel::APELLIDO_MATERNO => $request->apellido_materno,
                    ClientesModel::RFC => $request->rfc
                ],
                $request->all()
            );
            // $modelo = $this->servicio->create($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
