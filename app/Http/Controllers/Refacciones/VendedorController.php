<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioVendedor;
use Illuminate\Http\Request;

class VendedorController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVendedor();
    }
}
