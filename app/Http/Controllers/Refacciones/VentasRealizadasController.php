<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\HistoricoProductosServicioModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioPermisoVenta;
use App\Servicios\Refacciones\ServicioVentasRealizadas;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\OperacionesPiezasModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Servicios\Refacciones\ServicioDesgloseProductos;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioReVentasEstatus;
use App\Servicios\Refacciones\ServicioVentaProducto;
use App\Servicios\Refacciones\ServicioVentasServicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentasRealizadasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentasRealizadas();
        $this->servicioVentasPermiso = new ServicioPermisoVenta();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioVentasServicios = new ServicioVentasServicios();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
        $this->servicioReVentasEstatus = new ServicioReVentasEstatus();
        $this->ventasRealizadasModel = new VentasRealizadasModel();
        $this->operacionesPiezasModel = new OperacionesPiezasModel();
        $this->historicoProductosServicioModel = new HistoricoProductosServicioModel();
        $this->servicioProductos = new ServicioProductos();
    }

    public function storeVenta(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardarSinFolio());
            $data = $this->servicio->registrarVenta($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventasByFolioId($folio_id)
    {
        try {
            $data = $this->servicio->ventasByFolioId($folio_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function totalventa($venta_id)
    {
        try {
            $venta_productos = $this->servicioVentaProducto->totalVentaById(['venta_id' => $venta_id]);
            $venta_servicios = $this->servicioVentasServicios->totalservicio($venta_id);
            $total = (float) $venta_servicios + (float)$venta_productos;
            return Respuesta::json($total, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function finalizarVenta(Request $request, $id)
    {
        try {
            //TODO: revisar venta total- VENTA TOTAL 
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaContado());

            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaCredito());
            }

            $data = $this->servicio->finalizarVenta($id, $request);
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function descontarVentaMpm(Request $request, $id)
    {
        try {
            //TODO: revisar venta total- VENTA TOTAL 
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaContado());

            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasFinalizaVentaCredito());
            }
            $request->merge(['mpm' => true]);
            $data = $this->servicio->finalizarVenta($id, $request);
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function index()
    {
        try {
            $data = $this->servicio->getAll();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function listaVentasByStatus(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasListaVentaByStatus());
            $data = $this->servicio->getVentasByStatus($request->all());
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function show($id)
    {
        try {
            $data = $this->servicio->getById($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventasFiltrarWithDetails(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusqueda());
            $data = $this->servicio->filtrarPorFechas($request->all());
            $array_final = $this->servicio->calcularTotal($data);
            return Respuesta::json($array_final, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getVentasByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getServicioByFolio(Request $request)
    {
        try {
            // ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getServicioByFolio($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalesSearchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getTotalesVentasByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getBusquedaVentas(Request $request)
    {
        try {
            $data = $this->servicio->getBusquedaVentas($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    //Obtener ventas para generar archivo
    public function getVentas6MesesAtras(Request $request)
    {
        try {

            // $ventas_realizadas = $this->servicio->getAllVentas($request);
            // return Respuesta::json(array('data' => $ventas_realizadas), 200);
            $layout = $this->servicio->generarlayoutford();
        } catch (\Throwable $e) {
            dd($e);
            return Respuesta::error($e);
        }
    }

    public function getAllVentas(Request $request)
    {
        try {
            $data = $this->servicio->getAllVentas($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getBusquedaVentasDevoluciones(Request $request)
    {
        try {
            $data = $this->servicio->getBusquedaVentasDevoluciones($request);
            return Respuesta::json(array('data' => $data), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function curlGetVehiculoInfo($numero_orden)
    {
        try {
            $data =  $this->servicio->curlGetDataApiMpm($numero_orden);
            $responseData = isset($data['vehiculo']) ? $data['vehiculo'] : [];
            return Respuesta::json($responseData, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getRefaccionesVentanillaTaller($numero_orden)
    {
        try {

            $existe_venta = $this->ventasRealizadasModel
                ->where(VentasRealizadasModel::NUMERO_ORDEN, '=', $numero_orden)
                ->first();
            $newArray = [];
            $dataWs = $this->servicio->listaProductosTaller($numero_orden);
            
            if ($existe_venta) {
                $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($existe_venta->id)->toArray();
                $VentaServicio = $this->servicioVentasServicios->ventaservicio($existe_venta->id)->toArray();
                $new_array = array_merge($VentaServicio, $productos_venta);

                foreach ($new_array as $key => $item) {
                    $stock = 0;
                    if (isset($item->producto_id)) {

                        $stock = $this->servicioProductos->stockByProductoId($item->producto_id);
                        $stock = $stock->desglose_producto->cantidad_actual;
                    }
                    
                    $newArray[$key]["id"] = $item->id;
                    $newArray[$key]["id_index_requisicion"] =isset( $item->id_index_requisicion)?  $item->id_index_requisicion : null;
                    $producto_id =  !empty($item->producto_id) ? $item->producto_id : null;
                    $newArray[$key]["producto_id"] =  $producto_id;
                    $newArray[$key]["cantidad"] = $item->cantidad;
                    $newArray[$key]["descripcion"] = isset($item->descripcion_producto) ? $item->descripcion_producto : $item->descripcion;  //valodar si se pone uno u otro al traspaso
                    $newArray[$key]["num_pieza"] = $item->no_identificacion;
                    $newArray[$key]["precio_unitario"] = isset($item->costo_mo) ? $item->costo_mo : $item->valor_unitario;
                    $newArray[$key]["tipo"] = !empty($item->producto_id) ? "PREPIKING" : "OPERACION";
                    $newArray[$key]["se_registro"] = true;
                    $newArray[$key]["cantidad_stock"] = $stock;
                    $newArray[$key]["orden_original"] = isset($item->orden_original) &&  $producto_id !== null ? $item->orden_original : 1;
                    $newArray[$key]["estatus_producto_orden"] =  isset($item->estatus_producto_orden) &&  $producto_id !== null ? $item->estatus_producto_orden : "O";
                }
                return Respuesta::json($newArray, 200);
            }
            
            foreach ($dataWs['refacciones'] as $key => $itemWs) {
                //checar esta validacion
                $historicoProductoServicio = $this->historicoProductosServicioModel
                    ->where(HistoricoProductosServicioModel::getTableName() . '.' . HistoricoProductosServicioModel::PRODUCTO_ID,  $itemWs->producto_id)
                    ->first();
                if ($historicoProductoServicio) {
                    $stock = $this->servicioProductos->stockByProductoId($historicoProductoServicio->producto_id);
                    $precio_unitario = $stock->valor_unitario;
                    $stock = $stock->desglose_producto->cantidad_actual;

                    $newArray[$key]["precio_unitario"] =  $precio_unitario;
                    $newArray[$key]["cantidad_stock"] = $stock;
                    $newArray[$key]["se_registro"] = $itemWs->se_registro;
                } else {
                   
                    $newArray[$key]["cantidad_stock"] = $itemWs->cantidad_stock;//$cantidad_stock;
                    $newArray[$key]["precio_unitario"] = !empty($itemWs->producto_id) ? $itemWs->precio_unitario : $itemWs->costo_mo;
                    $newArray[$key]["se_registro"] =  $itemWs->se_registro;
                }
               
                
                $newArray[$key]["id"] = $itemWs->id; //verificar id
                $newArray[$key]["producto_id"] = !empty($itemWs->producto_id) ? $itemWs->producto_id : null;
                $newArray[$key]["cantidad"] = isset($historicoProductoServicio->cantidad) && $itemWs->cantidad !== $historicoProductoServicio->cantidad ? $historicoProductoServicio->cantidad : $itemWs->cantidad;
                $newArray[$key]["descripcion"] = isset($itemWs->descripcion_producto) ? $itemWs->descripcion_producto : $itemWs->descripcion;  //valodar si se pone uno u otro al traspaso
                $newArray[$key]["num_pieza"] = $itemWs->num_pieza; //validar remplazo
                $newArray[$key]["tipo"] = $itemWs->tipo;
                $newArray[$key]["orden_original"] = 1;
                $newArray[$key]["estatus_producto_orden"] = "O";
            }
            return Respuesta::json($newArray, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function ventaMpm(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCrearVentaMpm());
            $data = $this->servicio->crearVentaMpm($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function detalleOperacion(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, [
                'venta_id' => 'required',
                // 'no_identificacion' => 'required'
            ]);
            $no_orden = $this->ventasRealizadasModel
                ->select('numero_orden')
                ->where('id', '=', $request->venta_id)
                // ->where('no_identificacion', '=', $request->no_identificacion)
                ->first();

            // producto.no_identificacion,producto.valor_unitario
            $data = $this->operacionesPiezasModel
                ->select('producto.*')
                ->join('producto', 'producto.id', '=', 'operaciones_piezas.id_refaccion')
                ->where('no_orden', '=', $no_orden->numero_orden)
                ->get();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function detalleVentampmById($venta_id)
    {
        try {
            $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($venta_id)->toArray();
            // dd($productos_venta);
            $venta_servicios = $this->servicioVentasServicios->ventaservicio($venta_id)->toArray();
            $new_array = array_merge($venta_servicios, $productos_venta);
            return Respuesta::json($new_array);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function FinalizarVentaServicio(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, [VentasRealizadasModel::NUMERO_ORDEN => 'required']);
            $data = $this->servicio->finalizarVentaServicio($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }


    #TODO: revisar si al abrir de nuevo afecta a kardex y otro flujo
    public function abrirVentaServicio(Request $request)
    {
        try {
            DB::beginTransaction();
            ParametrosHttpValidador::validar($request, [VentasRealizadasModel::NUMERO_ORDEN => 'required']);
            $data = $this->servicio->abrirOrdenServicio($request->all());
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getVentasPorMesByProducto(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasVentasAgrupadasMes());

            $data = $this->servicio->getVentasAgrupadasMes($request->all());
            $listado = [];
            $totalVentas = 0;
            foreach ($data as $val) {
                $totalVentas = $totalVentas + $val->total_ventas;
                $val->año = $request->get('year');
                array_push($listado, $val);
            }
            $cantidad = $request->get('cantidad_meses') ? $request->get('cantidad_meses') : 12;
            $promedio = $totalVentas / $cantidad;

            $totales[] = [
                'total' => $totalVentas,
                'promedio' => $promedio,
                'year' => $request->get('year'),
            ];


            return Respuesta::json([
                'lista_ventas' => $listado,
                'totales' => $totales
            ], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function apartarVenta(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['venta_id' => 'required']);
            $productos_venta = $this->servicioVentaProducto->geDetalleVentaById($request->venta_id)->toArray();
            $this->servicioReVentasEstatus->storeReVentasEstatus([
                ReVentasEstatusModel::VENTA_ID => $request->venta_id,
                ReVentasEstatusModel::ESTATUS_VENTA_ID  => EstatusVentaModel::ESTATUS_APARTADO
            ]);

            if (!empty($productos_venta)) {
                foreach ($productos_venta as $key => $venta) {
                    $this->servicioDesgloseProductos->actualizaStockByProducto(['producto_id' => $venta->producto_id]);
                }
            }

            $this->servicio->massUpdateWhereId(VentasRealizadasModel::ID, $request->venta_id, [
                VentasRealizadasModel::TIENE_PIEZAS_APARTADAS => 1
            ]);
            return Respuesta::json($productos_venta, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function ventasByUser($user_id)
    {
        try {
            $data = $this->servicio->getVentasByUser($user_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getOrdenesAbiertas()
    {
        try {

            //Cargamos las ordenes abiertas
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,  env('SERVICIOS_ORDENES_ABIERTAS'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $server_output = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($server_output);

            $tableVentas = VentasRealizadasModel::getTableName();

            $arrayData = [];
            foreach ($response as $key => $item) {
                $arrayData[$key]['id_cita'] = $item->id_cita;
                $arrayData[$key]['cliente'] = $item->cliente;
                $arrayData[$key]['asesor'] = $item->asesor;
                $arrayData[$key]['tecnico'] = $item->tecnico;
                $arrayData[$key]['modelo'] = $item->modelo;
                $arrayData[$key]['kilometraje'] = $item->kilometraje;
                $arrayData[$key]['placas'] = $item->placas;
                $arrayData[$key]['serie'] = $item->serie;
                $arrayData[$key]['anio'] = $item->anio;
                $arrayData[$key]['estatus_orden'] = $item->estatus_orden;
                $arrayData[$key]['presupuesto_adicional'] = $item->presupuesto_adicional;
                $arrayData[$key]['presupuesto_afectado'] = $item->presupuesto_afectado;
                $data =  $this->ventasRealizadasModel->where($tableVentas . '.' . VentasRealizadasModel::NUMERO_ORDEN, $item->id_cita)->count();
                $arrayData[$key]['orden_venta'] = $data > 0 ? true : false;
            }

            return Respuesta::json(['data' => $arrayData], 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }
}
