<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioOperacionesPiezas;
use Illuminate\Http\Request;

class OperacionesPiezasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioOperacionesPiezas();
    }

    public function verificartotales(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->validateOrSave($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
