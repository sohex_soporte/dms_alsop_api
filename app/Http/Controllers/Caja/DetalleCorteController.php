<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioDetalleCorte;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;


class DetalleCorteController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDetalleCorte();
    }

    public function getByCajaId(Request $request)
    {
        $modelo = $this->servicio->getByCajaId($request->toArray());
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

}
