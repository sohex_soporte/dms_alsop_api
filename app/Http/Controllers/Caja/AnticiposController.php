<?php

namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Caja\ServicioAnticipos;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;


class AnticiposController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAnticipos();
    }


    public function getFilter(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getAll($request), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByAnticipoId(int $id)
    {
        try {
            return Respuesta::json($this->servicio->getById($id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByAnticipoByFolioId(int $id)
    {
        try {
            return Respuesta::json($this->servicio->getByFolioId($id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByTotalByFolioId(int $id)
    {
        try {
            $respuesta = $this->servicio->getByTotalByFolioId($id);
            $response['total'] = $respuesta ? $respuesta->sumatotal : 0;
            return Respuesta::json($response, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateEstatus(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasEstatus());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->changeEstatus($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function aplicarAnticipo(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasAplicarFolio());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->aplicarAnticipo($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
