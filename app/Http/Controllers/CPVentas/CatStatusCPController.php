<?php

namespace App\Http\Controllers\CPVentas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\CPVentas\ServicioCatEstatusCP;
use Illuminate\Http\Request;

class CatStatusCPController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatEstatusCP();
    }
}
