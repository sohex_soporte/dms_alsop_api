<?php

namespace App\Http\Controllers\CPVentas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CPVentas\ServicioCPVentas;
use Illuminate\Http\Request;

class VentasCPController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCPVentas();
    }
    public function getAll(Request $request){
        try {
			return Respuesta::json($this->servicio->getAll($request->all()), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
}
