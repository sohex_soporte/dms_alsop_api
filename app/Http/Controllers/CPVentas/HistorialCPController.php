<?php

namespace App\Http\Controllers\CPVentas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CPVentas\ServicioHistorialCP;
use Illuminate\Http\Request;

class HistorialCPController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioHistorialCP();
    }
    public function getHistorial(Request $request){
        try {
			return Respuesta::json($this->servicio->getAllHistorial($request->all()), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
}
