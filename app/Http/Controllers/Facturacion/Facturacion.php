<?php

namespace App\Http\Controllers\Facturacion;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Facturas\ServicioFacturacion;
use App\Models\Facturas\Factura;
use Illuminate\Http\Request;
use Throwable;

class Facturacion extends CrudController
{
    private $servicioArchivos;
    public function __construct()
    {
        $this->servicio = new ServicioFacturacion();
        $this->servicioArchivos = new ServicioManejoArchivos();
    }

    public function uploadXmlFactura(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUploadxml());
            $file = $request->file(Factura::ARCHIVO_FACTURA);
            $newFileName = $this->servicio->setFileName($file);
            $directorio = $this->servicio->setDirectory(ServicioFacturacion::DIRECTORIO_FACTURAS);
            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . $newFileName;
            $this->servicio->handleDataXmlFactura($path);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json('', 200, $mensaje);
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByIdFactura($id)
    {
        try {
            return Respuesta::json($this->servicio->getinformacionFactura($id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function downloadFactura($file_name)
    {
        return response()->download(storage_path("app/facturas/uploads/xml/facturas/{$file_name}"));
    }

    public function existeFactura(Request $request)
    {
        ParametrosHttpValidador::validar($request, $this->servicio->getReglasUploadxml());
        // dd($this->servicioTimbreFiscal->existeTimbrefiscal($timbre[TimbreFiscal::UUID]));
    }
}
