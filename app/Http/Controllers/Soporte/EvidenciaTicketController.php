<?php

namespace App\Http\Controllers\Soporte;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Models\Soporte\EvidenciaTicketModel;
use App\Models\Soporte\HistorialTicketsModel;
use App\Models\Soporte\TicketsModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Soporte\ServicioEvidencia;
use Illuminate\Http\Request;

class EvidenciaTicketController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEvidencia();
        $this->servicioArchivos = new ServicioManejoArchivos();
        date_default_timezone_set('America/Mexico_City');
    }
    public function getEvidenciaTicket($ticket_id=''){
        return Respuesta::json($this->servicio->getEvidenciaTicket($ticket_id), 200);
    }
    public function uploadEvidencia(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, 
            [
                EvidenciaTicketModel::TICKET_ID => 'required',
                TicketsModel::FOLIO => 'required',
                TicketsModel::ROL_ID => 'required',
                HistorialTicketsModel::USUARIO => 'required',
                HistorialTicketsModel::COMENTARIO => 'required',
                EvidenciaTicketModel::EVIDENCIA => 'required|mimes:pdf,jpg,jpeg,png,mp4,avi'
            ]
        );
            $file = $request->file(EvidenciaTicketModel::EVIDENCIA);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(EvidenciaTicketModel::DIRECTORIO_EVIDENCIA);
            $url_image = $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . DIRECTORY_SEPARATOR . $newFileName;
            $xml_parameters = $this->servicio->handleSaveEvidencia($path,$request->all());
            $mensaje = __(static::$I0012_EVIDENCIA_TICKET_SUBIDA, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($xml_parameters, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
