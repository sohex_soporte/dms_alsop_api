<?php

namespace App\Http\Controllers\Soporte;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Soporte\ServicioHistorialTickets;
use Illuminate\Http\Request;

class HistorialTicketsController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioHistorialTickets();
        date_default_timezone_set('America/Mexico_City');
    }
    public function getAllHistorial($ticket_id){
        try {
			return Respuesta::json($this->servicio->getAllByTicket($ticket_id), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
    public function getByFolio($folio){
        try {
			return Respuesta::json($this->servicio->getAllByFolio($folio), 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
    }
}
