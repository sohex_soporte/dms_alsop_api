<?php

namespace App\Exceptions;

use Exception;

class MylsaException extends Exception {
    protected $code = 400;
}
