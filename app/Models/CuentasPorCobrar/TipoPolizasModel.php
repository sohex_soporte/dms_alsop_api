<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class TipoPolizasModel extends Modelo
{
    protected $table = 'tipo_polizas';
    const ID = "id";
    const CLAVE = "clave";
    const DESCRIPCION = "descripcion";

    const C0  = 'C0';
    const B = 'B';
    const DQ = 'DQ';
    const CA = 'CA';
    const CG = 'CG';
    const CH = 'CH';
    const CJ = 'CJ';
    const CM = 'CM';
    const CN = 'CN';
    const CQ = 'CQ';
    const CS = 'CS';
    const DO = 'DO';
    const FH = 'FH';
    const GT = 'GT';
    const OG = 'OG';
    const OT = 'OT';
    const QG = 'QG';
    const QH = 'QH';
    const QT = 'QT';
    const V0 = 'V0';
    const VN = 'VN';
    const VR = 'VR';
    const ND = 'ND';

    protected $fillable = [
        self::CLAVE,
        self::DESCRIPCION
    ];
}
