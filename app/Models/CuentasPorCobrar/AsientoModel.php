<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class AsientoModel extends Modelo
{
    protected $table = 'asientos';

    const ID = "id";
    const NOMBRE = "nombre";
    const REFERENCIA = "referencia";
    const CONCEPTO = "concepto";
    const TOTAL = "total";
    const TOTAL_PAGO = "total_pago";
    const CUENTA_ID = "cuenta_id";
    const CUENTA = "cuenta";
    const TIPO_ASIENTO_ID = "tipo_asiento_id";
    const TIPO = "tipo";
    const FECHA = "fecha";
    const FOLIO_ID = "folio_id";
    const CLAVE_POLIZA = "clave_poliza";
    const DEPARTAMENTO = "departamento";
    const ESTATUS = "estatus";
    const CLIENTE_ID = "cliente_id";
    const FOLIO = "folio";
    const PROVEEDOR_ID = "proveedor_id";
    const ASIENTO_ID = "asiento_id";
    const ABONO = "abono";
    const CARGO = "cargo";

    const DEPARTAMENTO_CAJAS_SAN_JUAN = 1;
    const DEPARTAMENTO_VENTAS_SAN_JUAN = 2;
    const DEPARTAMENTO_CAJAS_QUERETARO = 3;
    const DEPARTAMENTO_VENTAS_QUERETARO = 4;
    const HOJALATERIA = 5;
    const VENTA_MOSTRADOR = 6;
    const SERVICIOS = 7;
    const GARANTIAS = 8;
    const TESORERIA = 9;
    const COMPRAS = 10;
    const TALLER = 11;

    const TOTAL_REFACCIONES = 'total_refacciones';
    const TOTAL_MANO_OBRA = 'total_mano_obra';
    const TOTAL_CLIENTE_SERVICIOS = 'total_cliente_servicios';
    const TOTAL_INVENTARIO_PROCESO = 'total_inventario_proceso';
    const TOTAL_IVA_FACTURADO = 'total_iva_facturado';
    const TOTAL_COSTO_REFACCION = 'total_costo_refaccion';
    const TOTAL_DESCUENTO_REFACCION = 'total_descuento_refacciones';
    const TOTAL_DESCUENTO_MANO_OBRA = 'total_descuento_mano_obra';
    const TOTAL_GARANTIAS_POR_COBRAR = 'total_garantias_por_cobrar';
    const TOTAL_COSTO_PIEZA = 'total_costo_pieza';
    const PROCESOS = 'procesos';
    const ACONDICIONAMIENTO = 'acondicionamiento';
    const CORTESIA_NUEVOS = 'cortesia_nuevos';

    const TOTAL_FACTURA_SIN_IVA = 'total_factura_sin_iva';
    const TOTAL_IVA_FACTURA = 'total_iva_factura';
    const TOTAL_FACTURA = 'total_factura';



}

