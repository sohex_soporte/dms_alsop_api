<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class CuentasPorCobrarModel extends Modelo
{
    protected $table = 'cuentas_por_cobrar';
    const ID = "id";
    const FOLIO_ID = "folio_id";
    const CLIENTE_ID = "cliente_id";
    const CONCEPTO = "concepto";
    const TIPO_FORMA_PAGO_ID = "tipo_forma_pago_id";
    const TIPO_PAGO_ID = "tipo_pago_id";
    const PLAZO_CREDITO_ID = "plazo_credito_id";
    const IMPORTE = "importe";
    const TOTAL = "total";
    const COMENTARIOS = "comentarios";
    const ENGANCHE = "enganche";
    const TASA_INTERES = "tasa_interes";
    const INTERESES = "intereses";
    const ESTATUS_CUENTA_ID = "estatus_cuenta_id";
    const FECHA = "fecha";
    const USUARIO_GESTOR_ID = 'usuario_gestor_id';
    const TIPO_PROCESO = 'tipo_proceso';
    const NUMERO_ORDEN = 'numero_orden';

    const ESTATUS_TRANSFERENCIA_NO_AUTORIZADA = 3;
    const ESTATUS_TRANSFERENCIA_AUTORIZADA = 2;
    const ESTATUS_TRANSFERENCIA_ESPERA_AUTORIZACION = 1;
    const ESTATUS_TRANSFERENCIA_NO_APLICA = 0;
    const TRANSFERENCIA_AUTORIZADA = 'transferencia_autorizada'; //cuando el tipo de pago es transferencia, se tiene que autorizar
    const CUENTA_POR_COBRAR_ID = 'cuenta_por_cobrar_id';


    protected $fillable = [
        self::FOLIO_ID,
        self::CLIENTE_ID,
        self::ESTATUS_CUENTA_ID,
        self::CONCEPTO,
        self::TIPO_FORMA_PAGO_ID,
        self::TIPO_PAGO_ID,
        self::PLAZO_CREDITO_ID,
        self::IMPORTE,
        self::TOTAL,
        self::COMENTARIOS,
        self::ENGANCHE,
        self::TASA_INTERES,
        self::INTERESES,
        self::FECHA,
        self::USUARIO_GESTOR_ID,
        self::TRANSFERENCIA_AUTORIZADA,
        self::NUMERO_ORDEN
    ];
}
