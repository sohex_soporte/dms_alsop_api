<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class TelefonosSoporteModel extends Modelo
{
    protected $table = 'telefonos_soporte';
    const ID = "id";
    const TELEFONO = "telefono";
    const USUARIO = "usuario";
    protected $fillable = [
        self::TELEFONO,
        self::USUARIO
    ];
}
