<?php

namespace App\Models\CPVentas;

use App\Models\Core\Modelo;

class CPHistorialModel extends Modelo
{
    protected $table = 'cp_historial';
    const ID = "id";
    const ESTATUS_ID = "estatus_id";
    const USER_ID = "user_id";
    const CP_ID = "cp_id";
    const FECHA = "fecha";
    const HORA = "hora";
    const OBSERVACIONES = "observaciones";
    protected $fillable = [
        self::ID,
        self::ESTATUS_ID,
        self::USER_ID,
        self::CP_ID,
        self::FECHA,
        self::HORA,
        self::OBSERVACIONES
    ];
}
