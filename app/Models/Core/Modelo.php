<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modelo extends Model
{
    // use SoftDeletes;
    protected $softDelete = false;

    const UPDATED_AT = 'updated_at';
    const CREATED_AT = 'created_at';
    const DELETED_AT = 'deleted_at';

    protected $dates = [self::DELETED_AT];

    protected $hidden = [
        self::DELETED_AT,
    ];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

}
