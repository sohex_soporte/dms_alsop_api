<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class FilesSasSegurosModel extends Modelo
{
    protected $table = 'archivos_sas_seguros';
    const ID = 'id';
    const ARCHIVO = 'archivo';
    const PATH = 'path';
    const SEGURO_ID = 'seguro_id';
    protected $fillable = [
       self::ARCHIVO,
       self::PATH,
       self::SEGURO_ID
    ];
}
