<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class ReRolesModulosModel extends Modelo
{
    protected $table = 're_roles_modulos';
    const ID = "id";
    const ROL_ID = "rol_id";
    const MODULO_ID = "modulo_id";
    
    protected $fillable = [
        self::MODULO_ID,
        self::ROL_ID
    ];
}
