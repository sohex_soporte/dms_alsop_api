<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class RolesSubmenuModel extends Modelo
{
    protected $table = 'det_roles_submenu';
    const ID = "id";
    const ROL_ID = "rol_id";
    const SUBMENU_ID = "submenu_id";
    
    protected $fillable = [
        self::SUBMENU_ID,
        self::ROL_ID
    ];
}
