<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaFormaPagoModel extends ModeloNominas
{
    protected $table = 'ca_FormaPago';
     
    
    const ID = "id_FormaPago";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}