<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTipoJornadaModel extends ModeloNominas
{
    protected $table = 'ca_TipoJornada';
     
    
    const ID = "id_TipoJornada";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion
    ];
}