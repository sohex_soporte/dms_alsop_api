<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaNivelesEstudioModel extends ModeloNominas
{
    protected $table = 'ca_NivelesEstudio';
     
    
    const ID = "id_NivelEstudio";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}