<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaRegimenContratoModel extends ModeloNominas
{
	protected $table = 'ca_RegimenContrato';
     
    
    const ID = "id_RegimenContrato";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion
    ];
}
