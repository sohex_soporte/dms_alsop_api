<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaRiesgoPuestoModel extends ModeloNominas
{
    protected $table = 'ca_RiesgoPuesto';
    
     
    const ID = "id_RiesgoPuesto";
    const Numero = "Numero";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Numero,
        self::Descripcion
    ];
}
