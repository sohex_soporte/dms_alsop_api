<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class CatalogoCuentasModel extends Modelo
{
    protected $table = 'catalogo_cuentas';
    const ID = "id";
    const NO_CUENTA = "no_cuenta";
    const NOMBRE_CUENTA = "nombre_cuenta";
    const ID_MOVIMIENTO = "id_movimiento";
    const PRINCIPAL = "principal";
    const SUBCUENTA = "subcuenta";
    
    const ORDEN_SERVICIO = 8;
    const ABONO_BANORTE = 16;

    const CARGO = 2;
    const ABONO = 1;

    const CUENTA_VENTA_REFACCIONES_ACCESORIOS_SJR = 866;
    const CUENTA_MANO_OBRA_TALLER_SJR = 894;
    const CUENTA_CLIENTE_SERVICIO = 43;
    const CUENTA_IVA = 130;
    const CUENTA_BANORTE = 16;
    const CUENTA_IVA_COBRADO = 410;
    const CUENTA_CLIENTE_AUTOS_NUEVOS = 40;
    const CUENTA_ISAN_POR_PAGAR = 404;
    const CUENTA_IVA_POR_ACREDITAR = 81;
    const CUENTA_BANCOS = 21;
    const CUENTA_MENUDEO_REFACCIONES = 858;
    const CUENTA_REDONDEO = 2706;
    const CUENTA_COSTO_REFACCIONES_ACCESORIOS_SJR = 1305;

    const CUENTA_350001 = '878';
    const CUENTA_350003 = '880'; //MANO OBRA
    const CUENTA_110004 = '43'; //CLIENTE SERVICIOS
    const CUENTA_151000 = '230'; //INVENTARIO PROCESO
    const CUENTA_115015 = '81'; // IVA FACTURADO
    const CUENTA_450001 = '1329'; // COSTO REFACCION
    const CUENTA_350008 = '885'; // DESCUENTO REFACCION
    const CUENTA_350007 = '884'; // DESCUENTO MANO OBRA
    const CUENTA_340101 = '867';
    const CUENTA_115005 = '79';
    const CUENTA_131000 = '158';
    const CUENTA_430001 = '1291';
    const CUENTA_370002 = '909';
    const CUENTA_370003 = '910';
    const CUENTA_470001 = '1394';
    const CUENTA_370004 = '911';
    const CUENTA_241300 = '411'; // IVA FACTURADO REVISAR NO EXISTE CUENTA
    const CUENTA_241200 = '410';  // IVA COBRADO REVISAR NO EXISTE CUENTA
    const CUENTA_330004 = '844';  // GARANTIAS
    const CUENTA_350004 = '881';  // GARANTIAS
    const CUENTA_110008 = '45';  // GARANTIAS POR COBRAR
    const CUENTA_450002 = '1330';  // GARANTIAS POR COBRAR
    const CUENTA_540028 = '1986';  // CORTESIAS
    const CUENTA_510034 = '1764';  // ACONDICIONAMIENTO
    const CUENTA_202000 = '345';  // DOC. X PAGAR F.C. REFACCIONES
    const CUENTA_210002 = '354';  // DOC. X PAGAR F.C. REFACCIONES
    const CUENTA_110003 = '42';  // CLIENTES REFACCIONESS
    const CUENTA_440102 = '1316';  // COSTO REFACCION MAYOREO


    protected $fillable = [
        self::NO_CUENTA,
        self::NOMBRE_CUENTA,
        self::ID_MOVIMIENTO
    ];
}
