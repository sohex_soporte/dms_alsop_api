<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class EstatusFacturaModel extends Modelo
{
    protected $table = 'estatus_factura';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_CREADA = 1;
    const ESTATUS_CANCELADA = 2;
    
    
    protected $fillable = [
        self::NOMBRE
    ];
}
