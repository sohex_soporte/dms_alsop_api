<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class Almacenes extends Modelo
{
    protected $table = 'almacen';
    const ID = "id";
    const NOMBRE = "nombre";
    const UBICACION = "ubicacion";
    const CODIGO_ALMACEN = "codigo_almacen";
    
    const DEFAULT_ALMACEN = 1;

    protected $fillable = [
        self::NOMBRE,
        self::UBICACION,
        self::CODIGO_ALMACEN
    ];
}
