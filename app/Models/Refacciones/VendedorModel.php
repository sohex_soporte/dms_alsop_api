<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class VendedorModel extends Modelo
{
    protected $table = 'vendedor';
    const ID = "id";
    const NOMBRE = "nombre";
    const APELLIDO_MATERNO = 'apellido_materno';
    const APELLIDO_PATERNO = 'apellido_paterno';
    const NUMERO_VENDEDOR = 'nombre_vendedor';
    const RFC = 'rfc';
    const DIRECCION = 'direccion';
    const ESTADO = 'estado';
    const MUNICIPIO = 'municipio';
    const COLONIA = 'colonia';
    const CODIGO_POSTAL = 'codigo_postal';
    const TELEFONO = 'telefono';
    const TELEFONO_2 = 'telefono_secundario';
    const CORREO_ELECTRONICO = 'correo_electronico';
    const CORREO_ELECTRONICO_2 = 'correo_electronico_secundario';
    const NOTAS = 'notas';

    const CLAVE_VENDEDOR = 'clave_vendedor';

    protected $fillable = [
        self::NOMBRE,
        self::APELLIDO_MATERNO,
        self::APELLIDO_PATERNO,
        self::NUMERO_VENDEDOR,
        self::RFC,
        self::DIRECCION,
        self::ESTADO,
        self::MUNICIPIO,
        self::COLONIA,
        self::CODIGO_POSTAL,
        self::TELEFONO,
        self::TELEFONO_2,
        self::CORREO_ELECTRONICO,
        self::CORREO_ELECTRONICO_2,
        self::NOTAS,
        self::CLAVE_VENDEDOR
    ];
}
