<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReEstatusTraspasoModel extends Modelo
{
    protected $table = 're_estatus_traspaso';
    const ID = "id";
    const TRASPASO_ID = "traspaso_id";
    const ESTATUS_ID = "estatus_id";
    const USER_ID = "user_id";
    const ACTIVO = "activo";
    const STOCK = "stock";

    protected $fillable = [
        self::TRASPASO_ID,
        self::ESTATUS_ID,
        self::USER_ID,
        self::ACTIVO,
        self::STOCK
    ];
}
