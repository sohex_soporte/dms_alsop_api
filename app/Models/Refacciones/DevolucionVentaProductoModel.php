<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DevolucionVentaProductoModel extends Modelo
{
    protected $table = 'devolucion_venta_producto';
    const ID = "id";
    const FOLIO_ID = "folio_id";
    const VENTA_ID = "venta_id";
    const PRODUCTO_ID = "producto_id";
    const PRECIO_ID = "precio_id";
    const CANTIDAD = "cantidad";


    protected $fillable = [
        self::FOLIO_ID,
        self::VENTA_ID,
        self::PRODUCTO_ID,
        self::PRECIO_ID,
        self::CANTIDAD,
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }

    public function precio()
    {
        return $this->belongsTo(Precios::class);
    }
}



