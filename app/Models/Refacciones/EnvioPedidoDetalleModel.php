<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class EnvioPedidoDetalleModel extends Modelo
{
    protected $table = 'envio_pedido_detalle';
    const ID = "id";
    const VIA_ENVIO = 'via_envio';
    const DIA_ENVIO = 'dia_envio';
    const RUTA = 'ruta';
    const LINEAS = 'lineas';
    const TOTAL_PIEZAS = 'total_piezas';
    const PESO_TOTAL = 'peso_total';
    const NUMERO_REMISION = 'numero_remision';

    const PEDIDO_ID = 'pedido_id';


    protected $fillable = [
        self::VIA_ENVIO,
        self::DIA_ENVIO,
        self::RUTA,
        self::LINEAS,
        self::TOTAL_PIEZAS,
        self::PESO_TOTAL,
        self::NUMERO_REMISION,
        self::PEDIDO_ID
    ];
}
