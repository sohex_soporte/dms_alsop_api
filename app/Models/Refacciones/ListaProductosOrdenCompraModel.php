<?php


namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria

class ListaProductosOrdenCompraModel extends Modelo
{
    //use SoftDeletes; //Implementamos 

    protected $table = 'productos_orden_compra';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const ORDEN_COMPRA_ID = "orden_compra_id";
    const CANTIDAD = "cantidad";
    const PRECIO = 'precio';
    const TOTAL = 'total';
    
    // protected $dates = ['deleted_at']; //Registramos la nueva columna

    const REL_ORDEN_COMPRA = 're_orden_compra';

    protected $fillable = [
        self::PRODUCTO_ID,
        self::TOTAL,
        self::ORDEN_COMPRA_ID,
        self::CANTIDAD,
        self::PRECIO
    ];

    public function re_orden_compra()
    {
        return $this->hasOne(OrdenCompraModel::class, OrdenCompraModel::ID, self::ORDEN_COMPRA_ID);
    }

    public function rel_producto()
    {
        $this->hasOne(ProductosModel::class, ProductosModel::ID, self::PRODUCTO_ID);
    }
}
