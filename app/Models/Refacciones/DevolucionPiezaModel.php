<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class DevolucionPiezaModel extends Modelo
{
    protected $table = 'devolucion_pieza';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const ORDEN_COMPRA_ID = "orden_compra_id";
    const CANTIDAD = "cantidad";
    const TOTAL = "total";
    const NOTA_CREDITO = "nota_credito";
    const OBSERVACIONES = "observaciones";

    protected $fillable = [
        self::PRODUCTO_ID,
        self::ORDEN_COMPRA_ID,
        self::CANTIDAD,
        self::TOTAL,
        self::NOTA_CREDITO,
        self::OBSERVACIONES
    ];
}
