<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class EstatusCompra extends Modelo
{
    protected $table = 'estatus_compra';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_PROCESO = 1;
    const ESTATUS_FINALIZADA = 2;
    const FACTURA_PAGADA = 3;
    const ESTATUS_CANCELADA = 4;
    const ESTATUS_DEVOLUCION = 5;

    // ANTERIORMENTE ESTABAN ASÍ
    // const ESTATUS_CANCELADA = 1;
    // const ESTATUS_PROCESO = 2;
    // const ESTATUS_FINALIZADA = 3;
    // const FACTURA_PAGADA = 4;
    // const ESTATUS_DEVOLUCION = 5;
    
    protected $fillable = [
        self::NOMBRE
    ];
}
