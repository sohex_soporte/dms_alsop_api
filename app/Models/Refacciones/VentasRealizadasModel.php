<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;

class VentasRealizadasModel extends Modelo
{
    protected $table = 'ventas';
    const ID = "id";
    const FOLIO_ID = 'folio_id';
    const CLIENTE_ID = 'cliente_id';
    const ESTATUS_ID = 'estatus_id';
    const PRECIO_ID = 'precio_id';
    const VENTA_TOTAL = 'venta_total';
    const ALMACEN_ID = 'almacen_id';
    const TIPO_VENTA = 'tipo_venta_id'; //ventanilla o mostrador
    const TIPO_PRECIO_ID = 'tipo_precio_id';
    const NUMERO_ORDEN = 'numero_orden';
    const ORDEN_CERRADA = 'orden_cerrada';

    const VENTA_ID = 'venta_id';
    const REL_PRODUCTO_ID = 'producto_id';
    const REL_FOLIO = 'folio';
    const REL_CLIENTE = 'cliente';
    const REL_VENTAS_ESTATUS = 're_ventas_estatus';
    const REL_VENTAS_FINALIZADAS = 'ventas_finalizadas';

    const VENDEDOR_ID = 'vendedor_id';

    const REL_PRECIO = 'precio';
    const REL_ALMACEN = 'almacen';
    const REL_TIPO_PRECIO = 'tipo_precio';

    const TIPO_VENTA_VENTANILLA = 1;
    const TIPO_VENTA_VENTANILLA_TALLER = 2;

    const FECHA_INICIO = 'fecha_inicio';
    const FECHA_FIN = 'fecha_fin';

    const ESTATUS_COMPRA = "estatus_compra";
    const CANTIDAD_MESES = "cantidad_meses";
    const YEAR = "year";

    const TIENE_PIEZAS_APARTADAS = 'piezas_apartadas';
    const ESTATUS_APARTADAS = 1;
    const ESTATUS_NO_APARTADAS = 0;

    protected $fillable = [
        self::FOLIO_ID,
        self::CLIENTE_ID,
        self::ESTATUS_ID,
        self::VENTA_TOTAL,
        self::TIPO_VENTA,
        self::PRECIO_ID,
        self::ALMACEN_ID,
        self::TIPO_PRECIO_ID,
        self::NUMERO_ORDEN,
        self::ORDEN_CERRADA,
        self::TIENE_PIEZAS_APARTADAS,
        self::VENDEDOR_ID
    ];

    public function folio()
    {
        return $this->belongsTo(FoliosModel::class);
    }

    public function precio()
    {
        return $this->belongsTo(Precios::class);
    }

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class);
    }

    public function re_ventas_estatus()
    {
        return $this->hasOne(ReVentasEstatusModel::class, ReVentasEstatusModel::VENTA_ID, self::ID)
            ->where(ReVentasEstatusModel::ACTIVO, '=', TRUE);
    }

    public function ventas_finalizadas()
    {
        return $this->hasOne(ReVentasEstatusModel::class, ReVentasEstatusModel::VENTA_ID, self::ID)
            ->where(ReVentasEstatusModel::ACTIVO, '=', TRUE)
            ->whereIn(
                ReVentasEstatusModel::ESTATUS_VENTA_ID,
                [
                    EstatusVentaModel::ESTATUS_FINALIZADA,
                    EstatusVentaModel::ESTATUS_DEVOLUCION
                ]
            );
    }

    public function detalle_venta()
    {
        return $this->hasMany(VentaProductoModel::class, VentaProductoModel::FOLIO_ID, self::FOLIO_ID);
    }

    public function almacen()
    {
        return $this->hasMany(Almacenes::class, Almacenes::ID, self::ALMACEN_ID);
    }

    public function tipo_precio()
    {
        return $this->belongsTo(CatTipoPrecioModel::class);
    }
}
