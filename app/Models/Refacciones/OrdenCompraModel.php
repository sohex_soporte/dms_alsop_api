<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\FoliosModel;

class OrdenCompraModel extends Modelo
{
    protected $table = 'orden_compra';
    const ID = "id";
    const PROVEEDOR_ID = "proveedor_id";

    const FOLIO_ID = "folio_id";
    const FACTURA_ID = "factura_id";
    const FACTURA = "factura";
    const PAGO_INICIAL = "pago_inicial";
    const PAGO = "pago";
    const PAGO_PENDIENTE = "pago_pendiente";
    const FECHA = 'fecha';
    const FECHA_PAGO = 'fecha_pago';
    const TIPO_PAGO_ID = 'tipo_pago_id';
    const OBSERVACIONES = 'observaciones';

    const REL_COMPRA_ESTATUS = 're_compras_estatus';
    const REL_DETALLE_COMPRA = 'detalle_compra';
    const REL_PROVEEDOR = 'proveedor';
    const REL_FOLIOS = 'folios';

    protected $fillable = [
        self::TIPO_PAGO_ID,
        self::PROVEEDOR_ID,
        self::FACTURA_ID,
        self::FACTURA,
        self::PAGO_INICIAL,
        self::PAGO,
        self::PAGO_PENDIENTE,
        self::OBSERVACIONES,
        self::FECHA_PAGO,
        self::FECHA,
        self::FOLIO_ID
    ];

    public function proveedor()
    {
        return $this->hasOne(ProveedorRefacciones::class, ProveedorRefacciones::ID, self::PROVEEDOR_ID);
    }

    public function folios()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID, self::FOLIO_ID);
    }
    
    public function rel_factura()
    {
        return $this->hasOne(Factura::class, Factura::ID, ReFacturaOrdenCompraModel::FACTURA_ID);
    }

    public function tipo_pago()
    {
        return $this->hasOne(CatTipoPagoModel::class, CatTipoPagoModel::ID, self::TIPO_PAGO_ID);
    }

    public function re_compra_estatus()
    {
        return $this->hasMany(ReComprasEstatusModel::class, ReComprasEstatusModel::COMPRA_ID, self::ID);
    }

    public function re_compra_factura()
    {
        return $this->hasMany(ReFacturaOrdenCompraModel::class, ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID, self::ID);
    }

    public function detalle_compra()
    {
        return $this->hasMany(ListaProductosOrdenCompraModel::class, ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, self::ID);
    }

    public function re_compras_estatus()
    {
        return $this->hasOne(ReComprasEstatusModel::class, ReComprasEstatusModel::COMPRA_ID, self::ID)
        ->where(ReComprasEstatusModel::ACTIVO, '=', TRUE);
    }
}
