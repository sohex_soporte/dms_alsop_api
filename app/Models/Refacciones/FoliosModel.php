<?php

namespace App\Models\Refacciones;

use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\Core\Modelo;

class FoliosModel extends Modelo
{
    protected $table = 'folios';
    const ID = "id";
    const FOLIO = "folio";
    const TIPO_PROCESO_ID = 'tipo_proceso_id';
    const STATUS = "status";

    const REL_CUENTA_COBRAR = 'cuenta_por_cobrar';
    protected $fillable = [
        self::FOLIO,
        self::STATUS,
        self::TIPO_PROCESO_ID
    ];

    public function factura()
    {
        $this->belongsTo(Factura::class);
    }

    public function tipo_cuenta()
    {
        $this->hasOne(CatalogoProcesosModel::class, CatalogoProcesosModel::ID, self::TIPO_PROCESO_ID);
    }

    public function cuenta_por_cobrar()
    {
        return $this->hasOne(CuentasPorCobrarModel::class, CuentasPorCobrarModel::FOLIO_ID, self::ID);
    }
}
