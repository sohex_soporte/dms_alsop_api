<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;

class VentaProductoModel extends Modelo
{
    protected $table = 'venta_producto';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const VENTA_ID = "venta_id";
    const PRECIO_ID = "precio_id";
    const CLIENTE_ID = "cliente_id";
    const FOLIO_ID = "folio_id";
    const CANTIDAD = "cantidad";
    const TOTAL_VENTA = "venta_total";
    const VALOR_UNITARIO = "valor_unitario";
    const DESCONTADO_ALMACEN = "descontado_almacen";
    const ORDEN_ORIGINAL = "orden_original";
    const ESTATUS_PRODUCTO_ORDEN = "estatus_producto_orden";
    const ID_INDEX_REQUISICION = "id_index_requisicion";
    const PRECIO_MANUAL = "precio_manual";

    const REL_CLIENTE = 'cliente';

    const ESTATUS_PRODUCTO_ORDEN_O = 'O'; //ORIGINAL
    const ESTATUS_PRODUCTO_ORDEN_B = 'B'; //BORRADO
    const ESTATUS_PRODUCTO_ORDEN_A = 'A'; //AGREGADO

    protected $fillable = [
        self::PRODUCTO_ID,
        self::CLIENTE_ID,
        self::TOTAL_VENTA,
        self::VALOR_UNITARIO,
        self::PRECIO_ID,
        self::FOLIO_ID,
        self::CANTIDAD,
        self::VENTA_ID,
        self::DESCONTADO_ALMACEN,
        self::ORDEN_ORIGINAL,
        self::ESTATUS_PRODUCTO_ORDEN,
        self::ID_INDEX_REQUISICION,
        self::PRECIO_MANUAL
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }

    public function precio()
    {
        return $this->belongsTo(Precios::class);
    }

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class);
    }

    public function rel_folio()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID);
    }
}
