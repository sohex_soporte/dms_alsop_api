<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class AnticiposModel extends Modelo
{
    protected $table = 'anticipos';
    
    const ID = "id";
    const CLIENTE_ID = 'cliente_id';
    const TIPO_PROCESO_ID = 'tipo_proceso_id';
    const ESTATUS_ID = 'estatus_id';
    const TOTAL = 'total';
    const FOLIO_ID = 'folio_id';
    const COMENTARIO = 'comentario';
    const FECHA = 'fecha';


    protected $fillable = [
        self::CLIENTE_ID,
        self::TIPO_PROCESO_ID,
        self::ESTATUS_ID,
        self::TOTAL,
        self::FOLIO_ID,
        self::COMENTARIO,
        self::FECHA
    ];
}
