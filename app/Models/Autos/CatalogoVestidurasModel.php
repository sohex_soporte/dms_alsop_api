<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class CatalogoVestidurasModel extends Modelo
{
    protected $table = 'catalogo_vestiduras';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";

    protected $fillable = [
        self::NOMBRE,
        self::CLAVE
    ];
}
