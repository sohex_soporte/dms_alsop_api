<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class EquipoOpcionalModel extends Modelo
{
    protected $table = 'equipo_opcional_unidad';
    const ID = "id";
    const ID_VENTA_AUTO = "id_venta_auto";
    const PRODUCTO_ID = "producto_id";
    const TOTAL = "total";
    const CANTIDAD = "cantidad";

    const PRECIO = "precio";
    protected $fillable = [
        self::ID_VENTA_AUTO,
        self::PRODUCTO_ID,
        self::CANTIDAD,
        self::TOTAL,
        self::PRECIO
    ];
}
