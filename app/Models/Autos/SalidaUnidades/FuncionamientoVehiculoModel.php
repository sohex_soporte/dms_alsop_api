<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class FuncionamientoVehiculoModel extends Modelo
{
    protected $table = 'funcionamiento_vehiculo';
    const ID = "id";

    const ASISTENTE_ARRANQUE = "asistente_arranque";
    const PRESERVACION_CARRIL = "preservacion_carril";
    const SENSORES_DEL_LAT = "sensores_del_lat";
    const BLIS = "blis";
    const ALERTA_TRAFICO_CRUZADO = "alert_trafico_cruzado";
    const ASISTENCIA_ACTIVA_ESTACI = "asistencia_activa_estaci";
    const CAMARA_REVERSA = "camara_reversa";
    const CAMARA_FRONTAL = "camara_frontal";
    const CAMARA_360_GRADOS = "camara_360_grados";
    const SITEMA_MON_PLL_TMS = "sistema_mon_pll_tms";

    const ID_VENTA_AUTO = "id_venta_auto";

    protected $fillable = [
        self::ASISTENTE_ARRANQUE,
        self::PRESERVACION_CARRIL,
        self::SENSORES_DEL_LAT,
        self::BLIS,
        self::ALERTA_TRAFICO_CRUZADO,
        self::ASISTENCIA_ACTIVA_ESTACI,
        self::CAMARA_REVERSA,
        self::CAMARA_FRONTAL,
        self::CAMARA_360_GRADOS,
        self::SITEMA_MON_PLL_TMS,
        self::ID_VENTA_AUTO
    ];
}
