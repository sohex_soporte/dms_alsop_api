<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class CatModelosModel extends Modelo
{
    protected $table = 'catalogo_modelos';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";
    const YEAR = "year";
    const DESCRIPCION = "descripcion";
    const PRECIO = "precio";
    const ID_MARCA = "id_marca";
    const TIEMPO_LAVADO = "tiempo_lavado";

    protected $fillable = [
        self::NOMBRE,
        self::CLAVE,
        self::TIEMPO_LAVADO,
        self::ID_MARCA,
        self::YEAR,
        self::DESCRIPCION,
        self::PRECIO,
    ];
}
