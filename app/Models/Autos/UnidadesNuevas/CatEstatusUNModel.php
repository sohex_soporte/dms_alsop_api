<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;

class CatEstatusUNModel extends Modelo
{
    protected $table = 'catalogo_estatus_un';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ID,
        self::ESTATUS
    ];
}
