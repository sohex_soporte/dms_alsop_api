<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DetalleRemisionModel extends Modelo
{
    protected $table = 'detalle_remision';
    const ID = "id";
    const COLOR_ID = "color_id";
    const TYPE_ID = "type_id";
    const MODELO_ID = "modelo_id";
    const CLAVE_PRODUCTO_SERVICIO = "clave_producto_servicio";
    const NO_IDENTIFICACION = "no_identificacion";
    const CANTIDAD = "cantidad";
    const CLAVE_UNIDAD = "clave_unidad";
    const UNIDAD = "unidad";
    const DESCRIPCION = "descripcion";
    const VALOR_UNITARIO = "valor_unitario";
    const IMPORTE = "importe";
    const BRAND = "brand";
    const REMISIONID = "remision_id";

    protected $fillable = [
        self::COLOR_ID,
        self::TYPE_ID,
        self::CLAVE_PRODUCTO_SERVICIO,
        self::NO_IDENTIFICACION,
        self::CANTIDAD,
        self::CLAVE_UNIDAD,
        self::UNIDAD,
        self::DESCRIPCION,
        self::VALOR_UNITARIO,
        self::IMPORTE,
        self::BRAND,
        self::MODELO_ID,
        self::REMISIONID,
    ];
}
