<?php

namespace App\Models\Autos\UnidadesNuevas;
use App\Models\Core\Modelo;
class CatTipoBicicletas extends Modelo
{
    protected $table = 'cat_tipo_bicicletas';
    const ID = "id";
    const TIPO_BICILETA = 'tipo_bicicleta';

    protected $fillable = [
        self::TIPO_BICILETA
    ];
}
