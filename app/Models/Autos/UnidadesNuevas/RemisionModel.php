<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;

class RemisionModel extends Modelo
{
    protected $table = 'remisiones';
    const ID = "id";
    const VERSION = "version";
    const SERIE = "serie";
    const FOLIO = "folio";
    const FECHA = "fecha";
    const SELLO = "sello";
    const NO_CERTIFICADO = "no_certificado";
    const CERTIFICADO = "certificado";
    const SUBTOTAL = "subtotal";
    const TIPO_CAMBIO = "tipo_cambio";
    const MONEDA = "moneda";
    const TOTAL = "total";
    const TIPO_COMPROBANTE = "tipo_comprobante";
    const METODO_PAGO = "metodo_pago";
    const FORMA_PAGO = "forma_pago";
    const LUGAR_EXPEDICION = "lugar_expedicion";
    const EMISOR_RFC = "emisor_rfc";
    const EMISOR_NOMBRE = "emisor_nombre";
    const EMISOR_REGIMEN_FISCAL = "emisor_regimen_fiscal";
    const RECEPTOR_RFC = "receptor_rfc";
    const RECEPTOR_NOMBRE = "receptor_nombre";
    const RECEPTOR_REGIMEN_FISCAL = "receptor_regimen_fiscal";
    const ESTATUS_ID = 'estatus_id'; //estatus //vendida, apartada etc
    const UBICACION_ID = 'ubicacion_id';
    const ULTIMO_SERVICIO = 'ultimo_servicio';
    const ID_STATUS_UNIDAD = 'id_status_unidad';
    const USERID = 'user_id';


    const XML_REMISION = 'xml_remision';
    const DIRECTORIO_FACTURAS = 'remisiones';
    const ESTATUS_VENTA_DISPONIBLE = 1;
    const ESTATUS_VENTA_APARTADA = 2;
    const ESTATUS_VENTA_VENDIDA = 3;

    protected $fillable = [
        self::VERSION,
        self::SERIE,
        self::FOLIO,
        self::FECHA,
        self::SELLO,
        self::NO_CERTIFICADO,
        self::CERTIFICADO,
        self::SUBTOTAL,
        self::TIPO_CAMBIO,
        self::MONEDA,
        self::TOTAL,
        self::TIPO_COMPROBANTE,
        self::METODO_PAGO,
        self::FORMA_PAGO,
        self::LUGAR_EXPEDICION,
        self::EMISOR_RFC,
        self::EMISOR_NOMBRE,
        self::EMISOR_REGIMEN_FISCAL,
        self::RECEPTOR_RFC,
        self::RECEPTOR_NOMBRE,
        self::RECEPTOR_REGIMEN_FISCAL,
        self::ESTATUS_ID,
        self::UBICACION_ID,
        self::ULTIMO_SERVICIO,
        self::ID_STATUS_UNIDAD,
        self::USERID,
        
    ];
}
