<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class HistorialEstatusFinanciamientosModel extends Modelo
{
    protected $table = 'historial_estatus_financiamientos';
    const ID = 'id';
    const ID_ESTATUS = 'id_estatus';
    const COMENTARIO = 'comentario';
    const ID_USUARIO = 'id_usuario';
    const ID_FINANCIAMIENTO = 'id_financiamiento';
    const TIPO_COMENTARIO = 'tipo_comentario';

    protected $fillable = [
        self::ID,
        self::ID_ESTATUS,
        self::COMENTARIO,
        self::ID_USUARIO,
        self::ID_FINANCIAMIENTO,
        self::TIPO_COMENTARIO
    ];
}
