<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
class CatPerfilFinanciamientoModel extends Modelo
{
    protected $table = 'cat_perfil_financiamiento';
    const ID = 'id';
    const PERFIL = 'perfil';

    protected $fillable = [
        self::ID,
        self::PERFIL
    ];
}
