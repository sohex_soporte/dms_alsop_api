<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;

class EmisorFactura extends Modelo
{
    protected $table = 'emisor_factura';
    const ID = "id";
    const NOMBRE = "nombre";
    const RFC = "rfc";
    const REGIMEN_FISCAL = "regimen_fiscal";
    const FACTURA_ID = "factura_id";

    protected $fillable = [
        self::RFC,
        self::NOMBRE,
        self::REGIMEN_FISCAL,
        self::FACTURA_ID
    ];
}
