<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;

class ReceptorFactura extends Modelo
{
    const ID = "id";
    const RFC = "rfc";
    const NOMBRE = "nombre";
    const USOCFDI = "usocfdi";
    const FACTURA_ID = "factura_id";

    protected $fillable = [
        self::RFC,
        self::NOMBRE,
        self::USOCFDI,
        self::FACTURA_ID
    ];

    public function factura()
    {
        $this->belongsTo(Factura::class);
    }
}
