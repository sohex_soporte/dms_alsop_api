<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\DescuentosModel;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use Illuminate\Support\Facades\DB;


class ServicioDescuentos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'descuentos';
        $this->modelo = new DescuentosModel();
        $this->servicioCuentaPorCobrar = new ServicioCuentaPorCobrar();
        $this->servicioAbono = new ServicioAbono();
    }

    public function getReglasGuardar()
    {
        return [
            DescuentosModel::FOLIO_ID => 'required|numeric',
            DescuentosModel::TOTAL_APLICAR => 'required|numeric',
            DescuentosModel::TIPO_DESCUENTO => 'required|numeric',
            DescuentosModel::PORCENTAJE => 'required|numeric',
            DescuentosModel::CANTIDAD_DESCONTADA => 'required|numeric',
            DescuentosModel::TOTAL_DESCUENTO => 'required|numeric',
            DescuentosModel::COMENTARIO => 'nullable|string',
            DescuentosModel::FECHA => 'required|date',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DescuentosModel::FOLIO_ID => 'required|numeric',
            DescuentosModel::TOTAL_APLICAR => 'required|numeric',
            DescuentosModel::TIPO_DESCUENTO => 'required|numeric',
            DescuentosModel::PORCENTAJE => 'required|numeric',
            DescuentosModel::CANTIDAD_DESCONTADA => 'required|numeric',
            DescuentosModel::TOTAL_DESCUENTO => 'required|numeric',
            DescuentosModel::COMENTARIO => 'nullable|string',
            DescuentosModel::FECHA => 'required|date',
        ];
    }

    public function getReglasAplicarFolio()
    {
        return [
            DescuentosModel::FOLIO_ID => 'required|numeric|exists:folios,id',
        ];
    }

    public function getByFolioId($folio_id)
    {
        return $this->modelo->where(DescuentosModel::FOLIO_ID, $folio_id)->get();
    }

    public function getByFolioTipo($request)
    {
        return $this->modelo->where([
            DescuentosModel::FOLIO_ID =>  $request->get(DescuentosModel::FOLIO_ID),
            DescuentosModel::TIPO_DESCUENTO =>  $request->get(DescuentosModel::TIPO_DESCUENTO)
        ])->first();
    }

    public function getByTotalByFolioId($folio_id)
    {
        return $this->modelo->select(
            DB::raw('sum(descuentos.total_descuento) as sumatotal')
        )
            ->where([
                DescuentosModel::FOLIO_ID => $folio_id,
            ])->groupBy(
                DescuentosModel::getTableName() . '.' . DescuentosModel::FOLIO_ID
            )
            ->first();
    }

    public function aplicarDescuento($request)
    {
        $cuenta_por_cobrar = $this->servicioCuentaPorCobrar->getWhere(CuentasPorCobrarModel::FOLIO_ID, $request->get(DescuentosModel::FOLIO_ID))->first();
        if ($cuenta_por_cobrar) {
            $this->servicioCuentaPorCobrar->massUpdateWhereId(CuentasPorCobrarModel::FOLIO_ID, $request->get(DescuentosModel::FOLIO_ID), [
                CuentasPorCobrarModel::TOTAL => $cuenta_por_cobrar->total - $request->get(DescuentosModel::CANTIDAD_DESCONTADA),
                CuentasPorCobrarModel::IMPORTE => $cuenta_por_cobrar->importe - $request->get(DescuentosModel::CANTIDAD_DESCONTADA)
            ]);
            $this->servicioAbono->recalcularAbonos($cuenta_por_cobrar->id);
        }
        return $cuenta_por_cobrar;
    }
}
