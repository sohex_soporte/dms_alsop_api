<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\EstatusAnticiposModel;

class ServicioEstatusAnticipos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus anticipos';
        $this->modelo = new EstatusAnticiposModel();
    }

    public function getReglasGuardar()
    {
        return [
            EstatusAnticiposModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EstatusAnticiposModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre'
        ];
    }
}
