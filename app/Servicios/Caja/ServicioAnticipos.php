<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\AnticiposModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\Caja\EstatusAnticiposModel;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use Illuminate\Support\Facades\DB;


class ServicioAnticipos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'anticipos';
        $this->modelo = new AnticiposModel();
        $this->servicioCuentaPorCobrar = new ServicioCuentaPorCobrar();
        $this->servicioAbono = new ServicioAbono();
    }

    public function getReglasGuardar()
    {
        return [
            AnticiposModel::CLIENTE_ID => 'required|numeric|exists:clientes,id',
            AnticiposModel::TIPO_PROCESO_ID => 'required|numeric|exists:catalogo_procesos,id',
            AnticiposModel::ESTATUS_ID => 'required|numeric|exists:estatus_anticipos,id',
            AnticiposModel::TOTAL => 'required|numeric',
            AnticiposModel::FOLIO_ID => 'nullable|numeric',
            AnticiposModel::COMENTARIO => 'nullable|string',
            AnticiposModel::FECHA => 'required|date',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            AnticiposModel::CLIENTE_ID => 'required|numeric|exists:clientes,id',
            AnticiposModel::TIPO_PROCESO_ID => 'required|numeric|exists:catalogo_procesos,id',
            AnticiposModel::ESTATUS_ID => 'required|numeric|exists:estatus_anticipos,id',
            AnticiposModel::TOTAL => 'required|numeric',
            AnticiposModel::FOLIO_ID => 'nullable|numeric',
            AnticiposModel::COMENTARIO => 'nullable|string',
            AnticiposModel::FECHA => 'required|date',
        ];
    }
    public function getReglasEstatus()
    {
        return [
            AnticiposModel::ESTATUS_ID => 'required|numeric|exists:estatus_anticipos,id',
        ];
    }

    public function getReglasAplicarFolio()
    {
        return [
            AnticiposModel::FOLIO_ID => 'required|numeric|exists:folios,id',
        ];
    }

    public function getAll($request)
    {
        $tbl_principal = AnticiposModel::getTableName();
        $tbl_clientes = ClientesModel::getTableName();
        $tbl_procesos = CatalogoProcesosModel::getTableName();
        $tbl_estatus = EstatusAnticiposModel::getTableName();

        $consulta = $this->modelo
            ->select(
                $tbl_principal . '.*',
                $tbl_clientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tbl_clientes . '.' . ClientesModel::NOMBRE,
                $tbl_clientes . '.' . ClientesModel::APELLIDO_PATERNO,
                $tbl_clientes . '.' . ClientesModel::APELLIDO_MATERNO,
                $tbl_procesos . '.' . CatalogoProcesosModel::DESCRIPCION . ' as proceso',
                $tbl_estatus . '.' . EstatusAnticiposModel::NOMBRE . ' as estatus',
            )
            ->join($tbl_clientes, $tbl_clientes . '.' . ClientesModel::ID, $tbl_principal . '.' . AnticiposModel::CLIENTE_ID)
            ->join($tbl_procesos, $tbl_procesos . '.' . CatalogoProcesosModel::ID, $tbl_principal . '.' . AnticiposModel::TIPO_PROCESO_ID)
            ->join($tbl_estatus, $tbl_estatus . '.' . EstatusAnticiposModel::ID, $tbl_principal . '.' . AnticiposModel::ESTATUS_ID);

        if ($request->get(AnticiposModel::CLIENTE_ID)) {
            $consulta->where($tbl_principal . '.' . AnticiposModel::CLIENTE_ID, $request->get(AnticiposModel::CLIENTE_ID));
        }
        if ($request->get(AnticiposModel::TIPO_PROCESO_ID)) {
            $consulta->where($tbl_principal . '.' . AnticiposModel::TIPO_PROCESO_ID, $request->get(AnticiposModel::TIPO_PROCESO_ID));
        }
        if ($request->get(AnticiposModel::ESTATUS_ID)) {
            $consulta->where($tbl_principal . '.' . AnticiposModel::ESTATUS_ID, $request->get(AnticiposModel::ESTATUS_ID));
        }
        return $consulta->get();
    }

    public function getByFolioId($folio_id)
    {
        return $this->modelo->where(AnticiposModel::FOLIO_ID, $folio_id)->get();
    }

    public function getByTotalByFolioId($folio_id)
    {
        return $this->modelo->select(
            DB::raw('sum(anticipos.total) as sumatotal')
        )
            ->where([
                AnticiposModel::FOLIO_ID => $folio_id,
                AnticiposModel::ESTATUS_ID => EstatusAnticiposModel::ESTATUS_APLICADO
            ])->groupBy(
                AnticiposModel::getTableName() . '.' . AnticiposModel::FOLIO_ID
            )
            ->first();
    }

    public function changeEstatus($request, $id)
    {
        return $this->massUpdateWhereId(AnticiposModel::ID, $id, [
            AnticiposModel::ESTATUS_ID => $request->get(AnticiposModel::ESTATUS_ID)
        ]);
    }

    public function aplicarAnticipo($request, $id)
    {
        $aplicar_folio = $this->massUpdateWhereId(AnticiposModel::ID, $id, [
            AnticiposModel::ESTATUS_ID => EstatusAnticiposModel::ESTATUS_APLICADO,
            AnticiposModel::FOLIO_ID => $request->get(AnticiposModel::FOLIO_ID)
        ]);
        if ($aplicar_folio) {
            $anticipo = $this->getById($id);
            $cuenta_por_cobrar = $this->servicioCuentaPorCobrar->getWhere(CuentasPorCobrarModel::FOLIO_ID, $request->get(AnticiposModel::FOLIO_ID))->first();
            if ($cuenta_por_cobrar) {
                $this->servicioCuentaPorCobrar->massUpdateWhereId(CuentasPorCobrarModel::FOLIO_ID, $request->get(AnticiposModel::FOLIO_ID),[
                    CuentasPorCobrarModel::TOTAL => $cuenta_por_cobrar->total - $anticipo->total,
                    CuentasPorCobrarModel::IMPORTE => $cuenta_por_cobrar->importe - $anticipo->total
                ]);
                $this->servicioAbono->recalcularAbonos($cuenta_por_cobrar->id);
            }
        }
        return $aplicar_folio;
    }
}
