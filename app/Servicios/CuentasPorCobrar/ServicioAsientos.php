<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\AsientoModel;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Polizas\ServicioMovimientos;
use App\Models\Caja\DescuentosModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\TipoPolizasModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use DB;

class ServicioAsientos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'Asientos';
        $this->modelo = new AsientoModel();
        $this->servicioCurl = new ServicioCurl();
        $this->servicioMovimientos = new ServicioMovimientos();
        $this->modelo_descuentos = new DescuentosModel();
    }

    public function getReglasGuardar()
    {
        return [
            AsientoModel::CONCEPTO => 'required',
            AsientoModel::TOTAL_PAGO => 'required|numeric',
            AsientoModel::CUENTA_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::TIPO_ASIENTO_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::FECHA => 'nullable|date',
            AsientoModel::FOLIO_ID => 'nullable|exists:folios,id',
            AsientoModel::CLAVE_POLIZA => 'nullable|exists:tipo_polizas,clave',
            AsientoModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            AsientoModel::ESTATUS => 'nullable',
            AsientoModel::DEPARTAMENTO => 'nullable|numeric'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            AsientoModel::NOMBRE => 'required',
            AsientoModel::REFERENCIA => 'required',
            AsientoModel::CONCEPTO => 'required',
            AsientoModel::TOTAL => 'required|numeric',
            AsientoModel::CUENTA_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::TIPO_ASIENTO_ID => 'required|numeric|exists:catalogo_cuentas,id',
            AsientoModel::FECHA => 'nullable|date',
            AsientoModel::FOLIO_ID => 'nullable|exists:folios,id',
            AsientoModel::CLAVE_POLIZA => 'nullable|exists:tipo_polizas,clave'
        ];
    }

    public function getReglasPolizaHojalateria()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_REFACCIONES => 'nullable|numeric',
            AsientoModel::TOTAL_MANO_OBRA => 'nullable|numeric',
            AsientoModel::TOTAL_CLIENTE_SERVICIOS => 'nullable|numeric',
            AsientoModel::TOTAL_INVENTARIO_PROCESO => 'nullable|numeric',
            AsientoModel::TOTAL_IVA_FACTURADO => 'nullable|numeric',
            AsientoModel::TOTAL_COSTO_REFACCION => 'nullable|numeric'
        ];
    }

    public function getReglasPolizaVentaMostrador()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id'
        ];
    }

    public function getReglasPolizaGarantias()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_REFACCIONES => 'nullable|numeric',
            AsientoModel::TOTAL_MANO_OBRA => 'nullable|numeric',
            AsientoModel::TOTAL_GARANTIAS_POR_COBRAR => 'nullable|numeric',
            AsientoModel::TOTAL_INVENTARIO_PROCESO => 'nullable|numeric',
            AsientoModel::TOTAL_IVA_FACTURADO => 'nullable|numeric',
            AsientoModel::TOTAL_COSTO_REFACCION => 'nullable|numeric'
        ];
    }

    public function getReglasPolizaServicio()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_REFACCIONES => 'nullable|numeric',
            AsientoModel::TOTAL_MANO_OBRA => 'nullable|numeric',
            AsientoModel::TOTAL_CLIENTE_SERVICIOS => 'nullable|numeric',
            AsientoModel::TOTAL_INVENTARIO_PROCESO => 'nullable|numeric',
            AsientoModel::TOTAL_IVA_FACTURADO => 'nullable|numeric',
            AsientoModel::TOTAL_COSTO_REFACCION => 'nullable|numeric'
        ];
    }

    public function getReglasActualizaPolizaServicio()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            DescuentosModel::TIPO_DESCUENTO => 'required|numeric'
        ];
    }

    public function getReglasPolizaServicioInterno()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::PROCESOS => 'nullable|numeric',
            AsientoModel::ACONDICIONAMIENTO => 'nullable|numeric',
            AsientoModel::CORTESIA_NUEVOS => 'nullable|numeric'
        ];
    }

    public function getReglasPolizaRefaccionTaller()
    {
        return [
            CuentasPorCobrarModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            AsientoModel::TOTAL_COSTO_PIEZA => 'required|numeric'
        ];
    }

    public function getReglasPolizaCompraFordPlanta()
    {
        return [
            CuentasPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|exists:cuentas_por_pagar,id',
            AsientoModel::TOTAL_FACTURA_SIN_IVA => 'required|numeric',
            AsientoModel::TOTAL_IVA_FACTURA => 'required|numeric',
            AsientoModel::TOTAL_FACTURA => 'required|numeric'
        ];
    }

    public function getReglasPolizaCompraProveedorExterno()
    {
        return [
            CuentasPorPagarModel::CUENTA_POR_PAGAR_ID => 'required|exists:cuentas_por_pagar,id',
            AsientoModel::TOTAL_FACTURA_SIN_IVA => 'required|numeric',
            AsientoModel::TOTAL_IVA_FACTURA => 'required|numeric',
            AsientoModel::TOTAL_FACTURA => 'required|numeric'
        ];
    }

    public function servicioPolizaVentaMostrador($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::CM;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::VENTA_MOSTRADOR;
        $iva = $cuenta_por_cobrar['total'] * 0.16;
        $costo_refaccion = $cuenta_por_cobrar['total'] - $iva;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_340101;
        $data['total_pago'] = $cuenta_por_cobrar['total'];
        $respuesta[CatalogoCuentasModel::CUENTA_340101] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $iva;
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $costo_refaccion;
        $respuesta[CatalogoCuentasModel::CUENTA_340101] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_430001;
        $data['total_pago'] = $costo_refaccion;
        $respuesta[CatalogoCuentasModel::CUENTA_340101] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_BANORTE;
        $data['total_pago'] = $cuenta_por_cobrar['total'] + $iva;
        $respuesta[CatalogoCuentasModel::CUENTA_BANORTE] = $this->curl_asiento_api($data);


        return $respuesta;
    }

    public function servicioPolizaVentaMostradorCredito($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::CM;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::VENTA_MOSTRADOR;
        $iva = $cuenta_por_cobrar['total'] * 0.16;
        $costo_refaccion = $cuenta_por_cobrar['total'] - $iva;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_340101;
        $data['total_pago'] = $cuenta_por_cobrar['total'] - $iva;
        $respuesta[CatalogoCuentasModel::CUENTA_340101] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110003;
        $data['total_pago'] = $cuenta_por_cobrar['total'];
        $respuesta[CatalogoCuentasModel::CUENTA_110003] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115015;
        $data['total_pago'] = $iva;
        $respuesta[CatalogoCuentasModel::CUENTA_115015] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_440102;
        $data['total_pago'] = $costo_refaccion;
        $respuesta[CatalogoCuentasModel::CUENTA_440102] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $costo_refaccion;
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);


        return $respuesta;
    }

    public function servicioPolizaHojalateria($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::QH;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::HOJALATERIA;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_REFACCIONES];
        $respuesta[CatalogoCuentasModel::CUENTA_370002] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370003;
        $data['total_pago'] = $request[AsientoModel::TOTAL_MANO_OBRA];
        $respuesta[CatalogoCuentasModel::CUENTA_370003] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_CLIENTE_SERVICIOS];
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_INVENTARIO_PROCESO];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115015;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $respuesta[CatalogoCuentasModel::CUENTA_115015] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_470001;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_REFACCION];
        $respuesta[CatalogoCuentasModel::CUENTA_470001] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaServicios($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::QT;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;

        /*$total_abono = $request[AsientoModel::TOTAL_REFACCIONES] + $request[AsientoModel::TOTAL_MANO_OBRA] + $request[AsientoModel::TOTAL_INVENTARIO_PROCESO] + $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $total_cargo = $request[AsientoModel::TOTAL_CLIENTE_SERVICIOS] + $request[AsientoModel::TOTAL_COSTO_REFACCION] + $request[AsientoModel::TOTAL_DESCUENTO_REFACCION] + $request[AsientoModel::TOTAL_DESCUENTO_MANO_OBRA];

        if ($total_abono != $total_cargo) {
            return false;
        }*/

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_350001;
        $data['total_pago'] = $request[AsientoModel::TOTAL_REFACCIONES];
        $respuesta[CatalogoCuentasModel::CUENTA_350001] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_350003;
        $data['total_pago'] = $request[AsientoModel::TOTAL_MANO_OBRA];
        $respuesta[CatalogoCuentasModel::CUENTA_350003] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_CLIENTE_SERVICIOS];
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_INVENTARIO_PROCESO];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115015;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $respuesta[CatalogoCuentasModel::CUENTA_115015] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_450001;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_REFACCION];
        $respuesta[CatalogoCuentasModel::CUENTA_450001] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function getByTotalDescuentosByFolioId($folio_id)
    {
        return $this->modelo_descuentos->select(
            DB::raw('sum(descuentos.cantidad_descontada) as suma_descuento')
        )
            ->where([
                DescuentosModel::FOLIO_ID => $folio_id,
            ])->groupBy(
                DescuentosModel::getTableName() . '.' . DescuentosModel::FOLIO_ID
            )
            ->first();
    }

    public function getDescuentoByFolioAndTipo($folio_id, $tipo)
    {
        return $this->modelo_descuentos
            ->where([
                DescuentosModel::FOLIO_ID => $folio_id,
                DescuentosModel::TIPO_DESCUENTO => $tipo
            ])->first();
    }

    public function servicioActualizaPoliza($cuenta_por_cobrar_id, $tipo_descuento)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($cuenta_por_cobrar_id);
        $data['descuentos'] = $this->getByTotalDescuentosByFolioId($cuenta_por_cobrar['folio_id']);
        $api_descuento_refaccion = $this->getDescuentoByFolioAndTipo($cuenta_por_cobrar['folio_id'], 1);
        $api_descuento_mano_obra = $this->getDescuentoByFolioAndTipo($cuenta_por_cobrar['folio_id'], 2);
        // $data = $cuenta_por_cobrar_id;
        $data['descuento_refaccion'] = $api_descuento_refaccion ? $api_descuento_refaccion->cantidad_descontada : 0;
        $data['descuento_mano_obra'] = $api_descuento_mano_obra ? $api_descuento_mano_obra->cantidad_descontada : 0;
        $data['tipo_descuento'] = $tipo_descuento;
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::QT;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;
        switch ($cuenta_por_cobrar['tipo_proceso_id']) {
            case CatalogoProcesosModel::PROCESO_SERVICIOS:
                $respuesta = $this->actualizaProcesoServicios($data);
                break;
            case CatalogoProcesosModel::PROCESO_HOJALATERIA:
                $respuesta = $this->actualizaProcesoHojalateria($data);
                break;
            default:
                break;
        }

        return $respuesta;
    }

    public function actualizaProcesoServicios($data)
    {
        $refacciones = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 350001);
        $data_refacciones['asiento_id'] = $refacciones->asiento_id;
        $data_refacciones['abono'] = ($refacciones->abono - $data['descuento_refaccion']);
        $respuesta[CatalogoCuentasModel::CUENTA_350001] = $this->curl_asiento_api_modificar($data_refacciones);

        $mano_obra = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 350003);
        $data_mano_obra['asiento_id'] = $mano_obra->asiento_id;
        $data_mano_obra['abono'] = ($mano_obra->abono - $data['descuento_mano_obra']);
        $respuesta[CatalogoCuentasModel::CUENTA_350003] = $this->curl_asiento_api_modificar($data_mano_obra);

        $servicio_iva_facturado = $this->curl_asiento_api_detalle($data['folio_id'], 115015);
        $data_iva_facturado_data['asiento_id'] = $servicio_iva_facturado->asiento_id;
        $data_iva_facturado_data['abono'] = ((($data_refacciones['abono'] + $data_mano_obra['abono']) - $data['descuentos']->suma_descuento) * 0.16);
        $respuesta[CatalogoCuentasModel::CUENTA_115015] = $this->curl_asiento_api_modificar($data_iva_facturado_data);

        $servicio_clientes = $this->curl_asiento_api_detalle($data['folio_id'], 110004);
        $data_clientes_data['asiento_id'] = $servicio_clientes->asiento_id;
        $data_clientes_data['cargo'] = (($data_refacciones['abono'] + $data_mano_obra['abono'] + $data_iva_facturado_data['abono']) - $data['descuentos']->suma_descuento);
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api_modificar($data_clientes_data);

        $costo_refaccion = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 450001);
        $data_costo_refaccion['asiento_id'] = $costo_refaccion->asiento_id;
        $data_costo_refaccion['cargo'] = $data_refacciones['abono'];
        $respuesta[CatalogoCuentasModel::CUENTA_450001] = $this->curl_asiento_api_modificar($data_costo_refaccion);

        $inventario_proceso = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 151000);
        $data_inventario_proceso['asiento_id'] = $inventario_proceso->asiento_id;
        $data_inventario_proceso['abono'] = $data_refacciones['abono'];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api_modificar($data_inventario_proceso);

        if ($data['tipo_descuento'] == 1) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_350008;
            $data['total_pago'] = $data['descuento_refaccion'];
            $respuesta[CatalogoCuentasModel::CUENTA_350008] = $this->curl_asiento_api($data);
        }

        if ($data['tipo_descuento'] == 2) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_350007;
            $data['total_pago'] = $data['descuento_mano_obra'];
            $respuesta[CatalogoCuentasModel::CUENTA_350007] = $this->curl_asiento_api($data);
        }

        return $respuesta;
    }

    public function actualizaProcesoHojalateria($data)
    {
        $refacciones = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 370002);
        $data_refacciones['asiento_id'] = $refacciones->asiento_id;
        $data_refacciones['abono'] = ($refacciones->abono - $data['descuento_refaccion']);
        $respuesta[CatalogoCuentasModel::CUENTA_370002] = $this->curl_asiento_api_modificar($data_refacciones);

        $mano_obra = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 370003);
        $data_mano_obra['asiento_id'] = $mano_obra->asiento_id;
        $data_mano_obra['abono'] = ($mano_obra->abono - $data['descuento_mano_obra']);
        $respuesta[CatalogoCuentasModel::CUENTA_370003] = $this->curl_asiento_api_modificar($data_mano_obra);

        $costo_refaccion = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 470001);
        $data_costo_refaccion['asiento_id'] = $costo_refaccion->asiento_id;
        $data_costo_refaccion['cargo'] = ($costo_refaccion->cargo - $data['descuento_refaccion']);
        $respuesta[CatalogoCuentasModel::CUENTA_470001] = $this->curl_asiento_api_modificar($data_costo_refaccion);

        $servicio_iva_facturado = $this->curl_asiento_api_detalle($data['folio_id'], 115015);
        $data_iva_facturado_data['asiento_id'] = $servicio_iva_facturado->asiento_id;
        $data_iva_facturado_data['abono'] = ((($data_refacciones['abono'] + $data_mano_obra['abono']) - $data['descuentos']->suma_descuento) * 0.16);
        $respuesta[CatalogoCuentasModel::CUENTA_115015] = $this->curl_asiento_api_modificar($data_iva_facturado_data);

        $servicio_clientes = $this->curl_asiento_api_detalle($data['folio_id'], 110004);
        $data_clientes_data['asiento_id'] = $servicio_clientes->asiento_id;
        $data_clientes_data['cargo'] = (($data_refacciones['abono'] + $data_mano_obra['abono'] + $data_iva_facturado_data['abono']) - $data['descuentos']->suma_descuento);
        $respuesta[CatalogoCuentasModel::CUENTA_110004] = $this->curl_asiento_api_modificar($data_clientes_data);

        $inventario_proceso = $this->curl_asiento_api_detalle($data[AsientoModel::FOLIO_ID], 151000);
        $data_inventario_proceso['asiento_id'] = $inventario_proceso->asiento_id;
        $data_inventario_proceso['abono'] = $data_refacciones['abono'];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api_modificar($data_inventario_proceso);

        if ($data['tipo_descuento'] == 1) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370004;
            $data['total_pago'] = $data['descuento_refaccion'];
            $respuesta[CatalogoCuentasModel::CUENTA_370004] = $this->curl_asiento_api($data);
        }
        if ($data['tipo_descuento'] == 2) {
            $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
            $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_370004;
            $data['total_pago'] = $data['descuento_mano_obra'];
            $respuesta[CatalogoCuentasModel::CUENTA_370004] = $this->curl_asiento_api($data);
        }

        return $respuesta;
    }

    public function servicioPolizaGarantias($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::QG;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_330004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_REFACCIONES];
        $respuesta[CatalogoCuentasModel::CUENTA_330004] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_350004;
        $data['total_pago'] = $request[AsientoModel::TOTAL_MANO_OBRA];
        $respuesta[CatalogoCuentasModel::CUENTA_350004] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_110008;
        $data['total_pago'] = $request[AsientoModel::TOTAL_GARANTIAS_POR_COBRAR];
        $respuesta[CatalogoCuentasModel::CUENTA_110008] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_INVENTARIO_PROCESO];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115015;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURADO];
        $respuesta[CatalogoCuentasModel::CUENTA_115015] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_450002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_REFACCION];
        $respuesta[CatalogoCuentasModel::CUENTA_450002] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaServicioInterno($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::DO;
        $data[AsientoModel::ESTATUS] = isset($cuenta_por_cobrar['estatus']) ? $cuenta_por_cobrar['estatus'] : 'POR_APLICAR';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::SERVICIOS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::PROCESOS];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_510034;
        $data['total_pago'] = $request[AsientoModel::ACONDICIONAMIENTO];
        $respuesta[CatalogoCuentasModel::CUENTA_510034] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_510034;
        $data['total_pago'] = $request[AsientoModel::CORTESIA_NUEVOS];
        $respuesta[CatalogoCuentasModel::CUENTA_510034] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaRefaccionTaller($request)
    {
        $cuenta_por_cobrar = $this->servicioMovimientos->getTipoCuentaxCobrar($request['cuenta_por_cobrar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_cobrar['concepto']) ? $cuenta_por_cobrar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::V0;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::CLIENTE_ID] = isset($cuenta_por_cobrar['cliente_id']) ? $cuenta_por_cobrar['cliente_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_cobrar['fecha']) ? $cuenta_por_cobrar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_cobrar['folio_id']) ? $cuenta_por_cobrar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::TALLER;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_151000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_PIEZA];
        $respuesta[CatalogoCuentasModel::CUENTA_151000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_COSTO_PIEZA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaCompraFordPlanta($request)
    {
        ParametrosHttpValidador::validar_array($request, $this->getReglasPolizaCompraFordPlanta());
        $cuenta_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_pagar['concepto']) ? $cuenta_por_pagar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::ND;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::PROVEEDOR_ID] = isset($cuenta_por_pagar['proveedor_id']) ? $cuenta_por_pagar['proveedor_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_pagar['fecha']) ? $cuenta_por_pagar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_pagar['folio_id']) ? $cuenta_por_pagar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::COMPRAS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA_SIN_IVA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_202000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_202000] = $this->curl_asiento_api($data);

        return $respuesta;
    }

    public function servicioPolizaCompraProveedorExterno($request)
    {
        ParametrosHttpValidador::validar_array($request, $this->getReglasPolizaCompraProveedorExterno());
        $cuenta_por_pagar = $this->servicioMovimientos->getTipoCuentaxPagar($request['cuenta_por_pagar_id']);
        $respuesta = [];
        $data[AsientoModel::CONCEPTO] = isset($cuenta_por_pagar['concepto']) ? $cuenta_por_pagar['concepto'] : '';
        $data[AsientoModel::CLAVE_POLIZA] = TipoPolizasModel::ND;
        $data[AsientoModel::ESTATUS] = 'APLICADO';
        $data[AsientoModel::PROVEEDOR_ID] = isset($cuenta_por_pagar['proveedor_id']) ? $cuenta_por_pagar['proveedor_id'] : '';
        $data[AsientoModel::FECHA] = isset($cuenta_por_pagar['fecha']) ? $cuenta_por_pagar['fecha'] : '';
        $data[AsientoModel::FOLIO_ID] = isset($cuenta_por_pagar['folio_id']) ? $cuenta_por_pagar['folio_id'] : '';
        $data[AsientoModel::DEPARTAMENTO] = AsientoModel::COMPRAS;

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_131000;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA_SIN_IVA];
        $respuesta[CatalogoCuentasModel::CUENTA_131000] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::CARGO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_115005;
        $data['total_pago'] = $request[AsientoModel::TOTAL_IVA_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_115005] = $this->curl_asiento_api($data);

        $data['tipo_asiento_id'] = CatalogoCuentasModel::ABONO;
        $data['cuenta_id'] = CatalogoCuentasModel::CUENTA_210002;
        $data['total_pago'] = $request[AsientoModel::TOTAL_FACTURA];
        $respuesta[CatalogoCuentasModel::CUENTA_210002] = $this->curl_asiento_api($data);

        return $respuesta;
    }


    public function curl_asiento_api($data)
    {
        return $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/agregar', [
            AsientoModel::CONCEPTO => isset($data['concepto']) ? $data['concepto'] : '',
            AsientoModel::TOTAL => isset($data['total_pago']) ? $data['total_pago'] : '',
            AsientoModel::CUENTA => isset($data['cuenta_id']) ? $data['cuenta_id'] : '',
            AsientoModel::TIPO => isset($data['tipo_asiento_id']) ? $data['tipo_asiento_id'] : '',
            AsientoModel::FECHA =>  date('Y-m-d'),
            AsientoModel::FOLIO => isset($data['folio_id']) ? $data['folio_id'] : '',
            AsientoModel::CLAVE_POLIZA => isset($data['clave_poliza']) ? $data['clave_poliza'] : '',
            AsientoModel::CLIENTE_ID => isset($data['cliente_id']) ? $data['cliente_id'] : '',
            AsientoModel::ESTATUS => isset($data['estatus']) ? $data['estatus'] : '',
            AsientoModel::DEPARTAMENTO => isset($data['departamento']) ? $data['departamento'] : '',
            AsientoModel::PROVEEDOR_ID => isset($data['proveedor_id']) ? $data['proveedor_id'] : ''
        ], false);
    }

    public function curl_asiento_api_modificar($data)
    {
        return $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'asientos/api/modificar', [
            AsientoModel::ASIENTO_ID => isset($data['asiento_id']) ? $data['asiento_id'] : '',
            AsientoModel::CARGO => isset($data['cargo']) ? $data['cargo'] : '',
            AsientoModel::ABONO => isset($data['abono']) ? $data['abono'] : '',
        ], false);
    }

    public function curl_asiento_api_detalle($folio_id, $cuenta)
    {
        $api = $this->servicioCurl->curlGet(env('URL_CONTABILIDAD') . 'asientos/api/detalle?folio=' . $folio_id . '&cuenta=' . $cuenta, false);
        $respuesta = json_decode($api);
        return $respuesta->data ? $respuesta->data[0] : [];
    }
}
