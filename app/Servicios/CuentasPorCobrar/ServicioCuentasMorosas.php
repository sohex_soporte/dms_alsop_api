<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\CuentasMorosasModel;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;

class ServicioCuentasMorosas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cuentas morosas';
        $this->modelo = new CuentasMorosasModel();
        $this->modelo_cuentas_por_cobrar = new CuentasPorCobrarModel();
    }

    public function getReglasGuardar()
    {
        return [
            CuentasMorosasModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            CuentasMorosasModel::ABONO_ID => 'required|exists:abonos_por_cobrar,id',
            CuentasMorosasModel::DIAS_MORATORIOS => 'nullable|numeric',
            CuentasMorosasModel::MONTO_MORATORIO => 'nullable|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CuentasMorosasModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            CuentasMorosasModel::ABONO_ID => 'required|exists:abonos_por_cobrar,id',
            CuentasMorosasModel::DIAS_MORATORIOS => 'nullable|numeric',
            CuentasMorosasModel::MONTO_MORATORIO => 'nullable|numeric'
        ];
    }

    public function crearActualizarCuentasMorosas($cuenta_por_cobrar_id, array $data)
    {
        return $this->createOrUpdate(
            [
                CuentasMorosasModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar_id,
                CuentasMorosasModel::ABONO_ID => $data[CuentasMorosasModel::ABONO_ID],
                CuentasMorosasModel::DIAS_MORATORIOS => $data[CuentasMorosasModel::DIAS_MORATORIOS],
                CuentasMorosasModel::MONTO_MORATORIO => $data[CuentasMorosasModel::MONTO_MORATORIO]
            ],
            $data
        );
    }

    public function getAbonosMorosos($cuenta_por_cobrar_id)
    {
        $data_abonos = [];
        $abonos = $this->modelo
            ->leftJoin(CuentasPorCobrarModel::getTableName(), CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID, '=', CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::CUENTA_POR_COBRAR_ID)
            ->leftJoin(AbonosModel::getTableName(), AbonosModel::getTableName() . '.' . AbonosModel::ID, '=', CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::ABONO_ID)
            ->leftJoin(PlazoCreditoModel::getTableName(), PlazoCreditoModel::getTableName() . '.' . PlazoCreditoModel::ID, '=', CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::PLAZO_CREDITO_ID)
            ->select(
                CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::DIAS_MORATORIOS,
                CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::MONTO_MORATORIO,
                AbonosModel::getTableName() . '.' . AbonosModel::FECHA_VENCIMIENTO,
                AbonosModel::getTableName() . '.' . AbonosModel::TOTAL_ABONO,
                AbonosModel::getTableName() . '.' . AbonosModel::TOTAL_PAGO,
                AbonosModel::getTableName() . '.' . AbonosModel::FECHA_PAGO,
            )->where(CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID, $cuenta_por_cobrar_id)->get();
        if ($abonos) {
            foreach ($abonos as $abono) {
                $suma = $abono->total_abono + $abono->monto_moratorio;
                $abono->total_moratorio = $suma;
                array_push($data_abonos, $abono);
            }
        }
        return $data_abonos;
    }

    public function getBusqueda($request)
    {
        $fecha_inicio = $request->get('fecha_inicio');
        $fecha_fin = $request->get('fecha_fin');
        $cuenta_id = $request->get('cuenta_por_cobrar_id');

        $query = $this->modelo
            ->leftJoin(CuentasPorCobrarModel::getTableName(), CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID, '=', CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::CUENTA_POR_COBRAR_ID)
            ->leftJoin(ClientesModel::getTableName(), ClientesModel::getTableName() . '.' . ClientesModel::ID, '=', CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::CLIENTE_ID)
            ->leftJoin(PlazoCreditoModel::getTableName(), PlazoCreditoModel::getTableName() . '.' . PlazoCreditoModel::ID, '=', CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::PLAZO_CREDITO_ID)
            ->select(
                CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID,
                CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::CONCEPTO,
                CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::CLIENTE_ID,
                CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::TOTAL,
                CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::FECHA,
                CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::CUENTA_POR_COBRAR_ID,
                ClientesModel::getTableName() . '.' . ClientesModel::NUMERO_CLIENTE,
                ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE_EMPRESA,
                ClientesModel::getTableName() . '.' . ClientesModel::TELEFONO,
                ClientesModel::getTableName() . '.' . ClientesModel::CORREO_ELECTRONICO,
                ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO . ' as ap1_cliente',
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO . ' as ap2_cliente',
                PlazoCreditoModel::getTableName() . '.' . PlazoCreditoModel::NOMBRE . ' as plazo_credito',

            );
        if ($fecha_inicio && $fecha_fin) {
            $query->whereBetween(AbonosModel::getTableName() . '.' . AbonosModel::FECHA_VENCIMIENTO, [$fecha_inicio . ' 00:00:00', $fecha_fin . ' 23:59:59']);
        }
        if ($cuenta_id) {
            $query->where(CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID, $cuenta_id);
        }
        $query->groupBy(
            CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::ID,
            CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::CONCEPTO,
            CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::CLIENTE_ID,
            CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::TOTAL,
            CuentasPorCobrarModel::getTableName() . '.' . CuentasPorCobrarModel::FECHA,
            CuentasMorosasModel::getTableName() . '.' . CuentasMorosasModel::CUENTA_POR_COBRAR_ID,
            ClientesModel::getTableName() . '.' . ClientesModel::NUMERO_CLIENTE,
            ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE_EMPRESA,
            ClientesModel::getTableName() . '.' . ClientesModel::TELEFONO,
            ClientesModel::getTableName() . '.' . ClientesModel::CORREO_ELECTRONICO,
            ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE,
            ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO,
            ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO,
            PlazoCreditoModel::getTableName() . '.' . PlazoCreditoModel::NOMBRE

        );

        $resultado = $query->get();
        $data = [];
        if ($resultado) {
            foreach ($resultado as $value) {
                $value->abonos = $this->getAbonosMorosos($value->cuenta_por_cobrar_id);

                $value->total_adeudo_moratorio = 0;
                $value->total_abono130dias = 0;
                $value->total_abono3160dias = 0;
                $value->total_abono6190dias = 0;
                $value->total_abono90masdias = 0;

                if ($value->abonos) {
                    foreach ($value->abonos as $abono) {
                        if ($abono->dias_moratorios >= 1 && $abono->dias_moratorios <= 30) {
                            $value->total_abono130dias +=  $abono->total_moratorio;
                        }
                        if ($abono->dias_moratorios >= 31 && $abono->dias_moratorios <= 60) {
                            $value->total_abono3160dias +=  $abono->total_moratorio;
                        }
                        if ($abono->dias_moratorios >= 61 && $abono->dias_moratorios <= 90) {
                            $value->total_abono6190dias +=  $abono->total_moratorio;
                        }
                        if ($abono->dias_moratorios >= 90) {
                            $value->total_abono90masdias +=  $abono->total_moratorio;
                        }

                        $value->total_adeudo_moratorio +=  $abono->total_moratorio;;
                    }
                }
                array_push($data, $value);
            }
        }

        return $data;
    }
}
