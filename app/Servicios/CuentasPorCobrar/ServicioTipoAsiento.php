<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\TipoAsientoModel;

class ServicioTipoAsiento extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo asientos';
        $this->modelo = new TipoAsientoModel();
    }

    public function getReglasGuardar()
    {
        return [
            TipoAsientoModel::DESCRIPCION => 'required|string|max:50|unique:' . $this->modelo->getTable() . ',descripcion',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            TipoAsientoModel::DESCRIPCION => 'required|string|max:50|unique:' . $this->modelo->getTable() . ',descripcion',
        ];
    }
}
