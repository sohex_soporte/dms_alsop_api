<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\DocumentosSolicitudModel;

class ServicioDocumentoSolicitud extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'documento solicitud';
        $this->modelo = new DocumentosSolicitudModel();
    }

    public function getReglasGuardar()
    {
        return [
            DocumentosSolicitudModel::ID_SOLICITUD => 'required|string|exists:solicitud_credito,id',
            DocumentosSolicitudModel::RUTA_ARCHIVO => 'required|string',
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DocumentosSolicitudModel::ID_SOLICITUD => 'required|string|exists:solicitud_credito,id',
            DocumentosSolicitudModel::RUTA_ARCHIVO => 'required|string',
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => 'required|numeric'
        ];
    }
}
