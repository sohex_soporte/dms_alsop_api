<?php

namespace App\Servicios\Autos;

use App\Models\Autos\CatModelosModel;
use App\Servicios\Core\ServicioDB;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\ComentariosModel;
use App\Models\Autos\SalidaUnidades\ConfortModel;
use App\Models\Autos\SalidaUnidades\DesempenoModel;
use App\Models\Autos\SalidaUnidades\DocumentosModel;
use App\Models\Autos\SalidaUnidades\EspecialesModel;
use App\Models\Autos\SalidaUnidades\ExtrasModel;
use App\Models\Autos\SalidaUnidades\FuncionamientoVehiculoModel;
use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SeguridadModel;
use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use App\Models\Autos\SalidaUnidades\VehiculoHibridoModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\FoliosModel;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\Refacciones\ServicioClientes;
use Illuminate\Support\Facades\DB;

class ServicioVentaAutos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'venta auto';
        $this->modelo = new VentasAutosModel();
        $this->modeloRemision = new RemisionModel();
        $this->modeloDetalleRemisionModel = new DetalleRemisionModel();
        $this->modeloLineas = new CatLineas();
        $this->modeloCatCombustible = new CatCombustible();
        $this->modeloCuentasPorCobrar = new CuentasPorCobrarModel();
        $this->modeloFolios = new FoliosModel();
        $this->modeloDetalleCostosRemision = new DetalleCostosRemisionModel();

        $this->servicioClientes = new ServicioClientes();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioCuentasxCobrar = new ServicioCuentaPorCobrar();
        $this->servicio_abonos = new ServicioAbono();
    }

    public function getReglasGuardar()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'nullable|numeric',
            VentasAutosModel::ID_CLIENTE => 'required|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric',
            VentasAutosModel::ID_ESTATUS => 'required|numeric',
            VentasAutosModel::ID_TIPO_AUTO => 'required|numeric',
            VentasAutosModel::ID_ASESOR => 'required|numeric',
            VentasAutosModel::DESCRIPCION_MODELO => 'nullable',
            CuentasPorCobrarModel::TOTAL => 'required',
            VentasAutosModel::SOLICITA_FACTURACION => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            VentasAutosModel::MONEDA => 'required',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            VentasAutosModel::IMPUESTO_ISAN => 'required|numeric',
            VentasAutosModel::DESCUENTO => 'nullable|numeric',
            VentasAutosModel::IVA => 'required|numeric',
            VentasAutosModel::DESCUENTO => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'nullable|numeric',
            VentasAutosModel::ID_CLIENTE => 'required|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric',
            VentasAutosModel::ID_ESTATUS => 'required|numeric',
            VentasAutosModel::ID_TIPO_AUTO => 'required|numeric',
            VentasAutosModel::ID_ASESOR => 'required|numeric',
            VentasAutosModel::DESCRIPCION_MODELO => 'nullable',
            CuentasPorCobrarModel::TOTAL => 'required',
            VentasAutosModel::SOLICITA_FACTURACION => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            VentasAutosModel::MONEDA => 'required',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            VentasAutosModel::IMPUESTO_ISAN => 'required|numeric',
            VentasAutosModel::DESCUENTO => 'nullable|numeric',
            VentasAutosModel::IVA => 'required|numeric',
            VentasAutosModel::DESCUENTO => 'required|numeric',
            VentasAutosModel::CREDITO_APROVADO => 'nullable|numeric'
        ];
    }

    public function getReglasFiltroPrepedido()
    {
        return [
            VentasAutosModel::FECHA_INICIO => 'date',
            VentasAutosModel::FECHA_FIN => 'date',
            VentasAutosModel::SOLICITA_FACTURACION => 'nullable',
        ];
    }

    public function store($parametros)
    {
        $folio = $this->servicioFolios->generarFolio(CatalogoProcesosModel::PROCESO_VENTA_AUTOS_NUEVOS);
        $parametros[VentasAutosModel::ID_FOLIO] = $folio->id;
        $importe = $parametros[CuentasPorCobrarModel::TOTAL];
        if ($parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID] == TipoFormaPagoModel::FORMA_CREDITO) {

            $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            $intereses = ($tasa_interes * ($importe / 100));
            $total_pagar = $importe + $intereses;
            $parametros[CuentasPorCobrarModel::TOTAL] = $total_pagar;
            $parametros[CuentasPorCobrarModel::IMPORTE] = $importe;
            $parametros[CuentasPorCobrarModel::INTERESES] = $intereses;
            $parametros[VentasAutosModel::TIPO_VENTA] = TipoFormaPagoModel::FORMA_CREDITO;
        } else {
            $parametros[CuentasPorCobrarModel::IMPORTE] = $importe;
            $parametros[CuentasPorCobrarModel::TOTAL] = $importe;
            $parametros[CuentasPorCobrarModel::INTERESES] = 0;
            $parametros[VentasAutosModel::TIPO_VENTA] = TipoFormaPagoModel::FORMA_CONTADO;
        }

        $this->store_cxc($parametros);
        $venta = $this->modelo->create($parametros);
        $venta['folio'] = $folio->folio;

        return $venta;
    }

    public function store_cxc($parametros)
    {

        $cuentaxcobrar =  $this->servicioCuentasxCobrar->crear([
            CuentasPorCobrarModel::FOLIO_ID => $parametros[VentasAutosModel::ID_FOLIO],
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[VentasAutosModel::ID_CLIENTE],
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_PROCESO,
            CuentasPorCobrarModel::CONCEPTO => "VENTA_AUTO_NUEVO",
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID],
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID],
            CuentasPorCobrarModel::TOTAL => $parametros[CuentasPorCobrarModel::TOTAL],
            CuentasPorCobrarModel::ENGANCHE => $parametros[CuentasPorCobrarModel::ENGANCHE],
            CuentasPorCobrarModel::TASA_INTERES => $parametros[CuentasPorCobrarModel::TASA_INTERES],
            CuentasPorCobrarModel::INTERESES => $parametros[CuentasPorCobrarModel::INTERESES],
            CuentasPorCobrarModel::IMPORTE => $parametros[CuentasPorCobrarModel::IMPORTE],
            CuentasPorCobrarModel::COMENTARIOS => "",
            CuentasPorCobrarModel::TRANSFERENCIA_AUTORIZADA => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID] == CatTipoPagoModel::TIPO_TRANSFERENCIA ? CuentasPorCobrarModel::ESTATUS_TRANSFERENCIA_ESPERA_AUTORIZACION : CuentasPorCobrarModel::ESTATUS_TRANSFERENCIA_NO_APLICA,
            CuentasPorCobrarModel::FECHA => date('Y-m-d')

        ]);

        $this->servicio_abonos->crearAbonosByCuentaId($cuentaxcobrar->id);
        return $cuentaxcobrar;
    }

    public function getPrepedidos($parametros)
    {
        $data = [];
        $data['id_estatus_venta'] = [EstatusVentaAutosModel::ESTATUS_UNIDAD_ENTREGADA]; // diferente de
        $data['id_estado'] = UnidadesModel::TIPO_NUEVO;
        // dd($parametros, array_merge($data, $parametros));
        return $this->getVentaAutos(array_merge($data, $parametros));
    }

    public function getPrepedidosSeminuevos($parametros)
    {
        $data = [];
        $data['id_estatus_venta'] = [EstatusVentaAutosModel::ESTATUS_UNIDAD_ENTREGADA];
        $data['id_estado'] = UnidadesModel::TIPO_SEMI_NUEVO;
        return $this->getVentaAutos(array_merge($data, $parametros));
    }

    public function getVentaAutos($parametros)
    {
        $tabla_venta = $this->modelo->getTable();
        $tabla_unidades = $this->modeloRemision->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_lineas = $this->modeloLineas->getTable();
        $tabla_cat_combustible = $this->modeloCatCombustible->getTable();
        $tabla_detalle_costo_remision = $this->modeloDetalleCostosRemision->getTable();

        $query = $this->modelo->select(
            $tabla_venta . '.' . VentasAutosModel::ID,
            $tabla_venta . '.' . VentasAutosModel::MONEDA,
            $tabla_venta . '.' . VentasAutosModel::SERVICIO_CITA_ID,
            $tabla_venta . '.' . VentasAutosModel::DESCUENTO,
            $tabla_venta . '.' . VentasAutosModel::IMPUESTO_ISAN,
            $tabla_venta . '.' . VentasAutosModel::IVA,
            $tabla_venta . '.' . VentasAutosModel::DESCRIPCION_MODELO,
            $tabla_venta . '.' . VentasAutosModel::FIRMA_CLIENTE,
            $tabla_venta . '.' . VentasAutosModel::FIRMA_CONTADORA,
            $tabla_venta . '.' . VentasAutosModel::FIRMA_CREDITO,
            $tabla_venta . '.' . VentasAutosModel::FIRMA_ASESOR,
            $tabla_venta . '.' . VentasAutosModel::TIPO_VENTA,
            $tabla_venta . '.' . VentasAutosModel::CREDITO_APROVADO,
            $tabla_venta . '.' . VentasAutosModel::ID_ASESOR,
            $tabla_venta . '.' . VentasAutosModel::SOLICITA_FACTURACION,
            $tabla_venta . '.' . VentasAutosModel::ID_ESTATUS . ' as id_estatus_venta',

            $tabla_lineas . '.' . CatLineas::MODELO,
            $tabla_lineas . '.' . CatLineas::DESCRIPCION . ' as modelo_descripcion',
            $tabla_lineas . '.' . CatLineas::LINEA,
            $tabla_unidades . '.' . RemisionModel::ID . ' as id_unidad',
            $tabla_unidades . '.' . RemisionModel::UNIDAD_DESCRIPCION,
            $tabla_unidades . '.' . RemisionModel::ECONOMICO,
            $tabla_unidades . '.' . RemisionModel::UNIDAD_IMPORTADA,
            $tabla_unidades . '.' . RemisionModel::TIPO_AUTO,
            $tabla_unidades . '.' . RemisionModel::CLAVE_ISAN,
            $tabla_unidades . '.' . RemisionModel::CTA_MENUDO,
            $tabla_unidades . '.' . RemisionModel::CTA_FLOTILLA,
            $tabla_unidades . '.' . RemisionModel::CTA_CONAUTO,
            $tabla_unidades . '.' . RemisionModel::CTA_INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::CTA_PLLENO,
            $tabla_unidades . '.' . RemisionModel::CTA_INVENTARIO,
            $tabla_unidades . '.' . RemisionModel::VTA_MENUDEO,
            $tabla_unidades . '.' . RemisionModel::VTA_FLOTILLA,
            $tabla_unidades . '.' . RemisionModel::VTA_CONAUTO,
            $tabla_unidades . '.' . RemisionModel::VTA_INTERCAMBIO,
            $tabla_unidades . '.' . RemisionModel::VTA_PLLENO,
            $tabla_unidades . '.' . RemisionModel::USERID,
            $tabla_unidades . '.' . RemisionModel::C_SUBTOTAL,
            $tabla_unidades . '.' . RemisionModel::C_IVA,
            $tabla_unidades . '.' . RemisionModel::MOTOR,
            $tabla_unidades . '.' . RemisionModel::ESTATUS_ID . ' as id_estatus_unidad',
            $tabla_detalleRemision . '.' . DetalleRemisionModel::TRANSMISION,

            $tabla_detalleRemision . '.' . DetalleRemisionModel::PUERTAS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CILINDROS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::TRANSMISION,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD . ' as precio_costo',
            $tabla_unidades . '.' . RemisionModel::C_TOTAL . ' as precio_venta',

            $tabla_cat_combustible . '.' . CatCombustible::DESCRIPCION . ' as combustible',


            'clientes.id as cliente_id',
            'clientes.nombre_empresa as nombre_empresa',
            'clientes.nombre as nombre_cliente',
            'clientes.apellido_paterno as cliente_apellido_paterno',
            'clientes.apellido_materno as cliente_apellido_materno',
            'clientes.rfc as cliente_rfc',
            'clientes.telefono',
            'clientes.correo_electronico',
            'cuentas_por_cobrar.id as cxc_id',
            'cuentas_por_cobrar.concepto',
            'cuentas_por_cobrar.tipo_forma_pago_id',
            'cuentas_por_cobrar.tipo_pago_id',
            'cuentas_por_cobrar.plazo_credito_id',
            'cuentas_por_cobrar.enganche',
            'cuentas_por_cobrar.total',
            'cuentas_por_cobrar.tasa_interes',
            'usuarios.id as id_asesor',
            'usuarios.nombre as nombre_vendedor',
            'usuarios.apellido_paterno as apellido_paterno_vendedor',
            'usuarios.apellido_materno as apellido_materno_vendedor',
            'estatus_venta_autos.nombre as estatus_venta',
            'estatus_venta_autos.id as id_estatus_venta',
            'ventas_autos.created_at'

        )->from($tabla_venta);
        $query->leftJoin(
            $tabla_unidades,
            $tabla_unidades . '.' . RemisionModel::ID,
            '=',
            $tabla_venta . '.' . VentasAutosModel::ID_UNIDAD
        );
        $query->leftJoin(
            $tabla_detalleRemision,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::ID,
            '=',
            $tabla_unidades . '.' . RemisionModel::ID
        );
        $query->leftJoin(
            $tabla_detalle_costo_remision,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::REMISIONID,
            '=',
            $tabla_unidades . '.' . RemisionModel::ID
        );
        $query->leftJoin(
            $tabla_lineas,
            $tabla_lineas . '.id',
            '=',
            $tabla_unidades . '.' . RemisionModel::LINEA_ID
        );
        $query->leftJoin(
            $tabla_cat_combustible,
            $tabla_cat_combustible . '.' . CatCombustible::ID,
            '=',
            $tabla_detalleRemision . '.' . DetalleRemisionModel::COMBUSTIBLE_ID
        );
        $query->join('clientes', 'clientes.id', '=', 'ventas_autos.id_cliente');
        $query->join('cuentas_por_cobrar', 'cuentas_por_cobrar.folio_id', '=', 'ventas_autos.id_folio');
        $query->join('usuarios', 'usuarios.id', '=', $tabla_venta . '.' . VentasAutosModel::ID_ASESOR);
        $query->join('estatus_venta_autos', 'estatus_venta_autos.id', '=', 'ventas_autos.id_estatus');
        $query->leftJoin('folios', 'folios.id', '=', 'ventas_autos.id_folio');

        if (isset($parametros['venta_auto']) && $parametros['venta_auto']) {
            $query->join('equipo_opcional_unidad', 'equipo_opcional_unidad.id_venta_auto', '=', 'ventas_autos.id');
        }

        if (isset($parametros['servicio_cita_id']) && $parametros['servicio_cita_id']) {
            $query->whereNull('ventas_autos.servicio_cita_id');
        }

        if (isset($parametros['id_venta'])) {
            $query->where('ventas_autos.id', $parametros['id_venta']);
        }

        if (isset($parametros['id_estado'])) {
            // $query->where('unidades.id_estado', $parametros['id_estado']);
        }

        if (isset($parametros['solicita_facturacion'])) {
            $query->where($tabla_venta . '.' . VentasAutosModel::SOLICITA_FACTURACION, $parametros['solicita_facturacion']);
        }


        if (isset($parametros['id_estatus_venta'])) {
            // dd($parametros['id_estatus_venta'], "entra"); //verificar si llega un array
            // $query->where('ventas_autos.id_estatus', $parametros['id_estatus_venta']);
            $query->whereNotIn('estatus_venta_autos.id', $parametros['id_estatus_venta']);
        }

        if (isset($parametros['fecha_inicio']) && !isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '>=', $parametros['fecha_inicio']);
        }

        if (!isset($parametros['fecha_inicio']) && isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '<=', $parametros['fecha_fin']);
        }

        if (isset($parametros['fecha_inicio']) && isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '>=', $parametros['fecha_inicio']);
            $query->where('ventas_autos.created_at', '<=', $parametros['fecha_fin']);
        }

        if (isset($parametros['id_folio'])) {
            $query->where('ventas_autos.id_folio', $parametros['id_folio']);
        }

        if (isset($parametros['venta_auto']) && $parametros['venta_auto']) {
            $query->groupBy(
                'ventas_autos.id',
                'ventas_autos.moneda',
                $tabla_venta . '.' . VentasAutosModel::ID_ESTATUS,
                $tabla_unidades . '.' . RemisionModel::ID,
                $tabla_unidades . '.' . RemisionModel::UNIDAD_DESCRIPCION,
                $tabla_unidades . '.' . RemisionModel::ECONOMICO,
                $tabla_unidades . '.' . RemisionModel::UNIDAD_IMPORTADA,
                $tabla_unidades . '.' . RemisionModel::TIPO_AUTO,
                $tabla_unidades . '.' . RemisionModel::CLAVE_ISAN,
                $tabla_unidades . '.' . RemisionModel::CTA_MENUDO,
                $tabla_unidades . '.' . RemisionModel::CTA_FLOTILLA,
                $tabla_unidades . '.' . RemisionModel::CTA_CONAUTO,
                $tabla_unidades . '.' . RemisionModel::CTA_INTERCAMBIO,
                $tabla_unidades . '.' . RemisionModel::CTA_PLLENO,
                $tabla_unidades . '.' . RemisionModel::CTA_INVENTARIO,
                $tabla_unidades . '.' . RemisionModel::VTA_MENUDEO,
                $tabla_unidades . '.' . RemisionModel::VTA_FLOTILLA,
                $tabla_unidades . '.' . RemisionModel::VTA_CONAUTO,
                $tabla_unidades . '.' . RemisionModel::VTA_INTERCAMBIO,
                $tabla_unidades . '.' . RemisionModel::VTA_PLLENO,
                $tabla_unidades . '.' . RemisionModel::USERID,
                $tabla_unidades . '.' . RemisionModel::C_SUBTOTAL,
                $tabla_unidades . '.' . RemisionModel::C_IVA,
                'catalogo_marcas.nombre',
                'catalogo_modelos.nombre',
                'catalogo_colores.nombre',
                'catalogo_anio.nombre',
                'clientes.nombre_empresa',
                'clientes.nombre',
                'clientes.apellido_paterno',
                'clientes.apellido_materno',

                'clientes.rfc',
                'clientes.telefono',
                'clientes.correo_electronico',
                'cuentas_por_cobrar.concepto',
                'cuentas_por_cobrar.tipo_forma_pago_id',
                'cuentas_por_cobrar.tipo_pago_id',
                'cuentas_por_cobrar.plazo_credito_id',
                'cuentas_por_cobrar.enganche',
                'cuentas_por_cobrar.total',
                'cuentas_por_cobrar.tasa_interes',
                'usuarios.nombre',
                'usuarios.apellido_paterno',
                'usuarios.apellido_materno',
                'estatus_venta_autos.nombre',

                'ventas_autos.servicio_cita_id',
                'ventas_autos.created_at',

                'clientes.id',
                'cuentas_por_cobrar.id',
                'usuarios.id',
                'estatus_venta_autos.nombre'
            );
        }

        $query->limit(200);
        return $query->get();
    }

    public function getOne($id)
    {
        return $this->getVentaAutos(['id_venta' => $id]);
    }

    public function getPreventaById($id)
    {
        return $this->getVentaAutos([
            'id_venta' => $id,
            'id_estado' => UnidadesModel::TIPO_NUEVO,
            'id_estatus_venta' => EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO
        ]);
    }

    public function getPreventaSeminuevoById($id)
    {
        return $this->getVentaAutos([
            'id_venta' => $id,
            'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO,
            'id_estatus_venta' => EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO
        ]);
    }

    public function getReglasformatosalida()
    {
        return [
            VentasAutosModel::ID_VENTA_AUTO => 'required|exists:ventas_autos,id',

            VentasAutosModel::FIRMA_ASESOR => 'nullable',
            VentasAutosModel::FIRMA_CLIENTE => 'nullable',
            VentasAutosModel::FIRMA_CONTADORA => 'nullable',
            VentasAutosModel::FIRMA_CREDITO => 'nullable',

            FuncionamientoVehiculoModel::ASISTENTE_ARRANQUE => 'required',
            FuncionamientoVehiculoModel::PRESERVACION_CARRIL => 'required',
            FuncionamientoVehiculoModel::SENSORES_DEL_LAT => 'required',
            FuncionamientoVehiculoModel::BLIS => 'required',
            FuncionamientoVehiculoModel::ALERTA_TRAFICO_CRUZADO => 'required',
            FuncionamientoVehiculoModel::ASISTENCIA_ACTIVA_ESTACI => 'required',
            FuncionamientoVehiculoModel::CAMARA_REVERSA => 'required',
            FuncionamientoVehiculoModel::CAMARA_FRONTAL => 'required',
            FuncionamientoVehiculoModel::CAMARA_360_GRADOS => 'required',
            FuncionamientoVehiculoModel::SITEMA_MON_PLL_TMS => 'required',
            AhorroCombustibleModel::IVCT => 'required',
            AhorroCombustibleModel::TIVCT => 'required',

            ComentariosModel::EXPLICO_ASISTENCIA_FORD => 'required',
            ComentariosModel::EXPLICO_FORD_PROTECT => 'required',
            ComentariosModel::ACCESORIOS_PERSONALIZAR => 'required',
            ComentariosModel::PRUEBA_MANEJO => 'required',
            ComentariosModel::INFORMACION_MANTENIMIENTO => 'required',
            ComentariosModel::ASESOR_SERVICIO => 'required',

            ConfortModel::ACCIONAMIENTO_MANUAL => 'required',
            ConfortModel::ACCIONAMIENTO_ELECTRICO => 'required',
            ConfortModel::ASIENTO_DELANTERO_MASAJE => 'required',
            ConfortModel::ASIENTO_CALEFACCION => 'required',
            ConfortModel::EASY_POWER_FOLD => 'required',
            ConfortModel::MEMORIA_ASIENTOS => 'required',

            DesempenoModel::MOTORES => 'required',
            DesempenoModel::CLUTCH => 'required',
            DesempenoModel::CAJA_CAMBIOS => 'required',
            DesempenoModel::ARBOL_TRANSMISIONES => 'required',
            DesempenoModel::DIFERENCIAL => 'required',
            DesempenoModel::DIFERENCIAL_TRACCION => 'required',
            DesempenoModel::CAJA_REDUCTORA => 'required',
            DesempenoModel::CAJA_TRANSFERENCIA => 'required',
            DesempenoModel::SISTEMA_DINAMICO => 'required',
            DesempenoModel::DIRECCION => 'required',
            DesempenoModel::SUSPENSION => 'required',
            DesempenoModel::TOMA_PODER => 'required',

            DocumentosModel::CARTA_FACTURA => 'required',
            DocumentosModel::POLIZA_GARANTIA => 'required',
            DocumentosModel::POLIZA_SEGURO => 'required',
            DocumentosModel::TENENCIA => 'required',
            DocumentosModel::HERRAMIENTAS => 'required',

            EspecialesModel::PEPS => 'required',
            EspecialesModel::APERTIRA_CAJUELA => 'required',
            EspecialesModel::ENCENDIDO_REMOTO => 'required',
            EspecialesModel::TECHO_PANORAMICO => 'required',
            EspecialesModel::AIRE_ACONDICIONADO => 'required',
            EspecialesModel::EATC => 'required',
            EspecialesModel::CONSOLA_ENFRIAMIENTO => 'required',
            EspecialesModel::VOLANTE_AJUSTE_ALTURA => 'required',
            EspecialesModel::VOLANTE_CALEFACTADO => 'required',
            EspecialesModel::ESTRIBOS_ELECTRICOS => 'required',
            EspecialesModel::APERTURA_CAJA_CARGA => 'required',
            EspecialesModel::LIMPIAPARABRISAS => 'required',
            EspecialesModel::ESPEJO_ELECTRICO => 'required',
            EspecialesModel::ESPEJOS_ABATIBLES => 'required',

            ExtrasModel::CONECTIVIDAD => 'required',
            ExtrasModel::SYNC => 'required',
            ExtrasModel::MYFORD_TOUCH => 'required',
            ExtrasModel::MYFORD_TOUCH_NAVEGACION => 'required',
            ExtrasModel::SYNC_FORDPASS => 'required',
            ExtrasModel::APPLE_CARPLAY => 'required',
            ExtrasModel::ANDROID_AUTO => 'required',
            ExtrasModel::CONSOLA_SMARTPHONE => 'required',
            ExtrasModel::AUDIO_SHAKER_PRO => 'required',
            ExtrasModel::RADIO_HD => 'required',
            ExtrasModel::EQUIPO_SONY => 'required',
            ExtrasModel::PUERTOS_USB => 'required',
            ExtrasModel::BLUETOOH => 'required',
            ExtrasModel::WIFI => 'required',
            ExtrasModel::TARJETA_SD => 'required',
            ExtrasModel::INVERSOR_CORRIENTE => 'required',
            ExtrasModel::ESPEJO_ELECTROCROMATICO => 'required',

            IluminacionModel::ILUMINACION_AMBIENTAL => 'required',
            IluminacionModel::SISTEMA_ILUMINACION => 'required',
            IluminacionModel::FAROS_BIXENON => 'required',
            IluminacionModel::HID => 'required',
            IluminacionModel::LED => 'required',

            OtrasTecnologiasModel::EASY_FUEL => 'required',
            OtrasTecnologiasModel::TRACK_APPS => 'required',
            OtrasTecnologiasModel::AUTO_START_STOP => 'required',
            OtrasTecnologiasModel::SISTEMA_CTR_TERRENO => 'required',

            SeguridadModel::SISTEMA_FRENADO => 'required',
            SeguridadModel::CONTROL_TRACCION => 'required',
            SeguridadModel::ESC => 'required',
            SeguridadModel::RSC => 'required',
            SeguridadModel::CONTROL_TORQUE_CURVAS => 'required',
            SeguridadModel::CONTROL_CURVAS => 'required',
            SeguridadModel::CONTROL_BALANCE_REMOLQUE => 'required',
            SeguridadModel::FRENOS_ELECTRONICOS => 'required',
            SeguridadModel::CONTROL_ELECTRONICO_DES => 'required',

            SeguridadPasivaModel::CINTURON_SEGURIDAD => 'required',
            SeguridadPasivaModel::BOLSAS_AIRE => 'required',
            SeguridadPasivaModel::CHASIS_CARROCERIA => 'required',
            SeguridadPasivaModel::CARROSERIA_HIDROFORMA => 'required',
            SeguridadPasivaModel::CRISTALES => 'required',
            SeguridadPasivaModel::CABECERAS => 'required',
            SeguridadPasivaModel::SISTEMA_ALERTA => 'required',
            SeguridadPasivaModel::TECLADO_PUERTA => 'required',
            SeguridadPasivaModel::ALARMA_VOLUMETRICA => 'required',
            SeguridadPasivaModel::ALARMA_PERIMETRAL => 'required',
            SeguridadPasivaModel::MYKEY => 'required',

            VehiculoHibridoModel::VEHICULO_HIBRIDO => 'required',
            VehiculoHibridoModel::MOTOR_GASOLINA => 'required',
            VehiculoHibridoModel::TIPS_MANEJO => 'required',
            VehiculoHibridoModel::TRANSMISION_ECVT => 'required'
        ];
    }

    public function guardarFormatosalida($parametros)
    {
        $this->modeloAhorroCombustible = new AhorroCombustibleModel();
        $this->modeloComentarios = new ComentariosModel();
        $this->modeloConfort = new ConfortModel();
        $this->modeloDesempeno = new DesempenoModel();
        $this->modeloDocumentos = new DocumentosModel();
        $this->modeloEspeciales = new EspecialesModel();
        $this->modeloExtras = new ExtrasModel();
        $this->modeloFuncionamientoVehiculo = new FuncionamientoVehiculoModel();
        $this->modeloIluminacion = new IluminacionModel();
        $this->modeloOtrasTecnologias = new OtrasTecnologiasModel();
        $this->modeloSeguridad = new SeguridadModel();
        $this->modeloSeguridadPasiva = new SeguridadPasivaModel();
        $this->modeloVehiculoHibrido = new VehiculoHibridoModel();

        $this->modelo->updateOrCreate([VentasAutosModel::ID => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);

        $this->modeloAhorroCombustible->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloComentarios->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloConfort->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloDesempeno->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloDocumentos->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloEspeciales->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloExtras->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);

        $this->modeloFuncionamientoVehiculo->updateOrCreate([FuncionamientoVehiculoModel::ID_VENTA_AUTO => $parametros[FuncionamientoVehiculoModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloIluminacion->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloOtrasTecnologias->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloSeguridad->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloSeguridadPasiva->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
        $this->modeloVehiculoHibrido->updateOrCreate([VentasAutosModel::ID_VENTA_AUTO => $parametros[VentasAutosModel::ID_VENTA_AUTO]], $parametros);
    }



    public function getdataformatosalida($id_preventa)
    {
        return  $this->modelo->with([
            'seguridad',
            'ahorro_combustible',
            'comentarios',
            'confort',
            'desempeno',
            'documentos',
            'especiales',
            'extras',
            'funcionamiento_vehiculo',
            'iluminacion',
            'otras_tecnologias',
            'seguridad_pasiva',
            'vehiculo_hibrido',
            'cliente',
            'unidad',
            'asesor'
        ])->where(VentasAutosModel::ID, $id_preventa)
            ->get();
    }

    public function getClienteVentaAuto()
    {
        return $this->servicioClientes->getClientesAutosPreventa([]);
    }

    public function getClienteVentaAutoById($id_preventa)
    {
        return $this->servicioClientes->getClientesAutosPreventa([
            VentasAutosModel::ID => $id_preventa
        ]);
    }

    public function get_solicitud_transferencias($parametros)
    {

        $tabla_venta = $this->modelo->getTable();
        $tabla_cxc = $this->modeloCuentasPorCobrar->getTable();
        $tabla_folios = $this->modeloFolios->getTable();
        $tabla_unidades = $this->modeloRemision->getTable();
        $query = $this->modelo->select(
            $tabla_venta . '.' . VentasAutosModel::ID . ' as id_venta',
            $tabla_cxc . '.' . CuentasPorCobrarModel::ID . ' as id_cxc',
            $tabla_cxc . '.' . CuentasPorCobrarModel::TRANSFERENCIA_AUTORIZADA,
            $tabla_cxc . '.' . CuentasPorCobrarModel::CONCEPTO,
            $tabla_cxc . '.' . CuentasPorCobrarModel::TOTAL,
            $tabla_cxc . '.' . CuentasPorCobrarModel::IMPORTE,
            $tabla_cxc . '.' . CuentasPorCobrarModel::ENGANCHE,
            $tabla_folios . '.' . FoliosModel::FOLIO,
            $tabla_unidades . '.' . RemisionModel::ID . ' as id_unidad',
            $tabla_unidades . '.' . RemisionModel::UNIDAD_DESCRIPCION,
        )
            ->join(
                $tabla_unidades,
                $tabla_unidades . '.' . RemisionModel::ID,
                '=',
                $tabla_venta . '.' . VentasAutosModel::ID_UNIDAD
            )->join(
                $tabla_folios,
                $tabla_folios . '.' . FoliosModel::ID,
                '=',
                $tabla_venta . '.' . VentasAutosModel::ID_FOLIO
            )->join(
                $tabla_cxc,
                $tabla_cxc . '.' . CuentasPorCobrarModel::FOLIO_ID,
                '=',
                $tabla_venta . '.' . VentasAutosModel::ID_FOLIO
            );

        if (isset($parametros['id_cxc'])) {
            $query->where($tabla_cxc . '.' . CuentasPorCobrarModel::ID, $parametros['id_cxc']);
        }

        $query->where($tabla_cxc . '.' . CuentasPorCobrarModel::TRANSFERENCIA_AUTORIZADA, '!=', CuentasPorCobrarModel::ESTATUS_TRANSFERENCIA_NO_APLICA);

        return $query->get();
    }

    public function actualizarAutorizacion($id_cxc = '', Int $id_estatus_autorizado)
    {

        return tap(DB::table($this->modeloCuentasPorCobrar->getTable())
            ->where(CuentasPorCobrarModel::ID, $id_cxc))
            ->update([
                CuentasPorCobrarModel::TRANSFERENCIA_AUTORIZADA => $id_estatus_autorizado
            ])->first();
    }

    public function reglasVentaSinUnidad()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'required|exists:remisiones,id|unique:' . VentasAutosModel::getTableName() . ',id_unidad',
            VentasAutosModel::ID => 'required|exists:' . VentasAutosModel::getTableName() . ',id'
        ];
    }
}
