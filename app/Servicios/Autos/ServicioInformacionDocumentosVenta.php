<?php

namespace App\Servicios\Autos;

use App\Models\Autos\InformacionDocumentosVentaModel;
use App\Models\Autos\Inventario\DocumentacionModel;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\ClientesModel;
use App\Servicios\Core\ServicioDB;

class ServicioInformacionDocumentosVenta extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'documentacion';
        $this->modelo = new InformacionDocumentosVentaModel();
        $this->modeloClientes = new ClientesModel();
        $this->modeloVentasAutos = new VentasAutosModel();
        $this->modeloRemision = new RemisionModel();
        $this->modeloDetalleRemisionModel = new DetalleRemisionModel();
        $this->modeloLineas = new CatLineas();
        // $this->modeloCatCombustible = new CatCombustible();
        // $this->modeloCuentasPorCobrar = new CuentasPorCobrarModel();
        // $this->modeloFolios = new FoliosModel();
        $this->modeloDetalleCostosRemision = new DetalleCostosRemisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            InformacionDocumentosVentaModel::ID_VENTA_UNIDAD => 'required|exists:ventas_autos,id',
            InformacionDocumentosVentaModel::FECHA_FACTURA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ULTIMO_INGRESO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CONTRATO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ORDEN_COMPRA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_VENTA_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_INGRESO_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FIRMA_GERENTE => 'nullable',
            InformacionDocumentosVentaModel::FIRMA_OTROS => 'nullable'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            InformacionDocumentosVentaModel::ID_VENTA_UNIDAD => 'nullable|exists:ventas_autos,id',
            InformacionDocumentosVentaModel::FECHA_FACTURA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ULTIMO_INGRESO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CONTRATO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_ORDEN_COMPRA => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_VENTA_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FECHA_INGRESO_SMART => 'nullable|date',
            InformacionDocumentosVentaModel::FIRMA_GERENTE => 'nullable',
            InformacionDocumentosVentaModel::FIRMA_OTROS => 'nullable'
        ];
    }

    public function getByIdVentaUnidad($id_venta_unidad)
    {
        $tabla_documentacion = $this->modelo->getTable();
        $tabla_clientes = $this->modeloClientes->getTable();
        $tabla_venta = $this->modeloVentasAutos->getTable();
        $tabla_unidades = $this->modeloRemision->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_lineas = $this->modeloLineas->getTable();
        $tabla_detalle_costo_remision = $this->modeloDetalleCostosRemision->getTable();
        $tabla_cat_colores = CatalogoColoresModel::getTableName();
        $tabla_informacion_docs = InformacionDocumentosVentaModel::getTableName();


        $query = $this->modelo->select(
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::ID . ' as id_informacion_documentos',
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_FACTURA,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_ULTIMO_INGRESO,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_CONTRATO,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_CARTA_CONAUTO,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_ORDEN_COMPRA,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_VENTA_SMART,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FECHA_INGRESO_SMART,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FIRMA_GERENTE,
            $tabla_informacion_docs  . '.' . InformacionDocumentosVentaModel::FIRMA_OTROS,
            $tabla_venta . '.' . VentasAutosModel::ID,
            $tabla_venta . '.' . VentasAutosModel::IMPUESTO_ISAN,
            $tabla_venta . '.' . VentasAutosModel::IVA,
            $tabla_venta . '.' . VentasAutosModel::MONEDA,
            $tabla_venta . '.' . VentasAutosModel::SOLICITA_FACTURACION,
            $tabla_venta . '.' . VentasAutosModel::DESCUENTO,
            $tabla_venta . '.' . VentasAutosModel::TIPO_VENTA,
            $tabla_lineas . '.' . CatLineas::MODELO,
            $tabla_lineas . '.' . CatLineas::DESCRIPCION . ' as descripcion_linea',
            $tabla_lineas . '.' . CatLineas::LINEA,
            $tabla_lineas . '.' . CatLineas::MODELO . ' as anio_modelo',
            $tabla_unidades . '.' . RemisionModel::ID . ' as id_unidad',
            $tabla_unidades . '.' . RemisionModel::UNIDAD_DESCRIPCION,
            $tabla_unidades . '.' . RemisionModel::ECONOMICO,
            $tabla_unidades . '.' . RemisionModel::UNIDAD_IMPORTADA,
            $tabla_unidades . '.' . RemisionModel::TIPO_AUTO,
            $tabla_unidades . '.' . RemisionModel::CLAVE_ISAN,
            $tabla_unidades . '.' . RemisionModel::C_SUBTOTAL,
            $tabla_unidades . '.' . RemisionModel::C_IVA,
            $tabla_unidades . '.' . RemisionModel::MOTOR,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::PUERTAS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CILINDROS,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::TRANSMISION,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD,
            $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD . ' as precio_costo',
            $tabla_unidades . '.' . RemisionModel::C_TOTAL . ' as precio_venta',
            $tabla_clientes . '.' . ClientesModel::ID . ' as cliente_id',
            $tabla_clientes . '.' . ClientesModel::NOMBRE_EMPRESA . ' as nombre_empresa',
            $tabla_clientes . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
            $tabla_clientes . '.' . ClientesModel::APELLIDO_PATERNO . ' as cliente_apellido_paterno',
            $tabla_clientes . '.' . ClientesModel::APELLIDO_MATERNO . ' as cliente_apellido_materno',
            $tabla_clientes . '.' . ClientesModel::RFC . ' as cliente_rfc',
            $tabla_clientes . '.' . ClientesModel::TELEFONO,
            $tabla_clientes . '.' . ClientesModel::CORREO_ELECTRONICO,
            $tabla_clientes . '.' . ClientesModel::DIRECCION,
            $tabla_clientes . '.' . ClientesModel::COLONIA,
            $tabla_clientes . '.' . ClientesModel::CODIGO_POSTAL,
            $tabla_clientes . '.' . ClientesModel::ESTADO,

            'cuentas_por_cobrar.id as cxc_id',
            'cuentas_por_cobrar.concepto',
            'cuentas_por_cobrar.tipo_forma_pago_id',
            'cuentas_por_cobrar.tipo_pago_id',
            'cuentas_por_cobrar.plazo_credito_id',
            'cuentas_por_cobrar.enganche',
            'cuentas_por_cobrar.total',
            'cuentas_por_cobrar.tasa_interes',
            'color_interior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_interior',
            'color_exterior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_exterior',

            // )->from($tabla_documentacion);
            // $query->join($tabla_venta, $tabla_venta . '.' . VentasAutosModel::ID, '=', $tabla_documentacion . '.' . InformacionDocumentosVentaModel::ID_VENTA_UNIDAD);
        )->from($tabla_venta);
        $query->leftjoin($tabla_documentacion, $tabla_venta . '.' . VentasAutosModel::ID, '=', $tabla_documentacion . '.' . InformacionDocumentosVentaModel::ID_VENTA_UNIDAD);
        $query->leftjoin($tabla_unidades, $tabla_unidades . '.' . RemisionModel::ID, '=', $tabla_venta . '.' . VentasAutosModel::ID_UNIDAD);
        $query->leftjoin($tabla_detalleRemision, $tabla_detalleRemision . '.' . DetalleRemisionModel::ID, '=', $tabla_unidades . '.' . RemisionModel::ID);
        $query->leftjoin($tabla_detalle_costo_remision, $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID);
        $query->leftjoin($tabla_lineas, $tabla_lineas . '.' . CatLineas::ID, '=', $tabla_unidades . '.' . RemisionModel::LINEA_ID);
        $query->leftjoin($tabla_clientes, $tabla_clientes . '.' . ClientesModel::ID, '=', $tabla_venta . '.' . VentasAutosModel::ID_CLIENTE);
        $query->join('cuentas_por_cobrar', 'cuentas_por_cobrar.folio_id', '=', $tabla_venta . '.' . VentasAutosModel::ID_FOLIO)
            ->leftjoin(
                $tabla_cat_colores . ' as color_interior',
                'color_interior.' . CatalogoColoresModel::ID,
                '=',
                $tabla_detalleRemision . '.' . DetalleRemisionModel::COLORINTID
            )
            ->leftjoin(
                $tabla_cat_colores . ' as color_exterior',
                'color_exterior.' . CatalogoColoresModel::ID,
                '=',
                $tabla_detalleRemision . '.' . DetalleRemisionModel::COLORINTID
            );
        $query->where($tabla_venta . '.' . VentasAutosModel::ID, $id_venta_unidad);
        return  $query->first();
    }
}
