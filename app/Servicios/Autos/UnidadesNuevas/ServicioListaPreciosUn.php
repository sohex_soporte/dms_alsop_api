<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\ListPreciosUNModel;
use App\Models\Autos\UnidadesNuevas\PreciosUnidadesNuevasModel;
use App\Servicios\Core\ServicioDB;

class ServicioListaPreciosUn extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo precios';
        $this->modelo = new ListPreciosUNModel();
    }

    public function getReglasGuardar()
    {
        return [
            ListPreciosUNModel::CAT => 'required',
            ListPreciosUNModel::CLAVE_VEHICULAR => 'required',
            ListPreciosUNModel::DESCRIPCION => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ListPreciosUNModel::CAT => 'required',
            ListPreciosUNModel::CLAVE_VEHICULAR => 'required',
            ListPreciosUNModel::DESCRIPCION => 'required'
        ];
    }
    public function getPreciosParameters($parametros)
    {
        $query = $this->modelo
            ->select(
                ListPreciosUNModel::getTableName() . '.*',
                //UnidadesModel::getTableName() . '.' . UnidadesModel::UNIDAD_DESCRIPCION . ' as unidad',
            );
        if (isset($parametros[ListPreciosUNModel::CAT])) {
            $query->where(ListPreciosUNModel::getTableName() . '.' . ListPreciosUNModel::CAT, $parametros[ListPreciosUNModel::CAT]);
            $query->limit(1);
        }
        if (isset($parametros[ListPreciosUNModel::CLAVE_VEHICULAR])) {
            $query->where(ListPreciosUNModel::getTableName() . '.' . ListPreciosUNModel::CLAVE_VEHICULAR, $parametros[ListPreciosUNModel::CLAVE_VEHICULAR]);
            $query->limit(1);
        }

        return $query->get();
    }
}
