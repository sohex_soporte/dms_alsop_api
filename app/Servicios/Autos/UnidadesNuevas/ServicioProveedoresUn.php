<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatProveedoresUN;
use App\Servicios\Core\ServicioDB;

class ServicioProveedoresUn extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'proveedores unidades nuevas';
        $this->modelo = new CatProveedoresUN();
    }

    public function getReglasGuardar()
    {
        return [
            CatProveedoresUN::CLAVE => 'required',
            CatProveedoresUN::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatProveedoresUN::CLAVE => 'required',
            CatProveedoresUN::NOMBRE => 'required'
        ];
    }
}
