<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatTipoBicicletas;
use App\Servicios\Core\ServicioDB;

class ServicioCatTipoBicicletas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo tipo bicicletas';
        $this->modelo = new CatTipoBicicletas();
    }

    public function getReglasGuardar()
    {
        return [
            CatTipoBicicletas::TIPO_BICILETA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatTipoBicicletas::TIPO_BICILETA => 'required'
        ];
    }
}
