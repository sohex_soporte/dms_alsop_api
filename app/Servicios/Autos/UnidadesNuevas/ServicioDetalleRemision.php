<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Servicios\Core\ServicioDB;

class ServicioDetalleRemision extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'detalle remisión';
        $this->modelo = new DetalleRemisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            DetalleRemisionModel::REMISIONID => 'nullable',
            DetalleRemisionModel::COLOR_ID => 'required',
            DetalleRemisionModel::TYPE_ID => 'required',
            DetalleRemisionModel::MODELO_ID => 'required',
            DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO => 'required',
            DetalleRemisionModel::NO_IDENTIFICACION => 'required',
            DetalleRemisionModel::CANTIDAD => 'required',
            DetalleRemisionModel::CLAVE_UNIDAD => 'required',
            DetalleRemisionModel::UNIDAD => 'required',
            DetalleRemisionModel::DESCRIPCION => 'required',
            DetalleRemisionModel::VALOR_UNITARIO => 'required',
            DetalleRemisionModel::IMPORTE => 'required',
            DetalleRemisionModel::BRAND => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        //reglas
        return [
            DetalleRemisionModel::REMISIONID => 'nullable',
            DetalleRemisionModel::COLOR_ID => 'required',
            DetalleRemisionModel::TYPE_ID => 'required',
            DetalleRemisionModel::MODELO_ID => 'required',
            DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO => 'required',
            DetalleRemisionModel::NO_IDENTIFICACION => 'required',
            DetalleRemisionModel::CANTIDAD => 'required',
            DetalleRemisionModel::CLAVE_UNIDAD => 'required',
            DetalleRemisionModel::UNIDAD => 'required',
            DetalleRemisionModel::DESCRIPCION => 'required',
            DetalleRemisionModel::VALOR_UNITARIO => 'required',
            DetalleRemisionModel::IMPORTE => 'required',
            DetalleRemisionModel::BRAND => 'required',
        ];
    }
    public function getByRemision($id){
        return $this->getWhere(DetalleRemisionModel::REMISIONID, $id)[0];
    }
}
