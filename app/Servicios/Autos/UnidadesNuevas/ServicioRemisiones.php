<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\CatModelosModel;
use App\Models\Autos\EstatusSalidaUnidadModel;
use App\Models\Autos\UnidadesNuevas\CatCombustible;
use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use App\Models\Autos\UnidadesNuevas\CatTipoBicicletas;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Servicios\Autos\ServicioCatModelos;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\ServicioDB;
use App\Servicios\Facturas\ServicioXmlFactura;
use App\Servicios\Refacciones\ServicioCatalogoColores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleXMLElement;

class ServicioRemisiones extends ServicioDB
{
    public function __construct()
    {
        //
        $this->recurso = 'unidades';
        $this->modelo = new RemisionModel();
        $this->modeloDetalleRemisionModel = new DetalleRemisionModel();
        $this->modeloCatColores = new CatalogoColoresModel();
        $this->modeloCatCombustible = new CatCombustible();
        $this->modelEstatusSalidaUnidad = new EstatusSalidaUnidadModel();
        $this->modelUbicaciones = new CatalogoUbicacionModel();
        $this->modelTipoBicicletas = new CatTipoBicicletas();
        $this->modelModelos = new CatModelosModel();

        $this->servicio_detalle_remision = new ServicioDetalleRemision();
        $this->modelo_estatus = new CatEstatusUNModel();
        $this->servicio_colores = new ServicioCatalogoColores();
        $this->servicio_tipo_bicicletas = new ServicioCatTipoBicicletas();
        $this->servicio_modelos = new ServicioCatModelos();
        $this->servicioXml = new ServicioXmlFactura();
    }

    public function getReglasGuardar()
    {
        return [
            RemisionModel::VERSION => 'required',
            RemisionModel::SERIE => 'required',
            RemisionModel::FOLIO => 'required',
            RemisionModel::FECHA => 'required',
            RemisionModel::SELLO => 'required',
            RemisionModel::NO_CERTIFICADO => 'required',
            RemisionModel::CERTIFICADO => 'required',
            RemisionModel::SUBTOTAL => 'required',
            RemisionModel::TIPO_CAMBIO => 'required',
            RemisionModel::MONEDA => 'required',
            RemisionModel::TOTAL => 'required',
            RemisionModel::TIPO_COMPROBANTE => 'required',
            RemisionModel::METODO_PAGO => 'required',
            RemisionModel::FORMA_PAGO => 'required',
            RemisionModel::LUGAR_EXPEDICION => 'required',
            RemisionModel::EMISOR_RFC => 'required',
            RemisionModel::EMISOR_NOMBRE => 'required',
            RemisionModel::EMISOR_REGIMEN_FISCAL => 'required',
            RemisionModel::RECEPTOR_RFC => 'required',
            RemisionModel::RECEPTOR_NOMBRE => 'required',
            RemisionModel::RECEPTOR_REGIMEN_FISCAL => 'required',
            RemisionModel::ESTATUS_ID => 'required',
            RemisionModel::UBICACION_ID => 'required',
            RemisionModel::ULTIMO_SERVICIO => 'required',
            RemisionModel::ID_STATUS_UNIDAD => 'required',
            RemisionModel::USERID => 'required',
            DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO => 'required',
            DetalleRemisionModel::NO_IDENTIFICACION => 'required',
            DetalleRemisionModel::CANTIDAD => 'required',
            DetalleRemisionModel::CLAVE_UNIDAD => 'required',
            DetalleRemisionModel::UNIDAD => 'required',
            DetalleRemisionModel::DESCRIPCION => 'required',
            DetalleRemisionModel::VALOR_UNITARIO => 'required',
            DetalleRemisionModel::IMPORTE => 'required',
            DetalleRemisionModel::BRAND => 'required',
            DetalleRemisionModel::COLOR_ID => 'required',
            DetalleRemisionModel::MODELO_ID => 'required',
            DetalleRemisionModel::TYPE_ID => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RemisionModel::VERSION => 'required',
            RemisionModel::SERIE => 'required',
            RemisionModel::FOLIO => 'required',
            RemisionModel::FECHA => 'required',
            RemisionModel::SELLO => 'required',
            RemisionModel::NO_CERTIFICADO => 'required',
            RemisionModel::CERTIFICADO => 'required',
            RemisionModel::SUBTOTAL => 'required',
            RemisionModel::TIPO_CAMBIO => 'required',
            RemisionModel::MONEDA => 'required',
            RemisionModel::TOTAL => 'required',
            RemisionModel::TIPO_COMPROBANTE => 'required',
            RemisionModel::METODO_PAGO => 'required',
            RemisionModel::FORMA_PAGO => 'required',
            RemisionModel::LUGAR_EXPEDICION => 'required',
            RemisionModel::EMISOR_RFC => 'required',
            RemisionModel::EMISOR_NOMBRE => 'required',
            RemisionModel::EMISOR_REGIMEN_FISCAL => 'required',
            RemisionModel::RECEPTOR_RFC => 'required',
            RemisionModel::RECEPTOR_NOMBRE => 'required',
            RemisionModel::RECEPTOR_REGIMEN_FISCAL => 'required',
            RemisionModel::ESTATUS_ID => 'required',
            RemisionModel::UBICACION_ID => 'required',
            RemisionModel::ULTIMO_SERVICIO => 'required',
            RemisionModel::ID_STATUS_UNIDAD => 'required',
            RemisionModel::USERID => 'required',
        ];
    }
    public function  store(Request $request)
    {

        ParametrosHttpValidador::validar($request, $this->getReglasGuardar());
        // dd($request);
        $remision = $this->crear([
            RemisionModel::VERSION => $request->get(RemisionModel::VERSION),
            RemisionModel::SERIE => $request->get(RemisionModel::SERIE),
            RemisionModel::FOLIO => $request->get(RemisionModel::FOLIO),
            RemisionModel::FECHA => $request->get(RemisionModel::FECHA),
            RemisionModel::SELLO => $request->get(RemisionModel::SELLO),
            RemisionModel::NO_CERTIFICADO => $request->get(RemisionModel::NO_CERTIFICADO),
            RemisionModel::CERTIFICADO => $request->get(RemisionModel::CERTIFICADO),
            RemisionModel::SUBTOTAL => $request->get(RemisionModel::SUBTOTAL),
            RemisionModel::TIPO_CAMBIO => $request->get(RemisionModel::TIPO_CAMBIO),
            RemisionModel::MONEDA => $request->get(RemisionModel::MONEDA),
            RemisionModel::TOTAL => $request->get(RemisionModel::TOTAL),
            RemisionModel::TIPO_COMPROBANTE => $request->get(RemisionModel::TIPO_COMPROBANTE),
            RemisionModel::METODO_PAGO => $request->get(RemisionModel::METODO_PAGO),
            RemisionModel::FORMA_PAGO => $request->get(RemisionModel::FORMA_PAGO),
            RemisionModel::LUGAR_EXPEDICION => $request->get(RemisionModel::LUGAR_EXPEDICION),
            RemisionModel::EMISOR_RFC => $request->get(RemisionModel::EMISOR_RFC),
            RemisionModel::EMISOR_NOMBRE => $request->get(RemisionModel::EMISOR_NOMBRE),
            RemisionModel::EMISOR_REGIMEN_FISCAL => $request->get(RemisionModel::EMISOR_REGIMEN_FISCAL),
            RemisionModel::RECEPTOR_RFC => $request->get(RemisionModel::RECEPTOR_RFC),
            RemisionModel::RECEPTOR_NOMBRE => $request->get(RemisionModel::RECEPTOR_NOMBRE),
            RemisionModel::RECEPTOR_REGIMEN_FISCAL => $request->get(RemisionModel::RECEPTOR_REGIMEN_FISCAL),
            RemisionModel::ESTATUS_ID => $request->get(RemisionModel::ESTATUS_ID),
            RemisionModel::UBICACION_ID => $request->get(RemisionModel::UBICACION_ID),
            RemisionModel::ULTIMO_SERVICIO => $request->get(RemisionModel::ULTIMO_SERVICIO),
            RemisionModel::ID_STATUS_UNIDAD => $request->get(RemisionModel::ID_STATUS_UNIDAD),
            RemisionModel::USERID => $request->get(RemisionModel::USERID),

        ]);
        //Insertar TODAS las unidades
        foreach ($request->get(DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO) as $item => $val) {
            $this->servicio_detalle_remision->crear([
                DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO => $request->get(DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO)[$item],
                DetalleRemisionModel::NO_IDENTIFICACION => $request->get(DetalleRemisionModel::NO_IDENTIFICACION)[$item],
                DetalleRemisionModel::CANTIDAD => $request->get(DetalleRemisionModel::CANTIDAD)[$item],
                DetalleRemisionModel::CLAVE_UNIDAD => $request->get(DetalleRemisionModel::CLAVE_UNIDAD)[$item],
                DetalleRemisionModel::UNIDAD => $request->get(DetalleRemisionModel::UNIDAD)[$item],
                DetalleRemisionModel::DESCRIPCION => $request->get(DetalleRemisionModel::DESCRIPCION)[$item],
                DetalleRemisionModel::VALOR_UNITARIO => $request->get(DetalleRemisionModel::VALOR_UNITARIO)[$item],
                DetalleRemisionModel::IMPORTE => $request->get(DetalleRemisionModel::IMPORTE)[$item],
                DetalleRemisionModel::BRAND => $request->get(DetalleRemisionModel::BRAND)[$item],
                DetalleRemisionModel::COLOR_ID => $request->get(DetalleRemisionModel::COLOR_ID)[$item],
                DetalleRemisionModel::MODELO_ID => $request->get(DetalleRemisionModel::MODELO_ID)[$item],
                DetalleRemisionModel::TYPE_ID => $request->get(DetalleRemisionModel::TYPE_ID)[$item],
                DetalleRemisionModel::REMISIONID => $remision->id
            ]);
        }
    }
    public function  update(Request $request, $id)
    {
        ParametrosHttpValidador::validar($request, $this->getReglasUpdate());
        $remision = $this->massUpdateWhereId(RemisionModel::ID, $id, [
            RemisionModel::VERSION => $request->get(RemisionModel::VERSION),
            RemisionModel::SERIE => $request->get(RemisionModel::SERIE),
            RemisionModel::FOLIO => $request->get(RemisionModel::FOLIO),
            RemisionModel::FECHA => $request->get(RemisionModel::FECHA),
            RemisionModel::SELLO => $request->get(RemisionModel::SELLO),
            RemisionModel::NO_CERTIFICADO => $request->get(RemisionModel::NO_CERTIFICADO),
            RemisionModel::CERTIFICADO => $request->get(RemisionModel::CERTIFICADO),
            RemisionModel::SUBTOTAL => $request->get(RemisionModel::SUBTOTAL),
            RemisionModel::TIPO_CAMBIO => $request->get(RemisionModel::TIPO_CAMBIO),
            RemisionModel::MONEDA => $request->get(RemisionModel::MONEDA),
            RemisionModel::TOTAL => $request->get(RemisionModel::TOTAL),
            RemisionModel::TIPO_COMPROBANTE => $request->get(RemisionModel::TIPO_COMPROBANTE),
            RemisionModel::METODO_PAGO => $request->get(RemisionModel::METODO_PAGO),
            RemisionModel::FORMA_PAGO => $request->get(RemisionModel::FORMA_PAGO),
            RemisionModel::LUGAR_EXPEDICION => $request->get(RemisionModel::LUGAR_EXPEDICION),
            RemisionModel::EMISOR_RFC => $request->get(RemisionModel::EMISOR_RFC),
            RemisionModel::EMISOR_NOMBRE => $request->get(RemisionModel::EMISOR_NOMBRE),
            RemisionModel::EMISOR_REGIMEN_FISCAL => $request->get(RemisionModel::EMISOR_REGIMEN_FISCAL),
            RemisionModel::RECEPTOR_RFC => $request->get(RemisionModel::RECEPTOR_RFC),
            RemisionModel::RECEPTOR_NOMBRE => $request->get(RemisionModel::RECEPTOR_NOMBRE),
            RemisionModel::RECEPTOR_REGIMEN_FISCAL => $request->get(RemisionModel::RECEPTOR_REGIMEN_FISCAL),
            RemisionModel::ESTATUS_ID => $request->get(RemisionModel::ESTATUS_ID),
            RemisionModel::UBICACION_ID => $request->get(RemisionModel::UBICACION_ID),
            RemisionModel::ULTIMO_SERVICIO => $request->get(RemisionModel::ULTIMO_SERVICIO),
            RemisionModel::ID_STATUS_UNIDAD => $request->get(RemisionModel::ID_STATUS_UNIDAD),
            RemisionModel::USERID => $request->get(RemisionModel::USERID),

        ]);
        // $this->servicio_detalle_remision->massUpdateWhereId(DetalleRemisionModel::REMISIONID, $id, [
        //     DetalleRemisionModel::PUERTAS => $request->get(DetalleRemisionModel::PUERTAS),
        //     DetalleRemisionModel::CILINDROS => $request->get(DetalleRemisionModel::CILINDROS),
        //     DetalleRemisionModel::TRANSMISION => $request->get(DetalleRemisionModel::TRANSMISION),
        //     DetalleRemisionModel::COMBUSTIBLE_ID => $request->get(DetalleRemisionModel::COMBUSTIBLE_ID),
        //     DetalleRemisionModel::CAPACIDAD => $request->get(DetalleRemisionModel::CAPACIDAD),
        //     DetalleRemisionModel::COLORINTID => $request->get(DetalleRemisionModel::COLORINTID),
        //     DetalleRemisionModel::COLOREXTID => $request->get(DetalleRemisionModel::COLOREXTID)
        // ]);
        return $remision;
    }
    //Update only some fields 
    public function updateRemision($request, $id)
    {
        $data = [];
        if (isset($request[RemisionModel::FECHA])) {
            $data[RemisionModel::FECHA] = $request[RemisionModel::FECHA];
        }
        if (isset($request[RemisionModel::ESTATUS_ID])) {
            $data[RemisionModel::ESTATUS_ID] = $request[RemisionModel::ESTATUS_ID];
        }
        if (isset($request[RemisionModel::UBICACION_ID])) {
            $data[RemisionModel::UBICACION_ID] = $request[RemisionModel::UBICACION_ID];
        }
        if (isset($request[RemisionModel::ULTIMO_SERVICIO])) {
            $data[RemisionModel::ULTIMO_SERVICIO] = $request[RemisionModel::ULTIMO_SERVICIO];
        }
        return $this->massUpdateWhereId(RemisionModel::ID, $id, $data);
    }
    public function getUnidades($parametros)
    {
        $tabla_unidades = $this->modelo->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_cat_colores = $this->modeloCatColores->getTable();
        $tabla_estatus_salida_unidad = $this->modelEstatusSalidaUnidad->getTable();
        $tabla_ubicaciones = $this->modelUbicaciones->getTable();
        $tabla_tipo_bicicletas = $this->modelTipoBicicletas->getTable();
        $tabla_modelos = $this->modelModelos->getTable();
        // DetalleCostosRemisionModel
        //aqui poner los joins
        $query = $this->modelo->select(
            $tabla_unidades . '.' . RemisionModel::ID,
            // $tabla_estatus_remision. '.' . CatEstatusUNModel::ESTATUS,
            $tabla_unidades . '.' . RemisionModel::TOTAL . ' as precio_venta',
            $tabla_unidades . '.' . RemisionModel::VERSION,
            $tabla_unidades . '.' . RemisionModel::SERIE,
            $tabla_unidades . '.' . RemisionModel::FOLIO,
            $tabla_unidades . '.' . RemisionModel::FECHA,
            $tabla_unidades . '.' . RemisionModel::SELLO,
            $tabla_unidades . '.' . RemisionModel::NO_CERTIFICADO,
            $tabla_unidades . '.' . RemisionModel::CERTIFICADO,
            $tabla_unidades . '.' . RemisionModel::SUBTOTAL,
            $tabla_unidades . '.' . RemisionModel::TIPO_CAMBIO,
            $tabla_unidades . '.' . RemisionModel::MONEDA,
            $tabla_unidades . '.' . RemisionModel::TIPO_COMPROBANTE,
            $tabla_unidades . '.' . RemisionModel::METODO_PAGO,
            $tabla_unidades . '.' . RemisionModel::FORMA_PAGO,
            $tabla_unidades . '.' . RemisionModel::LUGAR_EXPEDICION,
            $tabla_unidades . '.' . RemisionModel::EMISOR_RFC,
            $tabla_unidades . '.' . RemisionModel::EMISOR_NOMBRE,
            $tabla_unidades . '.' . RemisionModel::EMISOR_REGIMEN_FISCAL,
            $tabla_unidades . '.' . RemisionModel::RECEPTOR_RFC,
            $tabla_unidades . '.' . RemisionModel::RECEPTOR_NOMBRE,
            $tabla_unidades . '.' . RemisionModel::RECEPTOR_REGIMEN_FISCAL,
            $tabla_unidades . '.' . RemisionModel::ULTIMO_SERVICIO,
            $tabla_unidades . '.' . RemisionModel::CREATED_AT,

            $tabla_estatus_salida_unidad . '.' . EstatusSalidaUnidadModel::NOMBRE . ' as estatus_unidad',

            $tabla_detalleRemision . '.' . DetalleRemisionModel::ID.' as detalle_id',
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CLAVE_PRODUCTO_SERVICIO,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::NO_IDENTIFICACION,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CANTIDAD,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::CLAVE_UNIDAD,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::UNIDAD,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::DESCRIPCION,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::VALOR_UNITARIO,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::IMPORTE,
            $tabla_detalleRemision . '.' . DetalleRemisionModel::BRAND,

            'color' . '.' . CatalogoColoresModel::ID . ' as id_color',
            'color' . '.' . CatalogoColoresModel::NOMBRE . ' as color',
            $tabla_ubicaciones . '.' . CatalogoUbicacionModel::NOMBRE . ' as ubicacion',
            $tabla_tipo_bicicletas . '.' . CatTipoBicicletas::TIPO_BICILETA . ' as tipo_bicicleta',
            $tabla_modelos . '.' . CatModelosModel::NOMBRE . ' as modelo'


        )->from($tabla_unidades);
        // $query->join($tabla_estatus_remision, $tabla_estatus_remision . '.id', '=', $tabla_unidades . '.' . RemisionModel::ESTATUS_ID); --> checale perro
        $query->Leftjoin($tabla_detalleRemision, $tabla_detalleRemision . '.' . DetalleRemisionModel::REMISIONID, '=', $tabla_unidades . '.' . RemisionModel::ID);

        $query->Leftjoin($tabla_cat_colores . ' as color', 'color.' . CatalogoColoresModel::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::COLOR_ID);
        $query->Leftjoin($tabla_estatus_salida_unidad, $tabla_estatus_salida_unidad . '.' . EstatusSalidaUnidadModel::ID, '=', $tabla_unidades . '.' . RemisionModel::ESTATUS_ID);
        $query->Leftjoin($tabla_ubicaciones, $tabla_ubicaciones . '.' . CatalogoUbicacionModel::ID, '=', $tabla_unidades . '.' . RemisionModel::UBICACION_ID);
        $query->Leftjoin($tabla_tipo_bicicletas, $tabla_tipo_bicicletas . '.' . CatTipoBicicletas::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::TYPE_ID);
        $query->Leftjoin($tabla_modelos, $tabla_modelos . '.' . CatModelosModel::ID, '=', $tabla_detalleRemision . '.' . DetalleRemisionModel::MODELO_ID);
        if (isset($parametros[RemisionModel::SERIE])) {
            $query->where(RemisionModel::SERIE, $parametros[RemisionModel::SERIE]);
        }

        if (isset($parametros[RemisionModel::ESTATUS_ID])) {
            $query->where($tabla_unidades . '.' . RemisionModel::ESTATUS_ID, '=', $parametros[RemisionModel::ESTATUS_ID]);
        }
        if (isset($parametros[RemisionModel::ID])) {
            $query->where($tabla_unidades . '.' . RemisionModel::ID, $parametros[RemisionModel::ID]);
        }
        $query->limit(200);
        return $query->get();
    }

    public function getunidadesNuevas($parameters)
    {
        return $this->getUnidades($parameters);
    }

    public function getunidadesSemiNuevas()
    {
        return $this->getUnidades([
            // 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function getUnidadSeminuevaById($id)
    {
        return $this->getUnidades([
            'id' => $id,
            // 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function getUnidadById($id)
    {
        return $this->getUnidades([
            RemisionModel::ID => $id,
            // 'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getTotalUnidades($parametros)
    {
        $tabla_unidades = $this->modelo->getTable();
        $tabla_detalleRemision = $this->modeloDetalleRemisionModel->getTable();
        $tabla_detalle_costo_remision = $this->modeloDetalleCostosRemision->getTable();
        $query = $this->modelo->select(
            DB::raw('sum(' . $tabla_detalle_costo_remision . '.' . RemisionModel::TOTAL . ') as total_precio_venta')
        )->from($tabla_unidades);
        $query->join($tabla_detalleRemision, $tabla_detalleRemision . '.' . DetalleRemisionModel::ID, '=', $tabla_unidades . '.' . RemisionModel::ID);

        if (isset($parametros['id_estado'])) {
            // $query->where('unidades.id_estado', $parametros['id_estado']);
        }

        return $query->get();
    }

    public function getTotalUnidadesNuevas()
    {
        return $this->getTotalUnidades([
            // 'id_estado' => UnidadesModel::TIPO_NUEVO
        ]);
    }

    public function getTotalUnidadesSemiNuevas()
    {
        return $this->getTotalUnidades([
            // 'id_estado' => UnidadesModel::TIPO_SEMI_NUEVO
        ]);
    }

    public function handleDataXmlRemision($path_factura = '')
    {
        if ($path_factura == '') {
            return [];
        }
        $real_path = realpath('../storage/app' . $path_factura);
        $xml = $this->getRemisionXml($real_path);
        $comprobante = $xml->xpath('//cfdi:Comprobante')[0]->attributes();
        $emisor = $xml->xpath('//cfdi:Comprobante//cfdi:Emisor')[0]->attributes();
        $receptor = $xml->xpath('//cfdi:Comprobante//cfdi:Receptor')[0]->attributes();

        $Conceptos = [];
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto) {
            $Conceptos[] = [
                "ClaveProdServ" => $Concepto['ClaveProdServ'],
                "NoIdentificacion" => $Concepto['NoIdentificacion'],
                "Cantidad" => $Concepto['Cantidad'],
                "ClaveUnidad" => $Concepto['ClaveUnidad'],
                "Unidad" => $Concepto['Unidad'],
                "Descripcion" => $Concepto['Descripcion'],
                "ValorUnitario" => $Concepto['ValorUnitario'],
                "Importe" => $Concepto['Importe'],
            ];
            // echo "<br />"; 
            // echo $Concepto['ClaveProdServ']; 
            // echo "<br />"; 
            // echo $Concepto['NoIdentificacion']; 
            // echo "<br />"; 
            // echo $Concepto['Cantidad']; 
            // echo "<br />"; 
            // echo $Concepto['ClaveUnidad']; 
            // echo "<br />"; 
            // echo $Concepto['Unidad']; 
            // echo "<br />"; 
            // echo $Concepto['Descripcion']; 
            // echo "<br />"; 
            // echo $Concepto['ValorUnitario']; 
            // echo "<br />"; 
            // echo $Concepto['Importe']; 
            // echo "<br />";   
            // echo "<br />"; 
        }
        return [
            'comprobante' => $this->getComprobante($comprobante),
            'emisor' => $this->getAttrsEmisor($emisor),
            'receptor' => $this->getAttrsReceptor($receptor),
            'conceptos' => $Conceptos,
        ];
        // return $xml;
    }

    public function getRemisionxml($path_factura = '')
    {
        if ($path_factura == '') {
            return [];
        }

        $xml = simplexml_load_file($path_factura);
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);
        // $xml->registerXPathNamespace('cfdi', $ns['Emisor']);
        // $xml->registerXPathNamespace('cfdi', $ns['FORDADD']);
        // $xml->registerXPathNamespace('venta_vehiculos', $ns['ventavehiculos']);
        // $xml->registerXPathNamespace('tfd', $ns['tfd']);
        // $xml->registerXPathNamespace('adddomrec', $ns['adddomrec']);
        // $xml->registerXPathNamespace('terceros', $ns['terceros']);

        return $xml;
    }

    public function getAttrsEmisor($parameters)
    {

        return [
            "Rfc" => $this->servicioXml->validateAndCastItem($parameters["Rfc"]),
            "Nombre" => $this->servicioXml->validateAndCastItem($parameters["Nombre"]),
            "RegimenFiscal" => $this->servicioXml->validateAndCastItem($parameters["RegimenFiscal"]),
        ];
    }

    public function getAttrsDomicilioReceptor($parameters)
    {
        return [
            "calle" => $this->servicioXml->validateAndCastItem($parameters["calle"]),
            "noExterior" => $this->servicioXml->validateAndCastItem($parameters["noExterior"]),
            "localidad" => $this->servicioXml->validateAndCastItem($parameters["localidad"]),
            "municipio" => $this->servicioXml->validateAndCastItem($parameters["municipio"]),
            "estado" => $this->servicioXml->validateAndCastItem($parameters["estado"]),
            "pais" => $this->servicioXml->validateAndCastItem($parameters["pais"]),
            "codigoPostal" => $this->servicioXml->validateAndCastItem($parameters["codigoPostal"])
        ];
    }

    public function getComprobante($parameters)
    {
        return [
            "Version" => $this->servicioXml->validateAndCastItem($parameters["Version"]),
            "Serie" => $this->servicioXml->validateAndCastItem($parameters["Serie"]),
            "Folio" => $this->servicioXml->validateAndCastItem($parameters["Folio"]),
            "Fecha" => $this->servicioXml->validateAndCastItem($parameters["Fecha"]),
            "Sello" => $this->servicioXml->validateAndCastItem($parameters["Sello"]),
            "NoCertificado" => $this->servicioXml->validateAndCastItem($parameters["NoCertificado"]),
            "Certificado" => $this->servicioXml->validateAndCastItem($parameters["Certificado"]),
            "SubTotal" => $this->servicioXml->validateAndCastItem($parameters["SubTotal"]),
            "TipoCambio" => $this->servicioXml->validateAndCastItem($parameters["TipoCambio"]),
            "Moneda" => $this->servicioXml->validateAndCastItem($parameters["Moneda"]),
            "Total" => $this->servicioXml->validateAndCastItem($parameters["Total"]),
            "TipoDeComprobante" => $this->servicioXml->validateAndCastItem($parameters["TipoDeComprobante"]),
            "MetodoPago" => $this->servicioXml->validateAndCastItem($parameters["MetodoPago"]),
            "FormaPago" => $this->servicioXml->validateAndCastItem($parameters["FormaPago"]),
            "LugarExpedicion" => $this->servicioXml->validateAndCastItem($parameters["LugarExpedicion"]),
        ];
    }
    public function getAttrsReceptor($parameters)
    {
        return [
            "Rfc" => $this->servicioXml->validateAndCastItem($parameters["Rfc"]),
            "Nombre" => $this->servicioXml->validateAndCastItem($parameters["Nombre"]),
            "UsoCFDI" => $this->servicioXml->validateAndCastItem($parameters["UsoCFDI"]),
        ];
    }
    public function getAttrsTraslados($parameters)
    {
        return [
            "Base" => $this->servicioXml->validateAndCastItem($parameters["Base"]),
            "Impuesto" => $this->servicioXml->validateAndCastItem($parameters["Impuesto"]),
            "TipoFactor" => $this->servicioXml->validateAndCastItem($parameters["TipoFactor"]),
            "TasaOCuota" => $this->servicioXml->validateAndCastItem($parameters["TasaOCuota"]),
            "Importe" => $this->servicioXml->validateAndCastItem($parameters["Importe"])
        ];
    }
    //Función para saber si existe un color
    public function validarColor($colorSave)
    {
        $color = $this->modeloCatColores->where(CatalogoColoresModel::NOMBRE, $colorSave)
            ->get();
        $existe = 1;
        if (count($color) == 0) {
            $existe = 0;
            //Crear color
            $color = $this->servicio_colores->crear([
                CatalogoColoresModel::NOMBRE => $colorSave,
                CatalogoColoresModel::CLAVE => $colorSave
            ]);
        } else {
            $color = $color[0];
        }
        return [
            "id" => $color[CatalogoColoresModel::ID],
            "existe" => $existe,
            "color" => $color[CatalogoColoresModel::NOMBRE],
            "clave" => $color[CatalogoColoresModel::CLAVE]
        ];
    }
}
