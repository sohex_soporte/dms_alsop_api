<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatAduanasUNModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatAduanas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo aduanas';
        $this->modelo = new CatAduanasUNModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatAduanasUNModel::ADUANA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatAduanasUNModel::ADUANA => 'required'
        ];
    }
}
