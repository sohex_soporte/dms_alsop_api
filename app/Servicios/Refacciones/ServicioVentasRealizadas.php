<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Autos\ServicioHistoricoProductosServicio;
use App\Servicios\Core\ServicioDB;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\permisoVentaModel;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Refacciones\ReVentasVentanillaTallerModel;
use App\Servicios\Refacciones\ServicioCurl;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use App\Models\Refacciones\OperacionesPiezasModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Models\Refacciones\VentaServicioModel;
use App\Servicios\Core\ServicioManejoArchivos;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ServicioVentasRealizadas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';
        $this->servicioFolio = new ServicioFolios();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioReVentasEstatus = new ServicioReVentasEstatus();
        $this->servicioReVentaVentanillaTaller = new ServicioReVentaVentanillaTaller();
        $this->servicioCurl = new ServicioCurl();
        $this->servicioProductos = new ServicioProductos();
        $this->modelo = new VentasRealizadasModel();
        $this->modeloEstatusCompra = new EstatusCompra();
        $this->modeloCliente = new ClientesModel();
        $this->modelo_plazo_credito = new PlazoCreditoModel();
        $this->modeloFolio = new FoliosModel();
        $this->modeloVentaServicio = new VentaServicioModel();
        $this->servicioCuentaxCobrar = new ServicioCuentaPorCobrar();
        $this->servicio_abonos = new ServicioAbono();
        $this->servicioHistoricoProductosServicio = new ServicioHistoricoProductosServicio();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();
        $this->servicioVentasServicios = new ServicioVentasServicios();
        $this->servicioOrdenCompra = new ServicioOrdenCompra();
        $this->servicioProductoPedidos = new ServicioProductoPedidos();
        

        $this->servicioManejoArchivos = new ServicioManejoArchivos();
        $this->modeloOperacionesPiezas = new OperacionesPiezasModel();
    }

    public function getReglasGuardar()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::CLIENTE_ID => 'exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric'
        ];
    }

    public function getReglasGuardarSinFolio()
    {
        return [
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::REL_PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentaProductoModel::CANTIDAD => 'required|numeric',
            VentasRealizadasModel::VENDEDOR_ID => 'required|numeric',
            VentaProductoModel::VALOR_UNITARIO => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric|exists:cat_tipo_precio,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric'
        ];
    }

    public function getReglasFinalizaVentaContado()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'nullable|numeric'
        ];
    }

    public function getReglasFinalizaVentaCredito()
    {
        return [
            VentasRealizadasModel::FOLIO_ID => 'required|exists:folios,id',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'required|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::CONCEPTO => 'required',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'nullable|numeric'
        ];
    }
    public function getReglasVentasAgrupadasMes()
    {
        return [
            VentaProductoModel::PRODUCTO_ID => 'required|exists:producto,id',
            VentasRealizadasModel::CANTIDAD_MESES => 'nullable|numeric',
            VentasRealizadasModel::YEAR => 'required|numeric'

        ];
    }

    public function registrarVenta($parametros)
    {
        $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_VENTA_MOSTRADOR);
        $parametros[VentasRealizadasModel::FOLIO_ID] = $folio->id;
        $parametros[VentasRealizadasModel::VENTA_TOTAL] = $parametros[VentaProductoModel::VALOR_UNITARIO] * $parametros[VentaProductoModel::CANTIDAD];
        $venta = $this->crear($parametros);
        if ($venta) {
            $re_estastus[ReVentasEstatusModel::VENTA_ID] = $venta->id;
            $re_estastus[ReVentasEstatusModel::ESTATUS_VENTA_ID] = EstatusVentaModel::ESTATUS_PROCESO;
            $this->servicioReVentasEstatus->storeReVentasEstatus($re_estastus);

            $parametros[VentasRealizadasModel::VENTA_ID] = $venta->id;
            $cantidad_stock = $this->servicioDesgloseProductos->getStockByProducto(['producto_id' => $parametros['producto_id']]);
            
            if (intval($parametros[VentaProductoModel::CANTIDAD]) > $cantidad_stock->cantidad_actual) {
                $cantidad_pedido = intval($parametros[VentaProductoModel::CANTIDAD]) - $cantidad_stock->cantidad_actual;
                $parametros[VentaProductoModel::CANTIDAD] = $cantidad_stock->cantidad_actual;
                
                $this->servicioProductoPedidos->store([
                    ProductosPedidosModel::PRODUCTO_ID => $parametros[ProductosPedidosModel::PRODUCTO_ID],
                    ProductosPedidosModel::CANTIDAD_SOLICITADA => $cantidad_pedido
                ]);
                return $this->servicioVentaProducto->crear($parametros);
            }
            $this->servicioVentaProducto->crear($parametros);
        }
        return isset($venta) && $venta ? $venta : null;
    }

    public function getReglasCrearVentaMpm()
    {
        return [
            VentasRealizadasModel::PRECIO_ID => 'required|exists:precio,id',
            VentasRealizadasModel::CLIENTE_ID => 'required|exists:clientes,id',
            VentasRealizadasModel::VENTA_TOTAL => 'required|numeric',
            VentasRealizadasModel::TIPO_VENTA => 'required|numeric',
            VentasRealizadasModel::ALMACEN_ID => 'required|numeric',
            VentasRealizadasModel::TIPO_PRECIO_ID => 'required|numeric|exists:cat_tipo_precio,id',
            ReVentasVentanillaTallerModel::NUMERO_ORDEN => 'required|numeric'
        ];
    }

    public function listaProductosTaller($numero_orden)
    {
        return $this->curlGetDataApiMpm($numero_orden);
    }

    public function crearVentaMpm($parametros)
    {
        #TODO: Verificar funcionalidad
        $existe_venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);
        if (!$existe_venta) {
            $folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::PROCESO_SERVICIOS);
            $parametros[VentasRealizadasModel::FOLIO_ID] = $folio->id;
            $parametros[VentasRealizadasModel::VENTA_TOTAL] = 0;
            $parametros[VentasRealizadasModel::NUMERO_ORDEN];
            $venta = $this->crear($parametros);

            if (!$venta) {
                DB::rollback();
                return null;
            }
            $re_estastus[ReVentasEstatusModel::VENTA_ID] = $venta->id;
            $re_estastus[ReVentasEstatusModel::ESTATUS_VENTA_ID] = EstatusVentaModel::ESTATUS_PROCESO;
            $this->servicioReVentasEstatus->storeReVentasEstatus($re_estastus);

            $productos_taller = $this->curlGetDataApiMpm($parametros[ReVentasVentanillaTallerModel::NUMERO_ORDEN]);
            $this->guardarDetalleProductoVentaMpm($productos_taller, [
                VentaProductoModel::PRECIO_ID =>  $parametros[VentaProductoModel::PRECIO_ID],
                VentaProductoModel::CLIENTE_ID => $parametros[VentaProductoModel::CLIENTE_ID],
                VentaProductoModel::FOLIO_ID => $folio->id,
                VentaProductoModel::VENTA_ID => $venta->id
            ]);

            $total_productos_venta = $this->servicioVentaProducto->totalVentaByFolio($folio->id);
            $totalservicio = $this->servicioVentasServicios->totalservicio($venta->id);
            $total_venta =  $total_productos_venta + $totalservicio;

            $this->massUpdateWhereId(VentasRealizadasModel::ID, $venta->id, [
                VentasRealizadasModel::VENTA_TOTAL => $total_venta
            ]);

            return $venta;
        } else {
            $folio = $this->servicioFolio->getById($existe_venta->folio_id);
            $venta = $existe_venta;
            return $venta;
        }
    }

    public function guardarDetalleProductoVentaMpm($data_array, $parametros_venta)
    {
        foreach ($data_array['refacciones'] as $key => $item) {
            if ($item->tipo == "OPERACION") {
                $precio_total = (float)$item->precio_unitario + (float)$item->costo_mo;
                return $this->servicioVentasServicios->createUpdateServicioVenta([
                    VentaServicioModel::NOMBRE_SERVICIO => $item->descripcion,
                    VentaProductoModel::VENTA_ID =>  $parametros_venta[VentaProductoModel::VENTA_ID],
                    VentaServicioModel::PRECIO => $precio_total,
                    VentaServicioModel::NO_IDENTIFICACION => trim($item->num_pieza),
                    VentaServicioModel::CANTIDAD => (int) $item->cantidad,
                    VentaServicioModel::IVA => $item->iva,
                    VentaServicioModel::COSTO_MO =>  $item->costo_mo
                ]);
            } else {
                $existe_producto = $this->servicioProductos->validarExistenciaProducto([
                    'No_identificacion' => trim($item->num_pieza)
                ])->first();

                $no_pieza =  isset($existe_producto->no_identificacion_dms) ? $existe_producto->no_identificacion_dms : trim($item->num_pieza);
                $cantidad_stock = $this->servicioProductos->stockProductos([
                    'no_identificacion' => $no_pieza
                ])->first();

                if (floatval($item->cantidad) > $cantidad_stock->cantidad_actual) {
                    throw new ParametroHttpInvalidoException(['validacion_msg' => "No Hay productos suficientes en almacen $no_pieza"]);
                }

                ##TODO: validar redondeo
                if (!empty($existe_producto) || !empty($cantidad_stock)) {

                    $productos_venta = $this->servicioVentaProducto->existeProducto([
                        VentaProductoModel::FOLIO_ID => $parametros_venta[VentaProductoModel::FOLIO_ID],
                        VentaProductoModel::VENTA_ID => $parametros_venta[VentaProductoModel::VENTA_ID],
                        VentaProductoModel::PRODUCTO_ID =>  isset($existe_producto->producto_id) ? $existe_producto->producto_id : $cantidad_stock->id
                    ]);

                    #si no existe en la venta se agrega
                    if (empty($productos_venta)) {
                        ##TODO: validar precios mano de obra y sumar en venta total
                        // $precio_total = (float)$item->precio_unitario + (float)$item->costo_mo;
                        $parametros_venta[VentaProductoModel::PRODUCTO_ID] = isset($existe_producto->producto_id) ? $existe_producto->producto_id : $cantidad_stock->id; //isset($existe_producto->producto_id);
                        $parametros_venta[VentaProductoModel::VALOR_UNITARIO] = !empty($existe_producto) ? $existe_producto->valor_unitario : $cantidad_stock->valor_unitario; //$existe_producto->valor_unitario;
                        $parametros_venta[VentaProductoModel::CANTIDAD] = round($item->cantidad);

                        $valor_unitario = !empty($existe_producto) ? $existe_producto->valor_unitario : $cantidad_stock->valor_unitario;
                        $parametros_venta[VentaProductoModel::TOTAL_VENTA] = $valor_unitario * $item->cantidad;
                        $parametros_venta[VentaProductoModel::DESCONTADO_ALMACEN] = 1;
                        $parametros_venta[VentaProductoModel::ORDEN_ORIGINAL] = 1;
                        $parametros_venta[VentaProductoModel::ESTATUS_PRODUCTO_ORDEN] = "O";
                        $parametros_venta[VentaProductoModel::ID_INDEX_REQUISICION] = $item->id;
                        $this->servicioVentaProducto->crear($parametros_venta);
                    }
                } else {
                    DB::rollback();
                    throw new ParametroHttpInvalidoException(['validacion_msg' => 'No existe producto con ese Num de Pieza']);
                }
            }
        }
    }

    public function curlGetDataApiMpm($numero_orden)
    {
        //DMS - https://sohex.net/queretaro_citas_dms/citas/dms_panles_queretaro/conexion/Conexion_DMS/recuperar_operaciones
        //MPM - https://so-hex.com/cs/mpm/matriz_mpm/orden/mpm_matriz/api/Conexion_DMS/recuperar_operaciones
        // dd("sasd");
        $request = $this->servicioCurl->curlPost(env('URL_SERVICIOS')  . env('URL_OPERACIONES'), [
            'id_cita' => $numero_orden
        ], true);


        if (empty($request)) {
            throw new ParametroHttpInvalidoException([
                'validacion_msg' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => ""])
            ]);
        }

        return $this->validarPiezasToMpm($request);
    }

    public function validarPiezasToMpm($request)
    {
        $new_array = [];
        $data_api =  json_decode($request);
        $validar = !empty($data_api) ? $data_api : [];
        // dd($validar);
        foreach ($validar->refacciones as $key => $value) {
            if (isset($value->num_pieza) && $value->num_pieza !== "") {
                $existe_producto = false;
                if ($value->tipo !== "OPERACION") {
                    $valida_producto = $this->servicioProductos->validarExistenciaProducto([
                        'No_identificacion' => trim($value->num_pieza)
                    ])->first();

                    $cantidad_stock = $this->servicioProductos->stockProductos([
                        'no_identificacion' => isset($valida_producto->no_identificacion_dms) ? trim($valida_producto->no_identificacion_dms) : trim($value->num_pieza)
                    ])->first();
                    $existe_producto = !empty($valida_producto) || !empty($cantidad_stock->id) ? true : false;
                } else {
                    $cantidad_stock = null;
                }

                $item = new \stdClass;
                $item->id = $value->id;
                $item->producto_id = isset($cantidad_stock->id) ? $cantidad_stock->id : null;
                $item->se_registro = $existe_producto;
                $item->cantidad = $value->cantidad;
                $item->cantidad_stock = isset($cantidad_stock->cantidad_actual) ? $cantidad_stock->cantidad_actual : 0;
                $item->descripcion = $value->descripcion;
                $item->num_pieza = trim($value->num_pieza);
                $item->existe = $value->existe;
                $item->precio_unitario =  isset($cantidad_stock->id) && $value->precio_unitario == '0' ? $cantidad_stock->valor_unitario : $value->precio_unitario;
                $item->total_horas = $value->total_horas;
                $item->costo_mo = $value->costo_total_con_iva;
                $item->iva = $value->costo_iva;
                $item->tipo = $value->tipo;
                // if (trim($value->num_pieza) == 'MXO5W30B') {
                // echo '<pre>';
                //     print_r($cantidad_stock);
                //     print_r($value);
                // print_r($item);
                // echo '</pre>';
                // die();
                // }
                array_push($new_array, $item);
            }
        }

        $responseData = [
            'vehiculo' => $validar->vehiculo,
            'refacciones' => $new_array
        ];
        return $responseData;
    }

    public function ventasByFolioId($folio_id)
    {
        return $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO,
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_ESTATUS . '.' . ReVentasEstatusModel::REL_ESTATUS,
            VentasRealizadasModel::REL_PRECIO,
            VentasRealizadasModel::REL_ALMACEN => function ($query) {
                $query->select(
                    Almacenes::ID,
                    Almacenes::NOMBRE,
                    Almacenes::CODIGO_ALMACEN
                );
            }
        ])->where(VentasRealizadasModel::FOLIO_ID, $folio_id)->get();
    }

    public function finalizarVenta($id, $request)
    {
        DB::beginTransaction();

        $modelo = $this->modelo->findOrFail($id);
        try {
            $data = $request->all();
            $modelo->fill($data);
            $modelo = $this->guardarModelo($modelo);
            if (empty($request->mpm)) {
                $cxc = $this->servicioCuentaxCobrar->procesarCuentasPorCobrar($request->all());
                DB::commit();
                return $cxc;
            }
            return $modelo;
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
            DB::rollback();
        }
    }

    //parche del parche
    public function descontarVentaMpm($id, $request)
    {
        DB::beginTransaction();

        $modelo = $this->modelo->findOrFail($id);
        try {
            $data = $request->all();
            $modelo->fill($data);
            $modelo = $this->guardarModelo($modelo);
            DB::commit();
            return $modelo;
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
            DB::rollback();
        }
    }

    /*public function procesarCuentasPorCobrar($parametros)
    {
        $importe = $parametros[VentasRealizadasModel::VENTA_TOTAL];

        if ($parametros['tipo_forma_pago_id'] == 2) {
            $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            $plazo_credito = $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID];
            $enganche = $parametros[CuentasPorCobrarModel::ENGANCHE];
            $totalsinenganche = $importe - $enganche;
            $cantidad_abonos = $this->modelo_plazo_credito->find($plazo_credito)->cantidad_mes;
            $abono_mensual = ($totalsinenganche / $cantidad_abonos);
            $interes_mensual = ($abono_mensual * $tasa_interes) / 100;
            $intereses = $interes_mensual * $cantidad_abonos;
            $total = $importe + $intereses;
        } else {
            $total = $parametros[VentasRealizadasModel::VENTA_TOTAL];
            $intereses = 0;
        }
        $params = [
            CuentasPorCobrarModel::FOLIO_ID => $parametros[CuentasPorCobrarModel::FOLIO_ID],
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[CuentasPorCobrarModel::CLIENTE_ID],
            CuentasPorCobrarModel::CONCEPTO => $parametros[CuentasPorCobrarModel::CONCEPTO],
            CuentasPorCobrarModel::IMPORTE => $importe,
            CuentasPorCobrarModel::TOTAL => $total,
            CuentasPorCobrarModel::FECHA => date('Y-m-d'),
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID],
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID],
            CuentasPorCobrarModel::ENGANCHE => $parametros[CuentasPorCobrarModel::ENGANCHE],
            CuentasPorCobrarModel::TASA_INTERES => $parametros[CuentasPorCobrarModel::TASA_INTERES],
            CuentasPorCobrarModel::INTERESES => $intereses,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_PROCESO,
            CuentasPorCobrarModel::USUARIO_GESTOR_ID =>  $parametros[CuentasPorCobrarModel::USUARIO_GESTOR_ID]
        ];

        if ($parametros['tipo_forma_pago_id'] == 2) {
            $data = $this->servicioCuentaxCobrar->update($params);
        } else {
            $data = $this->servicioCuentaxCobrar->crear($params);
        }

        $abonos = $this->servicio_abonos->crearAbonosByCuentaId($data->id);
        if (!$abonos) {
            return false;
        }
        return $data;
    }*/

    public function getAll()
    {
        $data =  $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO => function ($query) {
                $query->with([
                    'cuenta_por_cobrar'
                ]);
            },
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_FINALIZADAS => function ($query) {

                $query->with([
                    'estatus_venta'
                ]);
            }
        ])->get();

        $new_array = [];
        foreach ($data as $key => $item) {
            if (isset($item->ventas_finalizadas)) {
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function reglasListaVentaByStatus()
    {
        return [
            VentasRealizadasModel::ESTATUS_COMPRA => 'required|array',
            VentasRealizadasModel::CLIENTE_ID => 'nullable'
        ];
    }

    //TODO: listado ventas realizadas, separar devoluciones
    public function getVentasByStatus($parametros)
    {

        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tabla_clientes = ClientesModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tabla_clientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tabla_clientes . '.' . ClientesModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'folios.folio',
                'estatus_venta.nombre as estatus_venta',
                'clientes.nombre as nombre_cliente',
                'clientes.id as cliente_id',
                'ventas.created_at',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta'
            );

        if (isset($parametros[VentasRealizadasModel::CLIENTE_ID])) {
            $query->where($tabla_clientes . '.' . ClientesModel::ID, $parametros[VentasRealizadasModel::CLIENTE_ID]);
        }

        $query->where(ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableEstatusVenta . '.' . EstatusVentaModel::ID, $parametros[VentasRealizadasModel::ESTATUS_COMPRA]);
        return $query->get();
    }

    public function getById($id)
    {
        $data =  $this->modelo->with([
            VentasRealizadasModel::REL_FOLIO => function ($query) {
                $query->with([
                    'cuenta_por_cobrar'
                ]);
            },
            VentasRealizadasModel::REL_CLIENTE,
            VentasRealizadasModel::REL_VENTAS_FINALIZADAS => function ($query) {
                $query->with([
                    'estatus_venta'
                ]);
            }
        ])
            ->where(VentasRealizadasModel::ID, $id)
            ->get();

        $new_array = [];
        foreach ($data as $key => $item) {
            if (isset($item->ventas_finalizadas)) {
                array_push($new_array, $item);
            }
        }

        return $new_array;
    }

    public function updateStatusVenta($venta_id, $status_id)
    {
        $modelo = $this->modelo->find($venta_id);
        $modelo->estatus_id = $status_id;
        return $modelo->save();
    }

    public function getReglasBusqueda()
    {
        return [
            VentasRealizadasModel::FECHA_INICIO => 'required|date',
            VentasRealizadasModel::FECHA_FIN => 'required|date',
        ];
    }

    public function getReglasBusquedaTraspaso()
    {
        return [
            Traspasos::FECHA_INICIO => 'nullable|date',
            Traspasos::FECHA_FIN => 'nullable|date',
            VentasRealizadasModel::FOLIO_ID => 'nullable|exists:folios,id',
        ];
    }

    public function calcularTotal($modelo)
    {
        $ventas =  $modelo;
        $total = 0;
        foreach ($ventas as $key => $item) {
            $total = $total + $item->venta_total;
        }

        return [
            'total_venta' => $total,
            'data' => $modelo
        ];
    }

    public function filtrarPorFechas($parametros)
    {
        return $this->modelo->with([
            'folio' => function ($query) {
                $query->select('id', 'folio');
            },
            'estatus' => function ($query) {
                $query->select('id', 'nombre');
            },
            'cliente' => function ($query) {
                $query->select('id', 'nombre_empresa', 'numero_cliente', 'rfc');
            },
            'detalle_venta' => function ($query) {
                $query->with([
                    'producto' => function ($query_prod) {
                        $query_prod->select('id', 'no_identificacion', 'descripcion', 'valor_unitario');
                    }
                ]);
            }
        ])
            ->whereDate(VentasRealizadasModel::CREATED_AT, '>=', $parametros[VentasRealizadasModel::FECHA_INICIO])
            ->whereDate(VentasRealizadasModel::CREATED_AT, '<=', $parametros[VentasRealizadasModel::FECHA_FIN])
            ->get();
    }

    public function getVentasByFechas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableVentaProducto, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::FOLIO_ID)
            ->join($tableProducto, $tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                'ventas.id',
                'ventas.venta_total',
                'ventas.almacen_id',
                'folios.folio',
                'venta_producto.cantidad',
                'venta_producto.venta_total as venta_total_producto',
                'venta_producto.valor_unitario',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.unidad',
                'ventas.created_at',
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatusVenta',
                $tableEstatusVenta . '.' . EstatusVentaModel::ID . ' as estatusId'
            );
        if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
            $query->whereBetween('ventas.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA))
            ->get();
        return [
            'data' => $query->get()
        ];
    }

    public function getTotalesVentasByFechas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();

        $query = DB::table($tableVentas)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableVentaProducto, $tableFolios . '.' . FoliosModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::FOLIO_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->select(
                DB::raw('count(ventas.id) as total_ventas'),
                DB::raw('sum(venta_producto.cantidad) as total_cantidad'),
                DB::raw('sum(venta_producto.venta_total) as sum_venta_total'),
                DB::raw('sum(venta_producto.valor_unitario) as sum_valor_unitario')
            );
        if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
            $query->whereBetween('ventas.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
        }
        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA));
        return $query->first();
    }

    public function getBusquedaVentas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();

        $query = $this->modelo
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );
        if ($request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID)) {
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID));
        } else {
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, [
                EstatusVentaModel::ESTATUS_PROCESO,
                EstatusVentaModel::ESTATUS_FINALIZADA,
                EstatusVentaModel::ESTATUS_DEVOLUCION,
                EstatusVentaModel::ESTATUS_APARTADO
            ]);
        }
        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    public function getServicioByFolio($request)
    {

        $tableServicios = VentaServicioModel::getTableName();
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();

        $query = $this->modeloVentaServicio
            ->join($tableVentas, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableServicios . '.' . VentaServicioModel::VENTA_ID)
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableServicios . '.' . '*',
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );

        if ($request->get(VentasRealizadasModel::FOLIO_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, $request->get(VentasRealizadasModel::FOLIO_ID));
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);


        return $query->get();
    }

    public function getAllVentas($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableCxc = CuentasPorCobrarModel::getTableName();

        $query = $this->modelo
            ->join($tableFolios, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->leftJoin($tableCxc, $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID, '=', $tableCxc . '.' . CuentasPorCobrarModel::FOLIO_ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableVentas . '.' . VentasRealizadasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));

        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    public function getBusquedaVentasDevoluciones($request)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusVentaModel::getTableName();
        $tableClientes = ClientesModel::getTableName();
        $tableDevolucionVenta = DevolucionVentasModel::getTableName();
        $query = $this->modelo
            ->join($tableDevolucionVenta, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableDevolucionVenta . '.' . DevolucionVentasModel::VENTA_ID)
            ->join($tableFolios, $tableDevolucionVenta . '.' . DevolucionVentasModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusVentaModel::ID, '=', $tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID)
            ->join($tableClientes, $tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, '=', $tableClientes . '.' . ClientesModel::ID)
            ->select(
                $tableVentas . '.' . VentasRealizadasModel::ID,
                $tableDevolucionVenta . '.' . DevolucionVentasModel::FOLIO_ID,
                $tableFolios . '.' . FoliosModel::FOLIO,
                $tableClientes . '.' . ClientesModel::NUMERO_CLIENTE,
                $tableClientes . '.' . ClientesModel::NOMBRE . ' as cliente',
                $tableVentas . '.' . VentasRealizadasModel::VENTA_TOTAL,
                $tableVentas . '.' . VentasRealizadasModel::TIPO_VENTA,
                $tableEstatusVenta . '.' . EstatusVentaModel::NOMBRE . ' as estatus',
                $tableVentas . '.' . VentasRealizadasModel::CREATED_AT
            );
        if ($request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID)) {
            $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, $request->get(ReVentasEstatusModel::ESTATUS_VENTA_ID));
        } else {
            $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::ESTATUS_DEVOLUCION));
        }
        if ($request->get(VentasRealizadasModel::CLIENTE_ID)) {
            $query->where($tableVentas . '.' . VentasRealizadasModel::CLIENTE_ID, $request->get(VentasRealizadasModel::CLIENTE_ID));
        }
        if ($request->get(FoliosModel::FOLIO)) {
            $query->whereRaw('LOWER(' . $tableFolios . '.' . FoliosModel::FOLIO . ') LIKE (?) ', ["%{$request->get(FoliosModel::FOLIO)}%"]);
        }
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);

        return $query->get();
    }

    //TODO: Ventas
    public function reglasAutorizarPrecio()
    {
        return [
            permisoVentaModel::USUARIO_ID => 'required',
            // permisoVentaModel::ven =>'required',
        ];
    }

    public function finalizarVentaServicio($parametros)
    {
        $venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);
        if (empty($venta)) {
            throw new ParametroHttpInvalidoException([
                'msg' => "No se encontro la orden"
            ]);
        } else if (isset($venta) && $venta->orden_cerrada == 1) {
            throw new ParametroHttpInvalidoException([
                'msg' => "Orden cerrada"
            ]);
        }

        $total_venta = $this->servicioVentaProducto->totalVentaByFolio($venta->folio_id);
        $modelo = $this->modelo->findOrFail($venta->id);
        $modelo->fill([VentasRealizadasModel::ORDEN_CERRADA => true]);
        $modelo = $this->guardarModelo($modelo);
        $this->servicioCuentaxCobrar->procesarCuentasPorCobrar([
            VentasRealizadasModel::VENTA_TOTAL => $total_venta,
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => TipoFormaPagoModel::FORMA_CONTADO,
            CuentasPorCobrarModel::FOLIO_ID => $venta->folio_id,
            CuentasPorCobrarModel::CLIENTE_ID => $venta->cliente_id,
            CuentasPorCobrarModel::CONCEPTO => "CIERRE DE ORDEN",
            CuentasPorCobrarModel::TIPO_PAGO_ID => TipoFormaPagoModel::FORMA_CONTADO,
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => PlazoCreditoModel::UNA_EXHIBICION,
            CuentasPorCobrarModel::ENGANCHE => null,
            CuentasPorCobrarModel::TASA_INTERES => null,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_LIQUIDADO
        ]);


        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta->id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_FINALIZADA
        ]);

        $this->buscarProductosYdescontarStockByFolio($venta->folio_id);
        DB::commit();
        return $venta;
    }

    public function abrirOrdenServicio($parametros)
    {
        $venta = $this->buscarPorNumeroOrden($parametros[VentasRealizadasModel::NUMERO_ORDEN]);

        $modelo = $this->modelo->findOrFail($venta->id);
        $modelo->fill([VentasRealizadasModel::ORDEN_CERRADA => false]);
        $modelo = $this->guardarModelo($modelo);


        $this->servicioReVentasEstatus->storeReVentasEstatus([
            ReVentasEstatusModel::VENTA_ID => $venta->id,
            ReVentasEstatusModel::ESTATUS_VENTA_ID => EstatusVentaModel::ESTATUS_PROCESO
        ]);
    }

    public function buscarProductosYdescontarStockByFolio($folio_id)
    {
        $productos_venta = $this->servicioVentaProducto->getDataByFolio($folio_id);
        #validar que si son los mismos pero cantidad diferente
        foreach ($productos_venta as $key => $item) {
            if (empty($item->descontado_almacen)) {
                #descontar
                $this->servicioDesgloseProductos->actualizaStockByProducto([
                    VentaProductoModel::PRODUCTO_ID => $item->producto_id
                ]);

                #actualizar que ya se desconto
                $this->servicioVentaProducto->massUpdateWhereId(VentaProductoModel::ID, $item->id, [
                    VentaProductoModel::DESCONTADO_ALMACEN => 1
                ]);
            }
        }
    }

    public function buscarPorNumeroOrden($numero_orden)
    {
        return $this->modelo->where(VentasRealizadasModel::NUMERO_ORDEN, $numero_orden)->first();
    }

    public function getYearVentasByProducto($params)
    {
        $tableVentas = VentasRealizadasModel::getTableName();
        $tableVentaProducto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $fecha_actual = date('Y-m-d');
        //DB::connection()->enableQueryLog();
        $query = DB::table($tableVentas)
            ->join($tableVentaProducto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tableVentaProducto . '.' . VentaProductoModel::VENTA_ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->select(
                DB::raw('EXTRACT(YEAR FROM ' . $tableVentas . '.' . VentasRealizadasModel::CREATED_AT . ') as anio')
            );
        if (isset($params[VentaProductoModel::PRODUCTO_ID]) && $params[VentaProductoModel::PRODUCTO_ID]) {
            $query->where($tableVentaProducto . '.' . VentaProductoModel::PRODUCTO_ID, $params[VentaProductoModel::PRODUCTO_ID]);
        }
        if (isset($params[VentasRealizadasModel::YEAR]) && $params[VentasRealizadasModel::YEAR]) {
            $query->whereYear($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, $params[VentasRealizadasModel::YEAR]);
        }
        // if (isset($params[VentasRealizadasModel::CANTIDAD_MESES]) && $params[VentasRealizadasModel::CANTIDAD_MESES]) {
        //     $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
        //     $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
        //         $fecha_inicial . ' 00:00:00',
        //         $fecha_actual . ' 23:59:59'
        //     ]);
        // }

        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA));
        $query->groupBy(
            'producto_id',
            'anio'
        );
        return $query->get();
        //dd(DB::getQueryLog());
    }

    public function getVentasAgrupadasMes($params)
    {
        $where = '';
        $where_mes = '';

        $fecha_actual = date('Y-m-d');
        $mes_actual = Carbon::parse($fecha_actual)->format('m');
        $fecha_actual = date('Y-m-d H:i:s', strtotime($fecha_actual . ' +1 day'));

        if (isset($params[VentasRealizadasModel::CANTIDAD_MESES])) {

            if ($params[VentasRealizadasModel::CANTIDAD_MESES] > $mes_actual) {
                throw new ParametroHttpInvalidoException([
                    'msg' => "No puedes sobrepasar la cantidad máxima al mes inicial"
                ]);
            }

            $fecha_inicio = date("Y-m-d", strtotime($fecha_actual . " - " . $params[VentasRealizadasModel::CANTIDAD_MESES] . " month"));
            $mes_inicio = $mes_actual - $params[VentasRealizadasModel::CANTIDAD_MESES] + 1;
            $where_mes .= ($fecha_inicio) ? " where cat_meses.id between '{$mes_inicio}' and '{$mes_actual}'" : "";
            $where .= ($fecha_inicio) ? " and venta_producto.created_at between '{$fecha_inicio}' and '{$fecha_actual}'" : "";
        }
        return DB::select(
            DB::raw(
                'SELECT cat_meses.nombre as mes, T1.total_ventas, T1.venta_total
                FROM cat_meses
                LEFT JOIN(
                SELECT
                    SUM (venta_producto.cantidad) AS total_ventas,
                    SUM (venta_producto.venta_total) AS venta_total,
                    EXTRACT (MONTH FROM ventas.created_at) AS mes
                FROM
                    "ventas"
                INNER JOIN "venta_producto" ON "ventas"."id" = "venta_producto"."venta_id"
                INNER JOIN "re_ventas_estatus" ON "re_ventas_estatus"."ventas_id" = "ventas"."id"
                WHERE
                    "venta_producto"."producto_id" = ' . $params[VentaProductoModel::PRODUCTO_ID] . '
                AND "re_ventas_estatus"."activo" = true
                AND "re_ventas_estatus"."estatus_ventas_id" IN (' . EstatusVentaModel::ESTATUS_FINALIZADA . ') AND EXTRACT (YEAR FROM venta_producto.created_at) = ' . $params[VentasRealizadasModel::YEAR] . '
                ' . $where . '
                GROUP BY
                    "producto_id",
                    "mes"
                ) T1 ON T1.mes::integer  = cat_meses.id::integer'
                    . $where_mes
            )
        );
    }

    public function getVentasByUser($user_id)
    {

        $tabla_venta = VentasRealizadasModel::getTableName();
        $tabla_venta_producto = VentaProductoModel::getTableName();
        $tabla_producto = ProductosModel::getTableName();

        $query = $this->modelo->select(
            $tabla_venta_producto . '.' . VentaProductoModel::CANTIDAD,
            $tabla_venta_producto . '.' . VentaProductoModel::VALOR_UNITARIO,
            $tabla_producto . '.' . ProductosModel::DESCRIPCION,
            $tabla_producto . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            $tabla_producto . '.' . ProductosModel::PREFIJO,
            $tabla_producto . '.' . ProductosModel::SUFIJO,
            $tabla_producto . '.' . ProductosModel::BASICO,
            DB::raw('sum(' . $tabla_venta . '.' . VentasRealizadasModel::VENTA_TOTAL . ') as venta_total'),
        )->from($tabla_venta);
        $query->join($tabla_venta_producto, $tabla_venta_producto . '.' . VentaProductoModel::VENTA_ID, '=', $tabla_venta . '.' . VentasRealizadasModel::ID);
        $query->join($tabla_producto, $tabla_producto . '.' . ProductosModel::ID, '=', $tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID);
        $query->where($tabla_venta . '.' . VentasRealizadasModel::VENDEDOR_ID, $user_id);
        $query->groupBy(
            $tabla_venta_producto . '.' . VentaProductoModel::CANTIDAD,
            $tabla_venta_producto . '.' . VentaProductoModel::VALOR_UNITARIO,
            $tabla_producto . '.' . ProductosModel::DESCRIPCION,
            $tabla_producto . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            $tabla_producto . '.' . ProductosModel::PREFIJO,
            $tabla_producto . '.' . ProductosModel::SUFIJO,
            $tabla_producto . '.' . ProductosModel::BASICO
        );
        return $query->get();
    }

    //archivo que se envia a ford mensualmente
    public function generarlayoutford($accion = "inventario")
    {
        $folder = 'layout_ford';
        switch ($accion) {
            case 'inventario':
                $ventas = $this->getVentasReporte6MesesAtras();
                $file_name = 'vip_inv_mex_' . date('Ymd') . ".txt";
                $this->servicioManejoArchivos->setDirectory("$folder");
                $storage = Storage::disk('local');

                $ventas_meses = $this->getVentasAgrupadasMes([VentasRealizadasModel::CANTIDAD_MESES => 6, VentaProductoModel::PRODUCTO_ID => 3, VentasRealizadasModel::YEAR => date('Y')]);

                $storage->prepend($file_name, "HDRDEALERSA003489VIPSA211116193407"); //Encabezado

                foreach ($ventas as $key => $producto) {
                    if ($key <= 10) {
                        $descripcion = str_replace(' ', '-', trim($producto->descripcion));
                        $prefijo = $producto->prefijo;
                        $no_pieza = $producto->no_identificacion;
                        $basico = $producto->basico;
                        $sufijo = $producto->sufijo;
                        $producto_id = $producto->id;
                        $ubicacion_producto = $producto->ubicacion;

                        $precio_costo = $producto->precio_factura;
                        $precio_publico = $producto->valor_unitario;

                        // dd($ubicacion);
                        // $detalle = $this->getVentasByProducto($producto_id);
                        $ventas_meses = $this->getVentasAgrupadasMes([VentasRealizadasModel::CANTIDAD_MESES => 6, VentaProductoModel::PRODUCTO_ID => $producto_id, VentasRealizadasModel::YEAR => date('Y')]);
                        dd($ventas_meses);
                        //6 meses atras de fecha actual
                        $mes1 = !empty($ventas_meses[5]->venta_total) ? $ventas_meses[5]->venta_total : 0;
                        $mes2 = !empty($ventas_meses[4]->venta_total) ? $ventas_meses[4]->venta_total : 0;
                        $mes3 = !empty($ventas_meses[3]->venta_total) ? $ventas_meses[3]->venta_total : 0;
                        $mes4 = !empty($ventas_meses[2]->venta_total) ? $ventas_meses[2]->venta_total : 0;
                        $mes5 = !empty($ventas_meses[1]->venta_total) ? $ventas_meses[1]->venta_total : 0;
                        $mes6 = !empty($ventas_meses[0]->venta_total) ? $ventas_meses[0]->venta_total : 0;

                        $compra = $this->servicioOrdenCompra->ultimacompraByProducto($producto->id);
                        $ultima_compra_pieza = $compra->created_at ? date('y-m-d', $compra->created_at) : '';

                        $existencia_producto = 0;

                        $strLine = "$no_pieza  $prefijo  $basico $sufijo $descripcion $ubicacion_producto $ultima_compra_pieza $mes1 $mes2 $mes3 $mes4 $mes5 $mes6  $precio_costo $existencia_producto $precio_publico ";
                        dd($strLine);
                    } else {
                        break;
                    }
                }
                break;

            default:
                # code...
                break;
        }


        $storage->prepend($file_name, "TRLDEALERSA0034890000000071"); //Fin de archivo

        return $file_name;
    }

    //FILTRO POR PRODUCTO
    public function getVentasByProducto($producto_id, $meses = '1')
    {

        $tableVentas = VentasRealizadasModel::getTableName();
        $tabla_venta_producto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();

        $query = $this->modelo
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->join($tabla_venta_producto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tabla_venta_producto . '.' . VentaProductoModel::VENTA_ID)
            ->select(
                DB::raw('SUM( venta_producto.venta_total ) AS venta_total'),
                DB::raw('SUM ( venta_producto.cantidad ) AS cantidad')
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->where($tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $producto_id);
        $query->whereMonth($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, '=',  date("m", strtotime(date('Y-m-d') . "- $meses month")));
        return $query->first();
    }

    //TODAS LAS VENTAS
    public function getVentasReporte6MesesAtras()
    {
        $fecha_desde = date('Y-m-d');
        $fecha_hasta = date("Y-m-d", strtotime(date('Y-m-d') . "- 6 month"));

        $tableVentas = VentasRealizadasModel::getTableName();
        $tabla_venta_producto = VentaProductoModel::getTableName();
        $tableReVentaEstatus = ReVentasEstatusModel::getTableName();
        $tabla_producto = ProductosModel::getTableName();
        $ubicacion_producto = CatalogoUbicacionProductoModel::getTableName();

        $query = $this->modelo
            ->join($tabla_venta_producto, $tableVentas . '.' . VentasRealizadasModel::ID, '=', $tabla_venta_producto . '.' . VentaProductoModel::VENTA_ID)
            ->join($tabla_producto,  $tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID, '=', $tabla_producto . '.' . ProductosModel::ID)
            ->join($ubicacion_producto,  $tabla_producto . '.' . ProductosModel::UBICACION_PRODUCTO_ID, '=', $ubicacion_producto . '.' . CatalogoUbicacionProductoModel::ID)
            ->join($tableReVentaEstatus, $tableReVentaEstatus . '.' . ReVentasEstatusModel::VENTA_ID, '=', $tableVentas . '.' . VentasRealizadasModel::ID)
            ->select(
                $tabla_producto . '.' . ProductosModel::ID,
                $tabla_producto . '.' . ProductosModel::DESCRIPCION,
                $tabla_producto . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                $tabla_producto . '.' . ProductosModel::PREFIJO,
                $tabla_producto . '.' . ProductosModel::SUFIJO,
                $tabla_producto . '.' . ProductosModel::BASICO,
                $ubicacion_producto . '.' . CatalogoUbicacionProductoModel::NOMBRE . ' as ubicacion'
            );

        $query->whereIn($tableReVentaEstatus . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID, array(EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA));
        $query->where($tableReVentaEstatus . '.' . ReVentasEstatusModel::ACTIVO, true);
        $query->whereBetween($tableVentas . '.' . VentasRealizadasModel::CREATED_AT, [$fecha_desde, $fecha_hasta]);
        $query->groupBy(
            $tabla_producto . '.' . ProductosModel::ID,
            $tabla_venta_producto . '.' . VentaProductoModel::PRODUCTO_ID,
            $ubicacion_producto . '.' . CatalogoUbicacionProductoModel::NOMBRE
        );
        return $query->get();
    }
}
