<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Refacciones\OperacionesPiezasModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\VendedorModel;
use Illuminate\Support\Facades\DB;

class ServicioOperacionesPiezas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'vendedor';
        $this->modelo = new OperacionesPiezasModel();
    }

    public function getReglasGuardar()
    {
        return [
            OperacionesPiezasModel::TOTAL => 'required',
            OperacionesPiezasModel::NO_ORDEN => 'required',
            'precio_unitario' => 'required',
            OperacionesPiezasModel::ID_REFACCION => 'required',
            OperacionesPiezasModel::NO_OPERACION => 'required'

        ];
    }
    public function getReglasUpdate()
    {
        return [
            OperacionesPiezasModel::TOTAL => 'required',
            OperacionesPiezasModel::NO_ORDEN => 'required',
            'precio_unitario' => 'required',
            OperacionesPiezasModel::ID_REFACCION => 'required',
            OperacionesPiezasModel::NO_OPERACION => 'required'
        ];
    }

    public function validateOrSave($parametros)
    {
        $tabla_operaciones = OperacionesPiezasModel::getTableName();
        $query = $this->modelo->select(
            DB::raw('sum(' . $tabla_operaciones . '.' . OperacionesPiezasModel::TOTAL . ') as total_refacciones')
        )->from($tabla_operaciones)
            ->where(OperacionesPiezasModel::NO_ORDEN, '=', $parametros[OperacionesPiezasModel::NO_ORDEN])
            ->where(OperacionesPiezasModel::NO_OPERACION, '=', $parametros[OperacionesPiezasModel::NO_OPERACION])
            ->first();

        $actual_total = !empty($query->total_refacciones) ? floatval($query->total_refacciones) : 0;
        // $nuevo_total  = $actual_total + $parametros[OperacionesPiezasModel::TOTAL];
        // if ($nuevo_total > floatval($parametros['precio_unitario'])) {
        //     throw new ParametroHttpInvalidoException([
        //         'validacion_msg' => "La suma de productos excede el valor"
        //     ]);
        // } else {
        // }
        $this->crear($parametros);
    }
}
