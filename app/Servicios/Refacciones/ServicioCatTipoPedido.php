<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\TipoPedidoModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatTipoPedido extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'tipo pedido';
        $this->modelo = new TipoPedidoModel();
    }

    public function getReglasGuardar()
    {
        return [
            TipoPedidoModel::NOMBRE => 'required',
            TipoPedidoModel::CLAVE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            TipoPedidoModel::NOMBRE => 'required',
            TipoPedidoModel::CLAVE => 'required'
        ];
    }
}
