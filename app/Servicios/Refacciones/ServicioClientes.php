<?php

namespace App\Servicios\Refacciones;

use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\Refacciones\ClientesModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatalogoClaveClienteModel;
use App\Models\Refacciones\ClaveClienteModel;
use App\Models\Refacciones\ClienteVehiculosModel;
use App\Models\Usuarios\User;
use App\Servicios\CuentasPorCobrar\ServicioPlazoCredito;
use DB;
class ServicioClientes extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'clientes';
        $this->modelo = new ClientesModel();
        $this->modeloClave = new ClaveClienteModel();
        $this->modeloVehiculo = new ClienteVehiculosModel();

        $this->servicioClave = new ServicioClaveCliente();
        $this->servicioCatalogoClave = new ServicioCatalogoClaveCliente();

        $this->servicioCatContactos = new ServicioContacto();
        $this->servicioCatTipoPago = new ServicioCatTipoPago();
        $this->servicioCatalogoCfdi = new ServicioCatalogoCfdi();
        $this->servicioCatVehiculo = new ServicioClienteVehiculos();
        $this->servicioPlazoCredito = new ServicioPlazoCredito();
    }

    public function getReglasGuardar()
    {
        return [
            ClientesModel::NUMERO_CLIENTE => 'nullable',
            ClientesModel::TIPO_REGISTRO => 'nullable',
            ClientesModel::NOMBRE => 'nullable',
            ClientesModel::APELLIDO_MATERNO => 'nullable',
            ClientesModel::APELLIDO_PATERNO => 'nullable',
            ClientesModel::REGIMEN_FISCAL => 'nullable',
            ClientesModel::NOMBRE_EMPRESA => 'nullable',
            ClientesModel::RFC => 'nullable',
            ClientesModel::DIRECCION => 'nullable',
            ClientesModel::NUMERO_INT => 'nullable',
            ClientesModel::NUMERO_EXT => 'nullable',
            ClientesModel::COLONIA => 'nullable',
            ClientesModel::MUNICIPIO => 'nullable',
            ClientesModel::ESTADO => 'nullable',
            ClientesModel::PAIS => 'nullable',
            ClientesModel::CODIGO_POSTAL => 'nullable',
            ClientesModel::TELEFONO => 'nullable',
            ClientesModel::TELEFONO_2 => 'nullable',
            ClientesModel::TELEFONO_3 => 'nullable',
            ClientesModel::FLOTILLERO => 'nullable|numeric',
            ClientesModel::CORREO_ELECTRONICO => 'nullable',
            ClientesModel::CORREO_ELECTRONICO_2 => 'nullable',
            ClientesModel::FECHA_NACIMIENTO => 'nullable|date',
            ClientesModel::CFDI_ID => 'nullable|exists:catalogo_cfdi,id',
            ClientesModel::METODO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            ClientesModel::FORMA_PAGO => 'nullable|string',
            ClientesModel::SALDO => 'nullable',
            ClientesModel::LIMITE_CREDITO => 'nullable',
            ClientesModel::NOTAS => 'nullable',
            ClientesModel::ES_CLIENTE => 'nullable'

        ];
    }

    public function getReglasUpdate()
    {
        return [
            ClientesModel::NUMERO_CLIENTE => 'required',
            ClientesModel::TIPO_REGISTRO => 'required',
            ClientesModel::NOMBRE => 'required',
            ClientesModel::APELLIDO_MATERNO => 'required',
            ClientesModel::APELLIDO_PATERNO => 'required',
            ClientesModel::REGIMEN_FISCAL => 'required',
            ClientesModel::NOMBRE_EMPRESA => 'required',
            ClientesModel::RFC => 'required',
            ClientesModel::DIRECCION => 'required',
            ClientesModel::NUMERO_INT => 'required',
            ClientesModel::NUMERO_EXT => 'required',
            ClientesModel::COLONIA => 'required',
            ClientesModel::MUNICIPIO => 'required',
            ClientesModel::ESTADO => 'required',
            ClientesModel::PAIS => 'required',
            ClientesModel::CODIGO_POSTAL => 'required',
            ClientesModel::TELEFONO => 'required',
            ClientesModel::TELEFONO_2 => 'nullable',
            ClientesModel::TELEFONO_3 => 'nullable',
            ClientesModel::FLOTILLERO => 'nullable|numeric',
            ClientesModel::CORREO_ELECTRONICO => 'required',
            ClientesModel::CORREO_ELECTRONICO_2 => 'nullable',
            ClientesModel::FECHA_NACIMIENTO => 'required|date',
            ClientesModel::CFDI_ID => 'nullable|exists:catalogo_cfdi,id',
            ClientesModel::METODO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            ClientesModel::FORMA_PAGO => 'nullable|string',
            ClientesModel::SALDO => 'nullable',
            ClientesModel::LIMITE_CREDITO => 'nullable',
            ClientesModel::NOTAS => 'nullable'
        ];
    }

    public function getAll()
    {
        $data = $this->buscarTodos();
        foreach ($data as $key => $item) {
            $data[$key]['cfdi'] = isset($item[ClientesModel::CFDI_ID]) ? $this->servicioCatalogoCfdi->getById($item[ClientesModel::CFDI_ID]) : [];
            $data[$key]['metodo_pago'] = isset($item[ClientesModel::METODO_PAGO_ID]) ?  $this->servicioCatTipoPago->getById($item[ClientesModel::METODO_PAGO_ID]) : [];
            $data[$key]['plazo_credito'] = isset($item[ClientesModel::PLAZO_CREDITO_ID]) ?  $this->servicioPlazoCredito->getById($item[ClientesModel::PLAZO_CREDITO_ID]) : [];
        }
        return $data;
    }

    public function getCatalogo()
    {
        return $this->modelo->select(
            ClientesModel::ID,
            DB::raw("CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre_cliente"),
            DB::raw("CONCAT(direccion,' ',numero_int,' ',colonia,' ',municipio,' ',estado,' ',pais) as direccion_completa"),
            ClientesModel::TELEFONO,
        )->get();
    }


    public function getOneCliente($cliente_id)
    {
        $data = $this->modelo->where(ClientesModel::ID, $cliente_id)->get();
        foreach ($data as $key => $item) {
            $data[$key]['cfdi'] = isset($item[ClientesModel::CFDI_ID]) ? $this->servicioCatalogoCfdi->getById($item[ClientesModel::CFDI_ID]) : [];
            $data[$key]['metodo_pago'] = isset($item[ClientesModel::METODO_PAGO_ID]) ?  $this->servicioCatTipoPago->getById($item[ClientesModel::METODO_PAGO_ID]) : [];
        }
        return $data;
    }

    public function lastRecord()
    {
        return $this->modelo
            ->orderBy(ClientesModel::ID, 'desc')
            ->first();
    }

    public function getReglasBusquedaNombre()
    {
        return [
            ClientesModel::NOMBRE => 'required'
        ];
    }

    public function getReglasClienteId()
    {
        return [
            ClientesModel::ID => 'required|numeric|exists:clientes,id'
        ];
    }

    public function searchNombreCliente($parametros)
    {
        //Buscamos coincidencias
        $data = $this->modelo->where(ClientesModel::NOMBRE, 'LIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::APELLIDO_MATERNO, 'LIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::APELLIDO_PATERNO, 'LIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::NOMBRE_EMPRESA, 'LIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::NUMERO_CLIENTE, 'LIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->get();

        foreach ($data as $key => $item) {
            $data[$key]['contactos'] = isset($item[ClientesModel::ID]) ? $this->servicioCatContactos->contactosByClienteId($item[ClientesModel::ID]) : [];

            if (count($data[$key]['contactos']) == 0) {
                $data[$key]['contactos'] = array(
                    'contacto_nombre' => $item[ClientesModel::NOMBRE],
                    'contacto_apellido_paterno' => $item[ClientesModel::APELLIDO_MATERNO],
                    'contacto_apellido_materno' => $item[ClientesModel::APELLIDO_PATERNO],
                    'contacto_telefono' => $item[ClientesModel::TELEFONO],
                    'contacto_correo' => $item[ClientesModel::CORREO_ELECTRONICO]
                );
            }

            $data[$key]["vehiculo"] = isset($item[ClientesModel::ID]) ?  $this->servicioCatVehiculo->getVehiculosCliente($item[ClientesModel::ID]) : [];
        }

        return $data;
    }

    public function getReglasBusquedaCliente()
    {
        return [
            ClientesModel::NOMBRE_EMPRESA => 'required',
            ClientesModel::CORREO_ELECTRONICO => 'nullable',
            ClientesModel::CORREO_ELECTRONICO_2 => 'nullable'
        ];
    }

    public function searchCliente($request)
    {
        $data = $this->modelo->where(ClientesModel::NOMBRE_EMPRESA, 'LIKE', $request->get(ClientesModel::NOMBRE_EMPRESA))
            ->orWhere(ClientesModel::CORREO_ELECTRONICO, 'LIKE', $request->get(ClientesModel::CORREO_ELECTRONICO))
            ->orWhere(ClientesModel::CORREO_ELECTRONICO, 'LIKE', $request->get(ClientesModel::CORREO_ELECTRONICO_2))

            ->orWhere(ClientesModel::CORREO_ELECTRONICO_2, 'LIKE', $request->get(ClientesModel::CORREO_ELECTRONICO))
            ->orWhere(ClientesModel::CORREO_ELECTRONICO_2, 'LIKE', $request->get(ClientesModel::CORREO_ELECTRONICO_2))

            ->select(ClientesModel::ID, ClientesModel::NUMERO_CLIENTE, ClientesModel::NOMBRE, ClientesModel::APELLIDO_MATERNO, ClientesModel::APELLIDO_PATERNO, ClientesModel::NOMBRE_EMPRESA)
            ->orderBy(ClientesModel::ID, 'desc')
            ->limit(1)
            ->get();
        //dd($data);
        return $data;
    }

    public function getReglasCliente()
    {
        return [
            ClientesModel::NUMERO_CLIENTE => 'required'
        ];
    }

    public function searchNumeroCliente($parametros)
    {
        $data = $this->modelo->where(ClientesModel::NUMERO_CLIENTE, '=', $parametros[ClientesModel::NUMERO_CLIENTE])
            ->select(ClientesModel::ID)
            ->orderBy(ClientesModel::ID, 'desc')
            ->limit(1)
            ->get();
        return $data;
    }

    public function tieneCredito($parametros)
    {
        return $this->modelo->select(
            ClientesModel::getTableName() . '.'. ClientesModel::APLICA_CREDITO, 
            ClientesModel::getTableName() . '.'. ClientesModel::PLAZO_CREDITO_ID, 
            ClientesModel::getTableName() . '.'. ClientesModel::LIMITE_CREDITO, 
            PlazoCreditoModel::getTableName() . '.'.PlazoCreditoModel::CANTIDAD_MES,
            PlazoCreditoModel::getTableName() . '.'.PlazoCreditoModel::NOMBRE . ' as plazo_credito'
        )
        ->leftJoin(
            PlazoCreditoModel::getTableName(),
            PlazoCreditoModel::getTableName() . '.' . PlazoCreditoModel::ID,
            '=',
            ClientesModel::getTableName() . '.' . ClientesModel::PLAZO_CREDITO_ID
        )
        ->where(ClientesModel::getTableName() . '.'. ClientesModel::ID, $parametros[ClientesModel::ID])
        ->first();
    }

    public function clientePorClave($parametros)
    {
        return $this->modelo->select(ClientesModel::getTableName() . '.*')
            ->join(
                ClaveClienteModel::getTableName(),
                ClaveClienteModel::getTableName() . '.' . ClaveClienteModel::CLIENTE_ID,
                '=',
                ClientesModel::getTableName() . '.' . ClientesModel::ID
            )
            ->join(
                CatalogoClaveClienteModel::getTableName(),
                ClaveClienteModel::getTableName() . '.' . ClaveClienteModel::CLAVE_ID,
                '=',
                CatalogoClaveClienteModel::getTableName() . '.' . CatalogoClaveClienteModel::ID
            )
            ->where(CatalogoClaveClienteModel::CLAVE, $parametros[CatalogoClaveClienteModel::CLAVE])
            ->get();
    }

    public function getReglasClaveCliente()
    {
        return [
            CatalogoClaveClienteModel::CLAVE => 'required|exists:' . CatalogoClaveClienteModel::getTableName() . ',clave'
        ];
    }
    public function getReglasUpdateAplicaCredito()
    {
        return [
            ClientesModel::APLICA_CREDITO => 'required|boolean'
        ];
    }
    public function getReglasUpdatePlazoCredito()
    {
        return [
            ClientesModel::LIMITE_CREDITO => 'required|numeric',
            ClientesModel::PLAZO_CREDITO_ID => 'required|numeric|exists:plazos_credito,id'
        ];
    }


    public function updateAplicaCredito($data, $id)
    {
        $modelo = $this->modelo->find($id);
        if (!$data[ClientesModel::APLICA_CREDITO]) {
            $modelo->plazo_credito_id = null;
        }
        $modelo->aplica_credito = $data[ClientesModel::APLICA_CREDITO];
        return $modelo->save();
    }

    public function updatePlazoCredito($data, $id)
    {
        $modelo = $this->modelo->find($id);
        $modelo->limite_credito = $data[ClientesModel::LIMITE_CREDITO];
        $modelo->aplica_credito = $data[ClientesModel::APLICA_CREDITO];
        $modelo->plazo_credito_id = $data[ClientesModel::PLAZO_CREDITO_ID];
        return $modelo->save();
    }

    public function getClientesAutosPreventa($parametros)
    {
        $tabla_venta_autos = VentasAutosModel::getTableName();
        $tabla_clientes = ClientesModel::getTableName();
        $tabla_remision = RemisionModel::getTableName();
        $tabla_vendedor = User::getTableName();
        $tabla_estatus_venta = EstatusVentaAutosModel::getTableName();

        $query = $this->modelo->select(
            $tabla_clientes . '.*',
            $tabla_remision . '.' . RemisionModel::UNIDAD_DESCRIPCION,
            $tabla_remision . '.' . RemisionModel::ECONOMICO,
            $tabla_vendedor . '.' . User::NOMBRE . ' as nombre_vendedor',
            $tabla_estatus_venta . '.' . EstatusVentaAutosModel::NOMBRE . ' as estatus_venta',
        )
            ->leftJoin(
                VentasAutosModel::getTableName(),
                VentasAutosModel::getTableName() . '.' . VentasAutosModel::ID,
                '=',
                ClientesModel::getTableName() . '.' . ClientesModel::ID
            )->leftJoin(
                RemisionModel::getTableName(),
                RemisionModel::getTableName() . '.' . RemisionModel::ID,
                '=',
                VentasAutosModel::getTableName() . '.' . VentasAutosModel::ID_UNIDAD
            )->leftJoin(
                $tabla_vendedor,
                $tabla_vendedor . '.' . User::ID,
                '=',
                VentasAutosModel::getTableName() . '.' . VentasAutosModel::ID_ASESOR
            )->leftJoin(
                $tabla_estatus_venta,
                $tabla_estatus_venta . '.' . EstatusVentaAutosModel::ID,
                '=',
                VentasAutosModel::getTableName() . '.' . VentasAutosModel::ID_ESTATUS
            );;

        $query->whereIn(VentasAutosModel::ID_ESTATUS, [EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO]);
        $query->where(VentasAutosModel::TIPO_VENTA, TipoFormaPagoModel::FORMA_CREDITO);

        if (isset($parametros[VentasAutosModel::ID])) {
            $query->where($tabla_venta_autos . '.' . VentasAutosModel::ID, $parametros[VentasAutosModel::ID]);
        }

        return $query->get();
    }
}
