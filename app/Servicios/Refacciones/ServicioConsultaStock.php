<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\VentaProductoModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\Almacenes as AlmacenModel;
use App\Models\Refacciones\ListaProductosOrdenCompraModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ReComprasEstatusModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\TraspasoProductoAlmacenModel;
use App\Models\Refacciones\ReEstatusTraspasoModel;
use App\Models\Refacciones\CantidadProductosInicialModel;
use Illuminate\Support\Facades\DB;

class ServicioConsultaStock extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'consulta stock';
        $this->productoOrdenCompra = new ListaProductosOrdenCompraModel();
        $this->modeloTraspasoProducto = new TraspasoProductoAlmacenModel();
        $this->modeloCantidadProductosInicial = new CantidadProductosInicialModel();
    }

    public function getCantidadProductosInicial($request) {
        return $this->modeloCantidadProductosInicial->where(CantidadProductosInicialModel::PRODUCTO_ID,$request['producto_id'])->first();
    }

    public function getComprasStock($request)
    {
        $estatus_permitidos = isset($request['compra_realizada']) && $request['compra_realizada'] == true ? [EstatusCompra::ESTATUS_FINALIZADA, EstatusCompra::FACTURA_PAGADA] : [EstatusCompra::ESTATUS_PROCESO, EstatusCompra::ESTATUS_FINALIZADA, EstatusCompra::FACTURA_PAGADA];
        $consulta = $this->productoOrdenCompra
            ->join(
                OrdenCompraModel::getTableName(),
                OrdenCompraModel::getTableName() . "." . OrdenCompraModel::ID,
                '=',
                ListaProductosOrdenCompraModel::getTableName() . "." . ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID
            )
            ->join(
                ReComprasEstatusModel::getTableName(),
                ReComprasEstatusModel::getTableName() . "." . ReComprasEstatusModel::COMPRA_ID,
                '=',
                OrdenCompraModel::getTableName() . "." . OrdenCompraModel::ID
            );

        $consulta->where(ListaProductosOrdenCompraModel::getTableName() . "." . ListaProductosOrdenCompraModel::PRODUCTO_ID, $request['producto_id']);
        $consulta->where(ReComprasEstatusModel::getTableName() . "." . ReComprasEstatusModel::ACTIVO, true);
        $consulta->whereIn(ReComprasEstatusModel::getTableName() . "." . ReComprasEstatusModel::ESTATUS_COMPRA_ID, $estatus_permitidos);
        $consulta->select(
            ListaProductosOrdenCompraModel::getTableName() . "." . ListaProductosOrdenCompraModel::PRODUCTO_ID,
            ReComprasEstatusModel::ESTATUS_COMPRA_ID,
            DB::raw('sum(productos_orden_compra.cantidad) as total_cantidad'),
            DB::raw('sum(productos_orden_compra.precio) as total_precio_unitario'),
            DB::raw('sum(productos_orden_compra.total) as total_precio')
        );
        $consulta->groupBy(
            ListaProductosOrdenCompraModel::getTableName() . "." . ListaProductosOrdenCompraModel::PRODUCTO_ID,
            ReComprasEstatusModel::ESTATUS_COMPRA_ID
        );
        return $consulta->first();
    }

    public function getVentasProductoByIdProducto($respuesta)
    {
        $estatus_permitidos = [EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA];
        $ventasAlmacen = DB::table(VentaProductoModel::getTableName())
            ->join(
                VentasRealizadasModel::getTableName(),
                VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ID,
                '=',
                VentaProductoModel::getTableName() . "." . VentaProductoModel::VENTA_ID
            )
            ->join(
                ReVentasEstatusModel::getTableName(),
                ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::VENTA_ID,
                '=',
                VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ID
            )
            ->join(
                AlmacenModel::getTableName(),
                AlmacenModel::getTableName() . "." . AlmacenModel::ID,
                '=',
                VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ALMACEN_ID
            );
        $ventasAlmacen->where(VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID, $respuesta['producto_id']);
        $ventasAlmacen->where(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ACTIVO, true);
        $ventasAlmacen->whereIn(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ESTATUS_VENTA_ID, $estatus_permitidos);
        if (isset($respuesta['venta_id'])) {
            $ventasAlmacen->orWhere(VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ID, $respuesta['venta_id']);
            $ventasAlmacen->where(VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID, $respuesta['producto_id']);
            $ventasAlmacen->where(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ACTIVO, true);
            $ventasAlmacen->whereIn(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ESTATUS_VENTA_ID, [EstatusVentaModel::ESTATUS_PROCESO]);
        }
        $ventasAlmacen->select(
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
            ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
            VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::ALMACEN_ID,
            AlmacenModel::getTableName() . '.' . AlmacenModel::NOMBRE . ' as Almacen',
            DB::raw('sum(venta_producto.venta_total) as total_precio_venta'),
            DB::raw('sum(venta_producto.cantidad) as total_cantidad_venta'),
            DB::raw('sum(venta_producto.valor_unitario) as total_valor_unitario_venta')
        )
            ->groupBy(
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
                VentasRealizadasModel::getTableName() . '.' . VentasRealizadasModel::ALMACEN_ID,
                AlmacenModel::getTableName() . '.' . AlmacenModel::NOMBRE
            );
        $consulta = $ventasAlmacen->get()->toArray();

        return array_map(function ($item) {
            return (array) $item;
        }, $consulta);
    }

    public function getVentasTotalesProductoId($respuesta)
    {
        $estatus_permitidos = [EstatusVentaModel::ESTATUS_FINALIZADA, EstatusVentaModel::FACTURA_PAGADA, EstatusVentaModel::ESTATUS_APARTADO];
        $ventas = DB::table(VentaProductoModel::getTableName())
            ->join(
                VentasRealizadasModel::getTableName(),
                VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ID,
                '=',
                VentaProductoModel::getTableName() . "." . VentaProductoModel::VENTA_ID
            )
            ->join(
                ReVentasEstatusModel::getTableName(),
                ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::VENTA_ID,
                '=',
                VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ID
            );
           
            $ventas->where(VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID, $respuesta['producto_id']);
            $ventas->where(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ACTIVO, true);
            $ventas->whereIn(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ESTATUS_VENTA_ID, $estatus_permitidos);
            if (isset($respuesta['venta_id'])){
                $ventas->orWhere(VentasRealizadasModel::getTableName() . "." . VentasRealizadasModel::ID, $respuesta['venta_id']);
                $ventas->where(VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID, $respuesta['producto_id']);
                $ventas->where(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ACTIVO, true);
                $ventas->whereIn(ReVentasEstatusModel::getTableName() . "." . ReVentasEstatusModel::ESTATUS_VENTA_ID, [EstatusVentaModel::ESTATUS_PROCESO]);
            }
            $ventas->select(
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
                DB::raw('sum(venta_producto.venta_total) as total_precio_venta'),
                DB::raw('sum(venta_producto.cantidad) as total_cantidad_venta'),
                DB::raw('sum(venta_producto.valor_unitario) as total_valor_unitario_venta')
            )
            ->groupBy(
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID
            );
            return $ventas->first();
    }

    public function sumaProductosTraspasadosAlmacen($producto_id, $almacen_id)
    {
        return $this->modeloTraspasoProducto
                    ->select(
                        DB::raw('sum('.TraspasoProductoAlmacenModel::CANTIDAD.') as total_cantidad')
                    )
                    ->join(
                        ReEstatusTraspasoModel::getTableName(),
                        ReEstatusTraspasoModel::getTableName() . "." . ReEstatusTraspasoModel::TRASPASO_ID,
                        '=',
                        TraspasoProductoAlmacenModel::getTableName() . "." . TraspasoProductoAlmacenModel::TRASPASO_ID
                    )
                    ->where(TraspasoProductoAlmacenModel::PRODUCTO_ID, $producto_id)
                    ->where(TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID, $almacen_id)
                    ->where(ReEstatusTraspasoModel::getTableName() . "." .ReEstatusTraspasoModel::ESTATUS_ID, 1)
                    ->where(ReEstatusTraspasoModel::getTableName() . "." .ReEstatusTraspasoModel::ACTIVO, true)
                    ->first();
    }
}
