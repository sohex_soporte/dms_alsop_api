<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Support\Facades\DB;

class ServicioFolios extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'folios';
        $this->modelo = new FoliosModel();
        $this->servicioOrdenCompra = new ServicioOrdenCompra();
        $this->modeloVentas = new VentasRealizadasModel();
        $this->modeloTrasPasos = new Traspasos();
    }

    public function getReglasGuardar()
    {
        return [
            FoliosModel::FOLIO => 'required',
            FoliosModel::STATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            FoliosModel::FOLIO => 'required',
            FoliosModel::STATUS => 'required'
        ];
    }

    public function generarFolio($tipo_proceso_id)
    {
        if (empty($tipo_proceso_id)) {
            throw new ParametroHttpInvalidoException([
                'tipo_proceso_id' => __(self::$E0016_MISSING_PARAMS, ["parametro" => $tipo_proceso_id])
            ]);
        }
        $folio = $this->crear([
            FoliosModel::TIPO_PROCESO_ID => $tipo_proceso_id
        ]);

        $modelo = $this->modelo->find($folio->id);
        $modelo->folio = 'DMS-' . $folio->id;
        $modelo->save();
        return $modelo;
    }

    public function getByFolio($folio)
    {
        // CatalogoCuentasModel::
        $table_folios = FoliosModel::getTableName();
        $table_cuentas = CatalogoCuentasModel::getTableName();
        $folios = DB::table($table_folios)
            ->select(
                $table_folios . "." . FoliosModel::ID,
                $table_folios . "." . FoliosModel::FOLIO,
                $table_folios . "." . FoliosModel::TIPO_PROCESO_ID,
                $table_folios . "." . FoliosModel::STATUS,
                $table_folios . "." . FoliosModel::CREATED_AT,
                $table_cuentas . "." . CatalogoCuentasModel::NOMBRE_CUENTA . ' as nombre_cuenta'
            )
            ->join($table_cuentas, $table_cuentas . '.' . CatalogoCuentasModel::ID, '=', $table_folios . '.' . FoliosModel::TIPO_PROCESO_ID)
            ->where(FoliosModel::FOLIO, $folio)->get();

        $newArray = [];
        foreach ($folios as $key => $item) {
            switch ($item->tipo_proceso_id) {
                case CatalogoProcesosModel::PROCESO_SERVICIOS:
                    $newArray[$key]['id'] = $item->id;
                    $newArray[$key]['folio'] = $item->folio;
                    $newArray[$key]['nombre_cuenta'] = $item->nombre_cuenta;
                    $newArray[$key]['status'] = $item->status;
                    $newArray[$key]['tipo_proceso_id'] = $item->tipo_proceso_id;
                    $newArray[$key]['created_at'] = $item->created_at;
                    $newArray[$key]['compras'] = $this->getDataCompras($item->id);
                    break;
                case CatalogoProcesosModel::PROCESO_VENTA_MOSTRADOR:
                    $newArray[$key]['id'] = $item->id;
                    $newArray[$key]['folio'] = $item->folio;
                    $newArray[$key]['tipo_proceso_id'] = $item->tipo_proceso_id;
                    $newArray[$key]['nombre_cuenta'] = $item->nombre_cuenta;
                    $newArray[$key]['status'] = $item->status;
                    $newArray[$key]['created_at'] = $item->created_at;
                    $newArray[$key]['ventas'] = $this->getDataVentas($item->id);
                    break;
                case CatalogoProcesosModel::PROCESO_TRASPASO:
                    $newArray[$key]['id'] = $item->id;
                    $newArray[$key]['folio'] = $item->folio;
                    $newArray[$key]['tipo_proceso_id'] = $item->tipo_proceso_id;
                    $newArray[$key]['nombre_cuenta'] = $item->nombre_cuenta;
                    $newArray[$key]['status'] = $item->status;
                    $newArray[$key]['created_at'] = $item->created_at;
                    $newArray[$key]['traspaso'] = $this->getDataTraspaso($item->id);
                    break;
                default:
                    break;
            }
        }

        return $newArray;
    }

    public function getDataCompras($folio_id)
    {
        return $this->servicioOrdenCompra->getOrdenCompraByFolio($folio_id);
    }

    public function getDataVentas($folio_id)
    {
        return $this->modeloVentas->where(VentasRealizadasModel::FOLIO_ID, $folio_id)->get();
    }

    public function getDataTraspaso($folio_id)
    {
        return $this->modeloTrasPasos->where(Traspasos::FOLIO_ID, $folio_id)->get();
    }
}
