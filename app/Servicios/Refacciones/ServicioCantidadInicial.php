<?php

namespace App\Servicios\Refacciones;
use App\Models\Refacciones\CantidadProductosInicialModel;
use App\Models\Refacciones\ProductosModel;

use App\Servicios\Core\ServicioDB;

class ServicioCantidadInicial extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'clientes';
        $this->modelo = new CantidadProductosInicialModel();
    }


    public function getCantidadProducto($parametros)
    {
        $tabla_cantidad_inicial = CantidadProductosInicialModel::getTableName();
        $tabla_productos = ProductosModel::getTableName();

        $query = $this->modelo->select(
            $tabla_cantidad_inicial . '.*',
            $tabla_productos . '.' . ProductosModel::VALOR_UNITARIO,
        )
            ->join(
                ProductosModel::getTableName(),
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                '=',
                CantidadProductosInicialModel::getTableName() . '.' . CantidadProductosInicialModel::PRODUCTO_ID
            );

        $query->where(CantidadProductosInicialModel::PRODUCTO_ID, $parametros[ProductosModel::ID]);



        return $query->get();
    }
}
