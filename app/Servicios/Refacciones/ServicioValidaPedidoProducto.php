<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\MaProductoPedidoModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Servicios\Core\ServicioDB;

class ServicioValidaPedidoProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'validar';
        $this->modelo = new ProductosPedidosModel();
        $this->maProductoPedidoModel = new MaProductoPedidoModel();
    }

    public function validarProductoPedido($parametros)
    {
        $query = $this->modelo->select(
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_SOLICITADA,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_BACKORDER,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_CARGADA,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::MA_PEDIDO_ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PROVEEDOR_ID,
            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA
        )
            ->join(
                ProductosModel::getTableName(),
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID
            );

        $query->whereRaw('(producto_pedidos.cantidad_solicitada > producto_pedidos.cantidad_cargada or producto_pedidos.cantidad_cargada is null)');
        $query->where(ProductosPedidosModel::PRODUCTO_ID, '=', $parametros[ProductosPedidosModel::PRODUCTO_ID]);
        $query->where(ProductosPedidosModel::PROVEEDOR_ID, '=', $parametros[ProductosPedidosModel::PROVEEDOR_ID]);
        return $query->get()->first();
    }

    public function pedidofinalizado()
    {
        $query = $this->maProductoPedidoModel->select(
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID,
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::AUTORIZADO,
            MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::FINALIZADO,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::MA_PEDIDO_ID
        )
            ->join(
                ProductosPedidosModel::getTableName(),
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::MA_PEDIDO_ID,
                MaProductoPedidoModel::getTableName() . '.' . MaProductoPedidoModel::ID
            );

        $query->where(ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID, '!=', ProductosPedidosModel::ESTATUS_STOCK); 

        if ($query->count() == 0) {
            $this->updateEstatusPedido();
        }
    }

    public function updateEstatusPedido()
    {
            $modelo_pedido = $this->maProductoPedidoModel
                ->orderBy(MaProductoPedidoModel::ID, 'desc')
                ->get()
                ->first();

            $modelo = $this->maProductoPedidoModel->findOrFail($modelo_pedido->id);
            return $modelo->where(MaProductoPedidoModel::ID, $modelo_pedido->id)
                ->update([MaProductoPedidoModel::FINALIZADO => 1]);
                
        
    }

    public function updatepedido($parametros)
    {
        $cantidad_factura = intval($parametros['cantidad_factura']);
        $cantidad_cargada = is_null($parametros['cantidad_cargada']) ? $cantidad_factura : $parametros['cantidad_cargada'] + $cantidad_factura;
        $cantidad_solicitada = $parametros['cantidad_solicitada'];

        $back_order = $cantidad_solicitada - $cantidad_cargada;
        $estatus = $back_order > 0 ? ProductosPedidosModel::ESTATUS_BACKORDER : ProductosPedidosModel::ESTATUS_STOCK;

        return $this->massUpdateWhereId(
            ProductosPedidosModel::ID,
            $parametros['producto_pedido_id'],
            [
                'cantidad_cargada' => $cantidad_cargada,
                'cantidad_backorder' => $back_order,
                'estatus_id' => $estatus
            ]
        );
    }
}
