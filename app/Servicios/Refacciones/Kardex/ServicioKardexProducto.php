<?php

namespace App\Servicios\Refacciones\Kardex;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioCantidadInicial;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VentasRealizadasModel;

class ServicioKardexProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'servicio kardex';
        $this->servicioProductos = new ServicioProductos();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioCantidadInicial = new ServicioCantidadInicial();

    }

    public function getReglasBusquedaProducto()
    {
        return [
            ProductosModel::ID => 'required|numeric',
            ProductosModel::FECHA_RANGO_INICIO => 'nullable|date',
            ProductosModel::FECHA_RANGO_FIN => 'nullable|date',
            VentasRealizadasModel::CLIENTE_ID => 'nullable|numeric',

        ];
    }
    public function getDataProductoKardex($params)
    {

        $cantidad_inicial = $this->servicioCantidadInicial->getCantidadProducto($params);
        $ventas = $this->servicioProductos->getVentasProductoId($params);
        $compras = $this->servicioProductos->getComprasProductoId($params);
        return $this->formarArrayMovimientos($cantidad_inicial, $compras, $ventas);

    }

    public function formarArrayMovimientos($cantidad_inicial, $compras, $ventas)
    {
        $movimientos = [];
        
        foreach ($cantidad_inicial as $inicial) {
            $fecha = str_replace('T','',$inicial->created_at);
            $inicial->tipo = 'inicial';
            $inicial->fecha = $fecha;
            array_push($movimientos, $inicial);
        }
        foreach ($compras as $compra) {
            $compra->tipo = 'compra';
            array_push($movimientos, $compra);
        }

        foreach ($ventas as $venta) {
            $venta->tipo = 'venta';
            array_push($movimientos, $venta);
        }
        return $this->generarTableKardexProducto($movimientos);
    }

    public function generarTableKardexProducto($movimientos)
    {

        $sumCompras = 0; $sumVentas = 0; $stock = 0; $saldo = 0; $saldoCompras = 0; $saldoVentas = 0;
        $listadoKardex = [];

        $convArrayKardex = json_decode(json_encode($movimientos), true);
        usort($convArrayKardex, function ($a, $b) {
            return strcmp($a["fecha"], $b["fecha"]);
        });

        $saldoTotal = 0;
        foreach ($convArrayKardex as $mov) {

            $total = $mov['valor_unitario'] * $mov['cantidad'];
            if ($mov['tipo'] == 'inicial') {
                $stock += $mov['cantidad'];
                $mov['estatus'] = 'INICIAL';
                $mov['cliente'] = '-';
                $mov['folio'] = '-';
                $sumCompras += $mov['cantidad'];
                $saldoCompras += $total;
                $saldoTotal += $total;

                $mov['tipo_movimiento'] = 'entrada';

            }
            if ($mov['tipo'] == 'compra') {
             if ($mov['estatus_id'] == EstatusCompra::ESTATUS_FINALIZADA) {
                $stock += $mov['cantidad'];
                $sumCompras += $mov['cantidad'];
                $saldoCompras += $total;
                $saldoTotal += $total;
                $mov['tipo_movimiento'] = 'entrada';
                $mov['folio'] = $this->getFolio($mov['folio']);
             } else {
                $stock -= $mov['cantidad'];
                $sumCompras -= $mov['cantidad'];
                $saldoCompras -= $total;
                $saldoTotal -= $total;
                $mov['tipo_movimiento'] = 'salida';
                $mov['folio'] = $this->getFolio($mov['folio_devolucion']);
             }
           }
           else if ($mov['tipo'] == 'venta') {
                if ($mov['estatus_id'] == EstatusVentaModel::ESTATUS_FINALIZADA) {
                    $stock -= $mov['cantidad'];
                    $sumVentas += $mov['cantidad'];
                    $saldoVentas += $total;
                    $saldoTotal += $total;
                    $mov['tipo_movimiento'] = 'salida';
                    $mov['folio'] = $this->getFolio($mov['folio']);
                } else {
                    $stock += $mov['cantidad'];
                    $sumVentas -= $mov['cantidad'];
                    $saldoVentas -= $total;
                    $saldoTotal -= $total;
                    $mov['tipo_movimiento'] = 'entrada';
                    $mov['folio'] = $this->getFolio($mov['folio_devolucion']);
                }           
           }
           
           $mov['total'] = $total;
           $mov['totalAcomulado'] = $saldoTotal;
           $mov['existencia'] = $stock;
           array_push($listadoKardex, $mov);
        }


        $totales = [
            'SaldoTotalCompras' => $saldoCompras,
            'SaldoTotalVentas' => $saldoVentas,
            'SaldoTotal' => $saldoCompras + $saldoVentas,
            'CantidadTotalCompras' => $sumCompras,
            'CantidadTotalVentas' => $sumVentas,
            'CantidadTotal' => $sumCompras - $sumVentas,
        ];
        return  [
            'totales' => $totales,
            'kardex' => $listadoKardex
        ];
    }

    function sortFunction( $a, $b ) {
        return strtotime($a["fecha"]) - strtotime($b["fecha"]);
    }

    private function getFolio($folio_id){
        $busquedaFolio = $this->servicioFolios->getById($folio_id);
        return $busquedaFolio ? $busquedaFolio->folio : '';
    }

}
