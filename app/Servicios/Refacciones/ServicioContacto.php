<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatContactos;

class ServicioContacto extends ServicioDB
{

    public function __construct() 
    {
        $this->recurso = 'contactos';
        $this->modelo = new CatContactos();
    }

    public function getReglasGuardar()
    {
        return [
            CatContactos::NOMBRE => 'required',
            CatContactos::APELLIDO_PATERNO => 'required',
            CatContactos::APELLIDO_MATERNO => 'required',
            CatContactos::TELEFONO => 'required',
            CatContactos::TELEFONO_SECUNDARIO => 'required',
            CatContactos::CORREO => 'required|email',
            CatContactos::CORREO_SECUNDARIO => 'nullable|email',
            CatContactos::NOTAS => 'nullable',
            CatContactos::CLIENTE_ID => 'required'

        ];
    }

    public function getReglasUpdate()
    {
        return [
            CatContactos::NOMBRE => 'nullable',
            CatContactos::APELLIDO_PATERNO => 'nullable',
            CatContactos::APELLIDO_MATERNO => 'nullable',
            CatContactos::TELEFONO => 'nullable',
            CatContactos::TELEFONO_SECUNDARIO => 'nullable',
            CatContactos::CORREO => 'nullable|email',
            CatContactos::CORREO_SECUNDARIO => 'nullable|email',
            CatContactos::NOTAS => 'nullable',
            CatContactos::CLIENTE_ID => 'required|exists:cat_contacto,id'
        ];
    }

    public function contactosByClienteId($cliente_id)
    {
        $consulta = $this->modelo->where(CatContactos::getTableName() .'.'. CatContactos::CLIENTE_ID, $cliente_id)->limit(200);
        
        return $consulta->get();
    }
}
