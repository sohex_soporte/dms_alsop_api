<?php

namespace App\Servicios\Soporte;

use App\Models\Soporte\CatStatusTickets;
use App\Models\Soporte\HistorialTicketsModel;
use App\Models\Soporte\TicketsModel;
use App\Servicios\Core\ServicioDB;

class ServicioHistorialTickets extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus tickets';
        $this->modelo = new HistorialTicketsModel();
    }

    public function getReglasGuardar()
    {
        return [
            HistorialTicketsModel::ESTATUS_ANTERIOR_ID => 'required',
            HistorialTicketsModel::ESTATUS_NUEVO_ID => 'required',
            HistorialTicketsModel::USER_ID => 'required',
            HistorialTicketsModel::TICKET_ID => 'required',
            HistorialTicketsModel::COMENTARIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            HistorialTicketsModel::ESTATUS_ANTERIOR_ID => 'required',
            HistorialTicketsModel::ESTATUS_NUEVO_ID => 'required',
            HistorialTicketsModel::USER_ID => 'required',
            HistorialTicketsModel::TICKET_ID => 'required',
            HistorialTicketsModel::COMENTARIO => 'required'
        ];
    }
    public function getAllByTicket($ticket_id){
        $query =  $this->modelo->select(
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::ID,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::COMENTARIO,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::USUARIO,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::CREATED_AT,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::INICIO_ESTATUS,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::FIN_ESTATUS,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::EVIDENCIA,
            'cat_status_tickets1.' . CatStatusTickets::ESTATUS.' as estatus_anterior',
            'cat_status_tickets2.' . CatStatusTickets::ESTATUS.' as estatus_nuevo',
        )
        ->join(
            CatStatusTickets::getTableName().' as cat_status_tickets1',
            'cat_status_tickets1.' . CatStatusTickets::ID,
            '=',
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::ESTATUS_ANTERIOR_ID
        )
        ->join(
            CatStatusTickets::getTableName().' as cat_status_tickets2',
            'cat_status_tickets2.' . CatStatusTickets::ID,
            '=',
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::ESTATUS_NUEVO_ID
        )
        ->where(
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::TICKET_ID,$ticket_id
        )
        ->orderBy(
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::CREATED_AT,'desc'
        )
        
        ;
        return $query->get();
    }
    public function getAllByFolio($folio){
        $query =  $this->modelo->select(
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::ID,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::COMENTARIO,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::USUARIO,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::CREATED_AT,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::INICIO_ESTATUS,
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::FIN_ESTATUS,
            'cat_status_tickets1.' . CatStatusTickets::ESTATUS.' as estatus_anterior',
            'cat_status_tickets2.' . CatStatusTickets::ESTATUS.' as estatus_nuevo',
        )
        ->join(
            CatStatusTickets::getTableName().' as cat_status_tickets1',
            'cat_status_tickets1.' . CatStatusTickets::ID,
            '=',
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::ESTATUS_ANTERIOR_ID
        )
        ->join(
            CatStatusTickets::getTableName().' as cat_status_tickets2',
            'cat_status_tickets2.' . CatStatusTickets::ID,
            '=',
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::ESTATUS_NUEVO_ID
        )
        ->join(
            TicketsModel::getTableName(),
            TicketsModel::getTableName() . '.' . CatStatusTickets::ID,
            '=',
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::TICKET_ID
        )
        ->where(
            TicketsModel::getTableName() . '.' . TicketsModel::FOLIO,$folio
        )
        ->orderBy(
            HistorialTicketsModel::getTableName() . '.' . HistorialTicketsModel::CREATED_AT,'desc'
        )
        
        ;
        return $query->get();
    }

    
}
