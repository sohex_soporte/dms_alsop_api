<?php

namespace App\Servicios\Soporte;

use App\Models\Notificaciones\NotificacionesModel;
use App\Models\Soporte\CatRolesSoporteModel;
use App\Models\Soporte\CatStatusPrioridadesModel;
use App\Models\Soporte\CatStatusTickets;
use App\Models\Soporte\HistorialTicketsModel;
use App\Models\Soporte\TicketsModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ServicioDB;
use App\Servicios\Notificaciones\ServicioNotificaciones;
use App\Servicios\Usuarios\ServicioUsuarios;

class ServicioTickets extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo tickets';
        $this->modelo = new TicketsModel();
        $this->modeloHistorialTickets = new HistorialTicketsModel();
        $this->ServicioHistorialTickets = new ServicioHistorialTickets();
        $this->ServicioNotificaciones = new ServicioNotificaciones();
        $this->ServicioTelefonosSoporte = new ServicioTelefonosSoporte();
        $this->ServicioUsuarios = new ServicioUsuarios();
        $this->ServicioCatRoles = new ServicioCatRoles();
    }

    public function getReglasGuardar()
    {
        return [
            TicketsModel::ASUNTO => 'required',
            TicketsModel::COMENTARIO => 'required',
            TicketsModel::USER_ID => 'required',
            TicketsModel::PRIORIDAD_ID => 'required',
            TicketsModel::ROL_ID => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            TicketsModel::ASUNTO => 'required',
            TicketsModel::COMENTARIO => 'required',
            TicketsModel::USER_ID => 'required',
            TicketsModel::PRIORIDAD_ID => 'required',
            TicketsModel::ROL_ID => 'required'
        ];
    }
    function random($num)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $num; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }
    public function handleSaveTicket($path, $data)
    {
        $random = $this->random(4);
        $folio_ticket = 'MQ-' . $random . $data[TicketsModel::USER_ID];
        $rol = $this->ServicioCatRoles->getById($data[TicketsModel::ROL_ID]);
        $ticket =  $this->crear([
            TicketsModel::ASUNTO => $data[TicketsModel::ASUNTO],
            TicketsModel::COMENTARIO => $data[TicketsModel::COMENTARIO],
            TicketsModel::USER_ID => $data[TicketsModel::USER_ID],
            TicketsModel::PRIORIDAD_ID => $data[TicketsModel::PRIORIDAD_ID],
            TicketsModel::ROL_ID => $data[TicketsModel::ROL_ID],
            TicketsModel::FOLIO => $folio_ticket,
            TicketsModel::EVIDENCIA => $path
        ]);
        $this->ServicioHistorialTickets->crear([
            HistorialTicketsModel::COMENTARIO => 'Creación de ticket',
            HistorialTicketsModel::USER_ID => $data[TicketsModel::USER_ID],
            HistorialTicketsModel::USUARIO => $data[HistorialTicketsModel::USUARIO],
            HistorialTicketsModel::ESTATUS_NUEVO_ID => 1,
            HistorialTicketsModel::TICKET_ID => $ticket->id,
            HistorialTicketsModel::EVIDENCIA => $path,
            HistorialTicketsModel::INICIO_ESTATUS => date('Y-m-d H:i:s'),
            HistorialTicketsModel::FIN_ESTATUS => date('Y-m-d H:i:s'),
        ]);
        $telefonos = $this->ServicioTelefonosSoporte->buscarTodos();
        $CONST_AGENCIA = 'ALSOP';
        $mensaje = "El usuario " . $data[HistorialTicketsModel::USUARIO] . " ha generado un nuevo ticket en la agencia " . $CONST_AGENCIA . " con # de folio: $folio_ticket, fecha de creación: " . date('Y-m-d H:i:s') . ' asunto: ' . $data[TicketsModel::ASUNTO] . ', Comentario: ' . $data[TicketsModel::COMENTARIO] . ', rol: ' . $rol->rol;
        foreach ($telefonos as $t => $data) {
            $this->notificacion($data->telefono, $mensaje);
        }

        return $ticket;
    }
    public function updateTicket($request, $id)
    {
        $inicio_estatus = $this->getStartTransition($id);
        $ticket_actual = $this->getById($id);
        $ticket = $this->massUpdateWhereId(TicketsModel::ID, $id, [
            TicketsModel::ESTATUS_ID => $request->get(HistorialTicketsModel::ESTATUS_NUEVO_ID)
        ]);
        $this->ServicioHistorialTickets->crear([
            HistorialTicketsModel::COMENTARIO => $request->get(HistorialTicketsModel::COMENTARIO).'123',
            HistorialTicketsModel::USER_ID => $request->get(HistorialTicketsModel::USER_ID),
            HistorialTicketsModel::USUARIO => $request->get(HistorialTicketsModel::USUARIO),
            HistorialTicketsModel::ESTATUS_ANTERIOR_ID => $ticket_actual->estatus_id,
            HistorialTicketsModel::ESTATUS_NUEVO_ID => $request->get(HistorialTicketsModel::ESTATUS_NUEVO_ID),
            HistorialTicketsModel::EVIDENCIA => $request->get(HistorialTicketsModel::EVIDENCIA),
            HistorialTicketsModel::INICIO_ESTATUS => $inicio_estatus,
            HistorialTicketsModel::FIN_ESTATUS => date('Y-m-d H:i:s'),
            HistorialTicketsModel::TICKET_ID => $id,
        ]);
        $this->ServicioNotificaciones->crear([
            NotificacionesModel::FECHA_NOTIFICACION => $request->get(NotificacionesModel::FECHA_NOTIFICACION),
            NotificacionesModel::ASUNTO => $request->get(NotificacionesModel::ASUNTO),
            NotificacionesModel::TEXTO => $request->get(NotificacionesModel::TEXTO),
            NotificacionesModel::USER_ID => $request->get(NotificacionesModel::USER_ID)
        ]);
        $usuario = $this->ServicioUsuarios->getWhere('id', $ticket_actual->user_id);
        if (count($usuario) > 0 && $usuario[0]['telefono'] != '') {
            $this->notificacion($usuario[0]['telefono'], $request->get(NotificacionesModel::TEXTO));
        }

        return $ticket;
    }
    public function getStartTransition($ticket_id = '')
    {
        $q = $this->modeloHistorialTickets->select(HistorialTicketsModel::FIN_ESTATUS)->where(HistorialTicketsModel::TICKET_ID, $ticket_id)->limit(1)->orderBy(HistorialTicketsModel::ID, 'desc')->get();
        if (count($q) == 1) {
            return $q[0]->fin_estatus;
        }
        return '';
    }
    public function notificacion($celular_sms = '', $mensaje_sms = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "celular=" . $celular_sms . "&mensaje=" . $mensaje_sms . "&sucursal=648370"
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
    }
    public function getAll($parametros)
    {
        $query = $this->modelo->select(
            TicketsModel::getTableName() . '.' . TicketsModel::ID,
            TicketsModel::getTableName() . '.' . TicketsModel::ASUNTO,
            TicketsModel::getTableName() . '.' . TicketsModel::COMENTARIO,
            TicketsModel::getTableName() . '.' . TicketsModel::EVIDENCIA,
            TicketsModel::getTableName() . '.' . TicketsModel::FOLIO,
            TicketsModel::getTableName() . '.' . TicketsModel::CREATED_AT,
            TicketsModel::getTableName() . '.' . TicketsModel::ESTATUS_ID,
            TicketsModel::getTableName() . '.' . TicketsModel::USER_ID,
            TicketsModel::getTableName() . '.' . TicketsModel::ROL_ID,
            CatStatusPrioridadesModel::getTableName() . '.' . CatStatusPrioridadesModel::PRIORIDAD,
            CatRolesSoporteModel::getTableName() . '.' . CatRolesSoporteModel::ROL,
            CatStatusTickets::getTableName() . '.' . CatStatusTickets::ESTATUS . ' as estatus_ticket',
            User::getTableName() . '.' . User::NOMBRE . ' AS nombre_usuario',
            User::getTableName() . '.' . User::APELLIDO_PATERNO . ' AS ap_usuario',
            User::getTableName() . '.' . User::APELLIDO_MATERNO . ' AS am_usuario',
            User::getTableName() . '.' . User::EMAIL . ' AS email_usuario',
            User::getTableName() . '.' . User::TELEFONO . ' AS telefono_usuario'
        )
            ->join(
                CatStatusPrioridadesModel::getTableName(),
                CatStatusPrioridadesModel::getTableName() . '.' . CatStatusPrioridadesModel::ID,
                '=',
                TicketsModel::getTableName() . '.' . TicketsModel::PRIORIDAD_ID
            )
            ->join(
                CatRolesSoporteModel::getTableName(),
                CatRolesSoporteModel::getTableName() . '.' . CatRolesSoporteModel::ID,
                '=',
                TicketsModel::getTableName() . '.' . TicketsModel::ROL_ID
            )
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                TicketsModel::getTableName() . '.' . TicketsModel::USER_ID
            )
            ->join(
                CatStatusTickets::getTableName(),
                CatStatusTickets::getTableName() . '.' . CatStatusTickets::ID,
                '=',
                TicketsModel::getTableName() . '.' . TicketsModel::ESTATUS_ID
            )
            ->orderBy(
                TicketsModel::getTableName() . '.' . TicketsModel::CREATED_AT,
                'desc',
            );
        if (isset($parametros[TicketsModel::ID])) {
            $query->where(
                TicketsModel::ID,
                $parametros[TicketsModel::ID]
            );
        }
        if (isset($parametros[TicketsModel::USER_ID])) {
            $query->where(
                TicketsModel::USER_ID,
                $parametros[TicketsModel::USER_ID]
            );
        }
        if (isset($parametros[TicketsModel::PRIORIDAD_ID])) {
            $query->where(
                TicketsModel::getTableName() . '.' . TicketsModel::PRIORIDAD_ID,
                $parametros[TicketsModel::PRIORIDAD_ID]
            );
        }
        if (isset($parametros[TicketsModel::ROL_ID])) {
            $query->where(
                TicketsModel::getTableName() . '.' . TicketsModel::ROL_ID,
                $parametros[TicketsModel::ROL_ID]
            );
        }
        if (isset($parametros[TicketsModel::FOLIO])) {
            $query->where(
                TicketsModel::getTableName() . '.' . TicketsModel::FOLIO,
                $parametros[TicketsModel::FOLIO]
            );
        }
        if (isset($parametros[TicketsModel::ESTATUS_ID])) {
            $query->where(
                TicketsModel::getTableName() . '.' . TicketsModel::ESTATUS_ID,
                $parametros[TicketsModel::ESTATUS_ID]
            );
        }
        if (isset($parameters['fecha_inicio']) && isset($parameters['fecha_fin'])) {
            $query->where(
                TicketsModel::getTableName() . '.' . TicketsModel::CREATED_AT,
                '>=',
                $parameters['fecha_inicio']
            );
            $query->where(
                TicketsModel::getTableName() . '.' . TicketsModel::CREATED_AT,
                '<=',
                $parameters['fecha_fin']
            );
        }
        return $query->get();
    }
}
