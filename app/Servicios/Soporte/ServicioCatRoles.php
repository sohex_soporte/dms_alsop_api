<?php

namespace App\Servicios\Soporte;

use App\Models\Soporte\CatRolesSoporteModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatRoles extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo roles';
        $this->modelo = new CatRolesSoporteModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatRolesSoporteModel::ROL => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatRolesSoporteModel::ROL => 'required'
        ];
    }
}
