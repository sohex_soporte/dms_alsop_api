<?php

namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatAgenciasModel;

class ServicioCatAgencias extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo agencias';
        $this->modelo = new CatAgenciasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatAgenciasModel::AGENCIA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatAgenciasModel::AGENCIA => 'required'
        ];
    }
}


