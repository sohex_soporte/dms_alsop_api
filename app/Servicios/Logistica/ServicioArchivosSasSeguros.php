<?php
namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\FilesSasSegurosModel;

class ServicioArchivosSasSeguros extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'archivos sas seguros';
        $this->modelo = new FilesSasSegurosModel();
    }
    public function saveFile($data){
        return $this->crear([
            FilesSasSegurosModel::ARCHIVO => $data['archivo'],
            FilesSasSegurosModel::PATH => $data['archivo'],
            FilesSasSegurosModel::SEGURO_ID => $data['seguro_id']
        ]);
    }
    public function getFilesBySeguro($seguro_id){
        return $this->modelo->where(FilesSasSegurosModel::SEGURO_ID,$seguro_id)->get();
    }
    public function getReglasUpload()
    {
        return [
            FilesSasSegurosModel::ARCHIVO => 'required|mimes:pdf',
            FilesSasSegurosModel::SEGURO_ID => 'nullable'
        ];
    }
}


