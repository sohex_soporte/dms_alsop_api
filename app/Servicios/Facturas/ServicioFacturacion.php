<?php

namespace App\Servicios\Facturas;

use App\Exceptions\MylsaException;
use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Facturas\ConceptosFactura;
use App\Models\Facturas\EmisorFactura;
use App\Models\Facturas\Factura;
use App\Models\Facturas\ReceptorFactura;
use App\Models\Facturas\TimbreFiscal;
use App\Servicios\Core\ServicioDB;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Throwable;

class ServicioFacturacion extends ServicioDB
{
    private $fileName;
    private $directorio;
    private $servicioXml;
    private $factura_id;
    // private $servicioFacturaEmisor;
    // private $servicioFacturaConceptos;
    // private $servicioTimbreFiscal;
    public function __construct()
    {
        $this->recurso = 'factura';
        $this->modelo = new Factura();
        $this->servicioXml = new ServicioXmlFactura();
        $this->servicioFacturaEmisor = new ServicioFacturaEmisor();
        $this->servicioFacturaReceptor = new ServicioFacturaReceptor();
        $this->servicioFacturaConceptos = new ServicioFacturaConceptos();
        $this->servicioTimbreFiscal = new ServicioTimbreFiscal();
        
    }

    public function setFacturaId(Int $id)
    {
        $this->factura_id = $id;
    }

    public function getFacturaId()
    {
        return $this->factura_id;
    }

    public function handleDataXmlFactura($path_factura = '')
    {
        if ($path_factura == '') {
            return [];
        }
        $real_path = realpath('../storage/app' . $path_factura);
        $xml = $this->getFacturaxml($real_path);
        $timbre = $this->getDataTimbreFiscal($xml->xpath('//t:TimbreFiscalDigital'));
        if ($this->servicioTimbreFiscal->existeTimbrefiscal($timbre[TimbreFiscal::UUID])) {
            throw new ParametroHttpInvalidoException([
                'error' => self::$I0010_FACTURA_REGISTRADA
            ]);
        }
        // if (!$this->servicioTimbreFiscal->existeTimbrefiscal($timbre[TimbreFiscal::UUID])) {
            $factura = $this->guardarDataComprobante($xml->xpath('//cfdi:Comprobante'), $path_factura, $timbre[TimbreFiscal::UUID]);
            $this->guardarDataTimbreFiscal($xml->xpath('//t:TimbreFiscalDigital'));
            $this->guardarDataEmisor($xml->xpath('//cfdi:Comprobante//cfdi:Emisor'));
            $this->guardarDataReceptor($xml->xpath('//cfdi:Comprobante//cfdi:Receptor'));
            $this->guardarDataConcepto($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto'));

            return $factura->id;
        // } else {
        //     $factura = $this->servicioTimbreFiscal->getWhere(TimbreFiscal::UUID, $timbre[TimbreFiscal::UUID]);
        //     $this->setFacturaId($factura[0][TimbreFiscal::FACTURA_ID]);
        //     $this->guardarDataConcepto($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto'));

        //     return $factura[0]['factura_id'];
        // }

        // $this->generarXml();
        // $this->getDataConceptoTraslado($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado'));
        // $this->getDataconceptoRetencion($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado'));
        
    }

    public function getFacturaxml($path_factura = '')
    {
        if ($path_factura == '') {
            return [];
        }

        $xml = simplexml_load_file($path_factura);
        // dd($xml, $path_factura);
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);
        return $xml;
    }

    public function getDataFacturaComprobante($nodo_xml_comprobante)
    {
        if ($nodo_xml_comprobante) {
            $comprobante = $nodo_xml_comprobante[0];
            $tipo_cambio = $this->servicioXml->validateAndCastItem($comprobante['TipoCambio']);
            $fecha = $this->servicioXml->validateAndCastItem($comprobante['Fecha']);
            $total = $this->servicioXml->validateAndCastItem($comprobante['Total']);
            $subtotal = $this->servicioXml->validateAndCastItem($comprobante['SubTotal']);
            $moneda = $this->servicioXml->validateAndCastItem($comprobante['Moneda']);
            $tipo_comprobante = $this->servicioXml->validateAndCastItem($comprobante['TipoDeComprobante']);
            $forma_pago = $this->servicioXml->validateAndCastItem($comprobante['FormaPago']);
            $lugar_expedicion = $this->servicioXml->validateAndCastItem($comprobante['LugarExpedicion']);
            $metodo_pago = $this->servicioXml->validateAndCastItem($comprobante['MetodoPago']);
            $folio = $this->servicioXml->validateAndCastItem($comprobante['Folio']);
            $serie = $this->servicioXml->validateAndCastItem($comprobante['Serie']);
            $no_certificado = $this->servicioXml->validateAndCastItem($comprobante['NoCertificado']);
            $certificado = $this->servicioXml->validateAndCastItem($comprobante['Certificado']);
            $sello = $this->servicioXml->validateAndCastItem($comprobante['Sello']);

            return [
                'tipo_cambio' => $tipo_cambio,
                'fecha' => $fecha,
                'total' => $total,
                'subtotal' => $subtotal,
                'moneda' => $moneda,
                'tipo_comprobante' => $tipo_comprobante,
                'forma_pago' => $forma_pago,
                'lugar_expedicion' => $lugar_expedicion,
                'no_certificado' => $no_certificado,
                'metodo_pago' => $metodo_pago,
                'serie' => $serie,
                'folio' => $folio,
                'certificado' => $certificado,
                'sello' => $sello,
            ];
        }
    }

    public function getDataTimbreFiscal($array)
    {
        if (count($array) > 0) {
            $item = $array[0];
            return [
                TimbreFiscal::SELLO_CFD => $this->servicioXml->validateAndCastItem($item['SelloCFD']),
                TimbreFiscal::NO_CERTIFICADO_SAT => $this->servicioXml->validateAndCastItem($item['NoCertificadoSAT']),
                TimbreFiscal::RFC_PROV_CERTIF => $this->servicioXml->validateAndCastItem($item['RfcProvCertif']),
                TimbreFiscal::FECHA_TIMBRADO => $this->servicioXml->validateAndCastItem($item['FechaTimbrado']),
                TimbreFiscal::SELLO_SAT => $this->servicioXml->validateAndCastItem($item['SelloSAT']),
                TimbreFiscal::UUID => $this->servicioXml->validateAndCastItem($item['UUID']),
            ];
        }

        return [];
    }

    public function guardarDataComprobante(array $data, $path_factura, $timbre_fiscal)
    {
        if (count($data) > 0) {
            $factura = $this->getDataFacturaComprobante($data);
            try {
                DB::beginTransaction();
                $factura = $this->modelo->create(
                    [
                        Factura::NO_CERTIFICADO => $factura['no_certificado'],
                        Factura::LUGAR_EXPEDICION => $factura['lugar_expedicion'],
                        Factura::FORMA_PAGO => $factura['forma_pago'],
                        Factura::METODO_PAGO => $factura['metodo_pago'],
                        Factura::FOLIO => $factura['folio'],
                        Factura::SERIE => $factura['serie'],
                        Factura::TIPO_COMPROBANTE => $factura['tipo_comprobante'],
                        Factura::TIPO_CAMBIO => $factura['tipo_cambio'],
                        Factura::MONEDA => $factura['moneda'],
                        Factura::TOTAL => $factura['total'],
                        Factura::SUB_TOTAL => $factura['subtotal'],
                        Factura::FECHA => $factura['fecha'],
                        Factura::CERTIFICADO => $factura['certificado'],
                        Factura::SELLO => $factura['sello'],
                        Factura::NO_CERTIFICADO => $factura['no_certificado'],
                        Factura::RUTA_XML_FACTURA => $path_factura,
                        Factura::FILE_NAME => $this->getFileName(),
                        Factura::UUID => $timbre_fiscal
                    ]
                );

                DB::commit();
                $this->setFacturaId($factura->id);
                return $factura;
            } catch (Throwable $e) {
                Log::error($e);
                DB::rollback();
                throw new MylsaException(__(static::$E0014_ERROR_UPDATING_RESOURCE, ['recurso' => $this->getRecurso()]));
            }
        }
    }

    public function guardarDataEmisor($data)
    {
        try {
            DB::beginTransaction();
            foreach ($data as $emisor) {
                $this->servicioFacturaEmisor->guardarEmisor(
                    $this->getFacturaId(),
                    [
                        EmisorFactura::RFC => $this->servicioXml->validateAndCastItem($emisor['Rfc']),
                        EmisorFactura::REGIMEN_FISCAL => $this->servicioXml->validateAndCastItem($emisor['RegimenFiscal']),
                        EmisorFactura::NOMBRE => $this->servicioXml->validateAndCastItem($emisor['Nombre']),
                    ]
                );
            }
            DB::commit();
        } catch (Throwable $e) {
            Log::error($e);
            DB::rollback();
            throw new MylsaException(__(static::$E0014_ERROR_UPDATING_RESOURCE, ['recurso' => $this->getRecurso()]));
        }
    }

    public function guardarDataReceptor($data)
    {
        try {
            DB::beginTransaction();
            foreach ($data as $key => $item) {
                $this->servicioFacturaReceptor->guardarReceptor(
                    $this->getFacturaId(),
                    [
                        ReceptorFactura::RFC => $this->servicioXml->validateAndCastItem($item['Rfc']),
                        ReceptorFactura::USOCFDI => $this->servicioXml->validateAndCastItem($item['UsoCFDI']),
                        ReceptorFactura::NOMBRE => $this->servicioXml->validateAndCastItem($item['Nombre']),
                    ]
                );
            }
            DB::commit();
        } catch (Throwable $e) {
            Log::error($e);
            DB::rollback();
            throw new MylsaException(__(static::$E0014_ERROR_UPDATING_RESOURCE, ['recurso' => $this->getRecurso()]));
        }
    }

    public function guardarDataConcepto(array $data)
    {
        try {
            DB::beginTransaction();
            foreach ($data as $key => $item) {
                $this->servicioFacturaConceptos->crearConcepto(
                    $this->getFacturaId(),
                    [
                        ConceptosFactura::CONCEPTO_CLAVE_PROD_SERV => $this->servicioXml->validateAndCastItem($item['ClaveProdServ']), // clave producto SAT
                        ConceptosFactura::CONCEPTO_NO_IDENTIFICACION => $this->servicioXml->validateAndCastItem($item['NoIdentificacion']), // clave unidad SAT
                        ConceptosFactura::CONCEPTO_CANTIDAD => $this->servicioXml->validateAndCastItem($item['Cantidad']), // Cantidad
                        ConceptosFactura::CONCEPTO_DESCRIPCION => $this->servicioXml->validateAndCastItem($item['Descripcion']), //Nombre del producto,
                        ConceptosFactura::CONCEPTO_VALOR_UNITARIO => $this->servicioXml->validateAndCastItem($item['ValorUnitario']), //
                        ConceptosFactura::CONCEPTO_IMPORTE => $this->servicioXml->validateAndCastItem($item['Importe']),
                        ConceptosFactura::CONCEPTO_CLAVE_UNIDAD => $this->servicioXml->validateAndCastItem($item['Unidad'])
                    ]
                );
            }
            DB::commit();
        } catch (Throwable $e) {
            Log::error($e);
            DB::rollback();
            throw new MylsaException(__(static::$E0014_ERROR_UPDATING_RESOURCE, ['recurso' => $this->getRecurso()]));
        }
    }

    public function getinformacionFactura($id)
    {
        if (!$id) {
            return [];
        }

        return $this->modelo
            ->where(Factura::ID, $id)
            ->with(
                [
                    Factura::REL_PRODUCTOS,
                    Factura::REL_RECEPTOR,
                    Factura::REL_EMISOR,
                    Factura::REL_CONCEPTOS,
                    Factura::REL_TIMBRE_FISCAL
                ]
            )->get();
    }

    public function guardarDataConceptoTraslado(array $data, $concepto)
    {
        if (count($data) > 0) {
            foreach ($data as $key => $item) {
                // $item['Base'];
                // $Impuesto = $item['Impuesto'];
                // $item['TipoFactor'];
                // $item['TasaOCuota'];
                // $item['Importe'];

                // $concepto->agregarImpuesto($traslado);
            }
        }
    }

    public function guardarDataconceptoRetencion(array $data, $concepto)
    {
        // foreach ($data as $key => $item) {
        //     $Impuesto = $item['Impuesto']; // ISR
        //     $TipoFactor = 'Tasa';
        //     $TasaOCuota = $item['TasaOCuota'];
        //     $Base = $item['Base'];
        //     $Importe = $Base * $TasaOCuota;
        //     // $concepto->agregarImpuesto($traslado);
        // }
    }

    public function guardarDataTimbreFiscal(array $data)
    {
        try {
            DB::beginTransaction();
            foreach ($data as $key => $item) {
                $this->servicioTimbreFiscal->guardarTimbre(
                    $this->servicioXml->validateAndCastItem($item['UUID']),
                    [
                        TimbreFiscal::SELLO_CFD => $this->servicioXml->validateAndCastItem($item['SelloCFD']),
                        TimbreFiscal::NO_CERTIFICADO_SAT => $this->servicioXml->validateAndCastItem($item['NoCertificadoSAT']),
                        TimbreFiscal::RFC_PROV_CERTIF => $this->servicioXml->validateAndCastItem($item['RfcProvCertif']),
                        TimbreFiscal::FECHA_TIMBRADO => $this->servicioXml->validateAndCastItem($item['FechaTimbrado']),
                        TimbreFiscal::SELLO_SAT => $this->servicioXml->validateAndCastItem($item['SelloSAT']),
                        TimbreFiscal::FACTURA_ID => $this->getFacturaId(),
                    ]
                );
            }
            DB::commit();
        } catch (Throwable $e) {
            Log::error($e);
            DB::rollback();
            throw new MylsaException(__(static::$E0014_ERROR_UPDATING_RESOURCE, ['recurso' => $this->getRecurso()]));
        }
    }

    public function setDirectory($folder)
    {
        $carpeta = self::DIRECTORIO_FACTURAS . DIRECTORY_SEPARATOR . self::DIRECTORIO_UPLOADS . DIRECTORY_SEPARATOR;
        $directorio = $carpeta  . self::DIRECTORIO_XML . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR;
        
        if ($this->fileExist($directorio)) {
            // dd($directorio);
            // $this->directorio = $directorio;
            return $directorio;
            // return $this->directorio;
        }

        Storage::makeDirectory($directorio);
        // $this->directorio = $directorio;
        // dd($this->directorio);
        // return $this->directorio;
        return $directorio;
    }

    public function getDirectory()
    {
        return $this->directorio;
    }

    public function fileExist($fileUrl)
    {
        return Storage::disk('local')->exists($fileUrl);
    }

    public function setFileName($fileObject)
    {
        $extension = "." . $fileObject->getClientOriginalExtension();
        $this->fileName = uniqid() . "_" . date('Y-m-d') . $extension;
        return $this->fileName;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function crearDocumento($request)
    {
        // $parametros = $request->all();
        // ParametrosHttpValidador::validar($request, $this->getReglasGuardar());
        // $this->setFileName($request->file(Factura::ARCHIVO_FACTURA));
        // $this->setDirectory($request);
        // $parametros['ruta_archivo'] = self::getDirectory();
        // $parametros['archivo'] = $this->getFileName();
        // return $this->modelo->create($parametros);
    }

    public function getReglasUploadxml()
    {
        return [
            Factura::ARCHIVO_FACTURA => 'required|mimes:xml'
        ];
    }

    public const FACTURA_ORDEN_COMPRA = 1;
    public const FACTURA_VENTAS = 2;
    public const DIRECTORIO_FACTURAS = 'facturas';
    public const DIRECTORIO_UPLOADS = 'uploads';
    public const DIRECTORIO_XML = 'xml';
    public const DIRECTORIO_PRODUCTOS_XML = 'productos';
}
