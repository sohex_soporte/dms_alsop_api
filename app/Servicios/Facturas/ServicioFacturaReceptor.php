<?php

namespace App\Servicios\Facturas;

use App\Servicios\Core\ServicioDB;
use App\Models\Facturas\ReceptorFactura;

class ServicioFacturaReceptor extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new ReceptorFactura();
    }

    public function guardarReceptor($id, array $data)
    {
        return $this->createOrUpdate(
            [ReceptorFactura::FACTURA_ID => $id],
            $data
        );
    }
}
