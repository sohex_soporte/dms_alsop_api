<?php

namespace App\Servicios\Core\Reportes;

abstract class Pdfservice
{
    protected $mpdf;

    abstract function setContenidoPdf($data);
    public function getInstance()
    {
        $this->mpdf = new \Mpdf\Mpdf();
    }

    public function generar()
    {
        return $this->mpdf->Output('Reporte_' . date('yy-m-d:m:s') . '.pdf', 'S');
    }

    public function addPage()
    {
        $this->mpdf->AddPage();
    }
}
