<?php

namespace App\Servicios\CPVentas;

use App\Models\CPVentas\CatStatusCPModel;
use App\Models\CPVentas\CPVentasModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ServicioDB;

class ServicioCPVentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cp_ventas';
        $this->modelo = new CPVentasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CPVentasModel::NO_CUENTA => 'required',
            CPVentasModel::VIN => 'required',
            CPVentasModel::APELLIDO => 'required',
            CPVentasModel::NOMBRE => 'required',
            CPVentasModel::TIPO_CLIENTE => 'required',
            CPVentasModel::TELEFONO_OFICINA => 'required',
            CPVentasModel::TELEFONO_CASA => 'required',
            CPVentasModel::ENGANCHE => 'required',
            CPVentasModel::TASA_INTERES => 'required',
            CPVentasModel::MENSUALIDAD => 'required',
            CPVentasModel::MONTO_FINANCIADO => 'required',
            CPVentasModel::MESES_CONTRATO => 'required',
            CPVentasModel::FECHA_INICIO_CONTRATO => 'required',
            CPVentasModel::FECHA_TERMINO => 'required',
            CPVentasModel::MARCA => 'required',
            CPVentasModel::MODELO => 'required',
            CPVentasModel::LINEA => 'required',
            CPVentasModel::TIPO_VEHICULO => 'required',
            CPVentasModel::ANIO => 'required',
            CPVentasModel::PLAN_FINANCIAMIENTO => 'required',
            CPVentasModel::VENDEDOR_ASIGNADO => 'required',
            CPVentasModel::ASIGNADO => 'required',
            CPVentasModel::EMAIL => 'required',
            CPVentasModel::INTENTOS => 'nullable',
            CPVentasModel::ESTATUS_ID => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CPVentasModel::NO_CUENTA => 'required',
            CPVentasModel::VIN => 'required',
            CPVentasModel::APELLIDO => 'required',
            CPVentasModel::NOMBRE => 'required',
            CPVentasModel::TIPO_CLIENTE => 'required',
            CPVentasModel::TELEFONO_OFICINA => 'required',
            CPVentasModel::TELEFONO_CASA => 'required',
            CPVentasModel::ENGANCHE => 'required',
            CPVentasModel::TASA_INTERES => 'required',
            CPVentasModel::MENSUALIDAD => 'required',
            CPVentasModel::MONTO_FINANCIADO => 'required',
            CPVentasModel::MESES_CONTRATO => 'required',
            CPVentasModel::FECHA_INICIO_CONTRATO => 'required',
            CPVentasModel::FECHA_TERMINO => 'required',
            CPVentasModel::MARCA => 'required',
            CPVentasModel::MODELO => 'required',
            CPVentasModel::LINEA => 'required',
            CPVentasModel::TIPO_VEHICULO => 'required',
            CPVentasModel::ANIO => 'required',
            CPVentasModel::PLAN_FINANCIAMIENTO => 'required',
            CPVentasModel::VENDEDOR_ASIGNADO => 'required',
            CPVentasModel::ASIGNADO => 'required',
            CPVentasModel::EMAIL => 'required',
            CPVentasModel::INTENTOS => 'nullable',
            CPVentasModel::ESTATUS_ID => 'nullable'
        ];
    }
    public function getAll($parameters)
    {
        $query = $this->modelo
                ->select(
                    CPVentasModel::getTableName() . '.*',
                    CatStatusCPModel::getTableName() . '.' . CatStatusCPModel::ESTATUS,
                    User::getTableName() . '.' . User::NOMBRE . ' as nombre_usuario',
                    User::getTableName() . '.' . User::APELLIDO_PATERNO . ' as ap_usuario',
                    User::getTableName() . '.' . User::APELLIDO_MATERNO . ' as am_usuario',
                )
                ->join(
                    User::getTableName(),
                    User::getTableName() . '.' . User::ID,
                    '=',
                    CPVentasModel::getTableName() . '.' . CPVentasModel::ASIGNADO
                )
                ->leftJoin(
                    CatStatusCPModel::getTableName(),
                    CatStatusCPModel::getTableName() . '.' . CatStatusCPModel::ID,
                    '=',
                    CPVentasModel::getTableName() . '.' . CPVentasModel::ESTATUS_ID
                );
                if (isset($parameters[CPVentasModel::VIN])) {
                    $query->where(
                        CPVentasModel::VIN,
                        $parameters[CPVentasModel::VIN]
                    );
                }
                if (isset($parameters[CPVentasModel::ASIGNADO])) {
                    $query->where(
                        CPVentasModel::ASIGNADO,
                        $parameters[CPVentasModel::ASIGNADO]
                    );
                }
            return $query->get();
    }
}
