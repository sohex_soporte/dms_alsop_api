<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatCompaniaSeguroModel;

class ServicioCatCompania extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo compañías seguros';
        $this->modelo = new CatCompaniaSeguroModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatCompaniaSeguroModel::COMPANIA => 'required',
            CatCompaniaSeguroModel::VALOR => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatCompaniaSeguroModel::COMPANIA => 'required',
            CatCompaniaSeguroModel::VALOR => 'required'
        ];
    }
}


